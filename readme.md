This Source code is same in IAPMS setup but having minimal custumization on user setup.

This application was made with Laravel Php Framework V 5.2.
This application runs and implemented and Repository pattern package using 
prettus/l5-repository - for guided of usage visit: https://github.com/andersao/l5-repository

Kindly import the database gpavs-default-setup.sql in you desired mysql DB.
the .sql file is located in this project root folder names "gpavs-default-setup.sql"

after the database default set up has been imported you may now login into the application defaults user on the setup.
the default user can be change upon production implementation or testing. You can add 3 types of user.
Administrator, Approver, Cashier.

sample users:

username:admin
pass:password

username:approver
pass:password

username:cashier
pass:password

After the setup has been followed and succeeded you may now start coding if has additional features to be added on this system.

Please read and follow below instructions of the standard coding for this project to avoid misunderstood for other developers who may handle this project and also for easy debugging.

HAPPY CODING.

Developer: Ryan Dingle
Credits: Romnic Susa - IAPMS Developer
-------------------------------------------------------------------------------------------------------------------------------

Routing Naming Convention

"resources.index" => Return view for listing of resources / return (json B) of list of resources
"resources.create" => Return a form to create resources  
"resources.store" => Handle the storing process of new resources (json A)
"resources.show" => Return view of to diplay single resources / Return sigle resources in (json B) format
"resources.edit" => Form to edit resources
"resources.update" => Handle updating of old resources and return (json A)
"resources.destroy" => Handle delete process of resources and return (json A)
"resources.save" => Handle store process if resources not existing otherwise handle update and return (json A)
"resources.list" => Return list of reources in datatable format like (json D)

JSON Return Format

json A
    return response()->json([ 
    	'success' => true , 
    	'message' => 'Resource has been created.' ,
    	'data' => $data // Optional
    ]);

json B
	return response()->json([ 
    	'success' => true , 
    	'data' => $data // object or array of object
    ]);

json C
	return response()->json([ 
    	'success' => true , 
    	'message' => 'DAR has been sent.' 
    ]);

json D
	return Datatables::of($scopes)->make(true);



Cheat Sheet
php artisan make:controller SampleController    // Singular Name and 'Controller' suffix
php artisan make:request SampleRequest    // Singular Name and 'Request' suffix
php artisan make:repository Sample    // Singular Name
php artisan make:bindings Sample     // To bind repository
php artisan make:migration create_sample_table --create=sample  // creating migrate of 'sample' table

Notes :
- Don't create model without repository
- No need to create model for a pivot table

Date Format : Dec-1-2017
For JS : "M-dd-yyyy",
For PHP : "M-d-Y", configuration can be found on config/app.php look for app.date_display

Random Token mismatch
- "https://github.com/laravel/framework/issues/8172"

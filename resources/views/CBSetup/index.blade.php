@extends('layouts.app')
@section('styles')

@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            <div class="page-header">
                <h1>
                    Company Branch Setup
                </h1>
            </div>
            @include('includes.alerts')
            <div class="row">
                <div class="col-xs-12">
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="open-modal btn btn-primary" data-modal="data-entry-modal" data-route="{{url('cb_setup/create/company')}}/null" href="javascript:;">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Company List
                    </div>
                    <table id="company-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Company Code</th>
                                <th>Company Name</th>
                                <th>Merchant Name</th>
                                <th>Tin Number</th>
                                <th>Depository Account</th>
                                <th width="6%"></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-12">
                    &nbsp;<br>
                </div>
                <div class="col-xs-12">
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="open-modal btn btn-primary create-branch-btn" data-modal="data-entry-modal" data-route="{{url('cb_setup/create/branch')}}" disabled="disabled" href="javascript:;">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Branch List
                    </div>
                    <table id="branch-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Branch Code</th>
                                <th>Branch Name</th>
                                <th width="6%"></th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script type="text/javascript">

    var company_id    = '';
    var company_table = $('#company-table').DataTable( {
        ajax: "{{url('cb_setup/list/company')}}",
        "processing": true,
        "serverSide": true,
        order: [[ 1, 'asc' ]],
        columns: [
            { data: "company_code" },
            { data: "company_name" },
            { data: "merchant_name" },
            { data: "tin_number" },
            { data: "depository_account" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    var icons = '<a data-modal="data-entry-modal" data-route="{{ url('cb_setup/edit')}}/'+data.company_id+'/company" href="javascript:;" class="open-modal edit"><i class="fa fa-pencil" title="Edit" rel="tooltip"></i></a> &nbsp;';
                        icons += '<a data-modal="data-entry-modal" data-route="{{ url('cb_setup/show') }}/'+data.company_id+'/company" href="javascript:;" class="open-modal view" title="View" rel="tooltip"><i class="fa fa-eye"></i></a> &nbsp;';
                        icons += '<a href="{{ url('cb_setup') }}/'+data.company_id+'/company" class="delete" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    return icons;
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    var branch_table    = $('#branch-table').DataTable( {
        ajax: "{{url('cb_setup/list/branch')}}/"+company_id,
        "processing": true,
        "serverSide": true,
        order: [[ 1, 'asc' ]],
        columns: [
            { data: "branch_code" },
            { data: "branch_name" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    var icons = '<a class="open-modal edit" href="javascript:;" data-route="{{url('cb_setup/edit')}}/'+data.branch_id+'/branch"><i class="fa fa-pencil" title="Edit" rel="tooltip"></i></a> &nbsp;';
                        icons += '<a class="open-modal view" href="javascript:;" data-route="{{ url('cb_setup/show') }}/'+data.branch_id+'/branch" class="view" title="View" rel="tooltip"><i class="fa fa-eye"></i></a> &nbsp;';
                        icons += '<a href="{{ url('cb_setup') }}/'+data.branch_id+'/branch" class="delete" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    return icons;
                },
                orderable: false,
                searchable: false
            }
        ]
    });

    // remove company
    $(document).on('click', "#company-table .delete" ,function(){
        $(this).deleteItemFrom(company_table, branch_table);
        return false;
    });

    // remove branch
    $(document).on('click', "#branch-table .delete" ,function(){
        $(this).deleteItemFrom(branch_table);  
        return false;
    });

    // dymamic post service
    $(document).on('submit', ".form-submit" ,function(e){
        e.preventDefault();
        var dis     = $(this)
        var method  = dis.attr('method');
        var action  = dis.attr('action');
        var reload  = dis.data('reload') ? dis.data('reload') : '';
        var data    = dis.serialize();

        $.ajax({
            type        : method,
            url         : action,
            cache       : false,
            data        : data,
            beforeSend  : sLoading(),
            success     : function(response){
                if(reload !== '') {
                    if(reload == 'company') company_table.ajax.reload( null, false );
                    else branch_table.ajax.url("{{url('cb_setup/list/branch')}}/"+company_id).load();
                }
                $.sSuccess(response.message);
                $('.close').trigger('click');
            }
        });
    });

    
    $(document).on('click','#company-table tbody tr',function(){
        $(".selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');

        var data    = company_table.row( $(this) ).data();
        company_id  = data.company_id;
        branch_table.ajax.url("{{url('cb_setup/list/branch')}}/"+data.company_id).load();
        $('.create-branch-btn').removeAttr('disabled');
        $('.create-branch-btn').attr("data-route", "{{url('cb_setup/create/branch')}}/"+data.company_id);
    });

</script>
@endsection

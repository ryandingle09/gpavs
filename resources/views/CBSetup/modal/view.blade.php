@if($type == 'company')
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="fa fa-info"></i> Company Details</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-xs-12 ">
            <table class="table">
                <tbody>
                    <tr>
                        <td>Company Code</td>
                        <td><b>{{$data->company_code}}</b></td>
                    </tr>
                    <tr>
                        <td>Company Name</td>
                        <td><b>{{$data->company_name}}</b></td>
                    </tr>
                    <tr>
                        <td>Merchant Name</td>
                        <td><b>{{$data->merchant_name}}</b></td>
                    </tr>
                    <tr>
                        <td>Tin Number</td>
                        <td><b>{{$data->tin_number}}</b></td>
                    </tr>
                    <tr>
                        <td>Depository Account</td>
                        <td><b>{{$data->depository_account}}</b></td>
                    </tr>
                </tbody>
            </table>
            <a href="" class="btn btn-warning btn-sm pull-right" data-dismiss="modal">
                <i class="ace-icon fa fa-times bigger-140"></i>
                Close
            </a>
        </div>
    </div>
</div>
@else
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="fa fa-info"></i> Branch Details</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-xs-12 ">
            <table class="table">
                <tbody>
                    <tr>
                        <td>Branch Code</td>
                        <td><b>{{$data->branch_code}}</b></td>
                    </tr>
                    <tr>
                        <td>Branch Name</td>
                        <td><b>{{$data->branch_name}}</b></td>
                    </tr>
                    <tr>
                        <td>Company Name</td>
                        <td><b>{{$data->company['company_name']}}</b></td>
                    </tr>
                </tbody>
            </table>
            <a href="" class="btn btn-warning btn-sm pull-right" data-dismiss="modal">
                <i class="ace-icon fa fa-times bigger-140"></i>
                Close
            </a>
        </div>
    </div>
</div>
@endif
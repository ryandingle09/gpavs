@if($type == 'company')
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Company</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-xs-12 ">
            <div id="alert"></div>
            <form class="form-horizontal form-submit" method="PUT" action="{{url('data/entry/'.$data->company_id.'/company')}}" data-reload="company">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Company Code </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" value="{{$data->company_code}}" name="company_code" />
                    </div>
                </div>
                <div class="form-group" >
                    <label class="col-sm-4 control-label align-left"> Company Name </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" value="{{$data->company_name}}" name="company_name" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Merchant Name </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" value="{{$data->merchant_name}}" name="merchant_name" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Tin Number </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" value="{{$data->tin_number}}" name="tin_number" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Depository Account </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" value="{{$data->depository_account}}" name="depository_account" />
                    </div>
                </div>
                <input type="hidden" name="updated_by" value="{{ $user }}">
                <input type="hidden" name="updated_at" value="{{ date('Y-m-d m:i:s') }}">
                <div class="form-row align-right" >
                    <button class="btn btn-info btn-sm">
                        <i class="ace-icon fa fa-save bigger-140"></i>
                        Apply
                    </button>
                    <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                        <i class="ace-icon fa fa-times bigger-140"></i>
                        Close
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@else
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Branch</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-xs-12 ">
            <div id="alert"></div>
            <form class="form-horizontal form-submit" method="PUT" action="{{url('data/entry/'.$data->branch_id.'/branch')}}" data-reload="branch">
                {{ csrf_field() }}
                <input type="hidden" name="company_id" value="{{$data->company_id}}">
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Branch Code </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" value="{{$data->branch_code}}" name="branch_code" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Branch Name </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" value="{{$data->branch_name}}" name="branch_name" />
                    </div>
                </div>
                <input type="hidden" name="updated_by" value="{{ $user }}">
                <input type="hidden" name="updated_at" value="{{ date('Y-m-d m:i:s') }}">
                <div class="form-row align-right" >
                    <button class="btn btn-info btn-sm">
                        <i class="ace-icon fa fa-save bigger-140"></i>
                        Apply
                    </button>
                    <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                        <i class="ace-icon fa fa-times bigger-140"></i>
                        Close
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
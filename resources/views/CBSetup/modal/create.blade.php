@if($type == 'company')
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="fa fa-plus"></i> Add Company</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-xs-12 ">
            <form class="form-horizontal form-submit" action="{{url('data/entry/company')}}" data-reload="company" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Company Code </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" name="company_code" />
                    </div>
                </div>
                <div class="form-group" >
                    <label class="col-sm-4 control-label align-left"> Company Name </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" name="company_name" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Merchant Name </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" name="merchant_name" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Tin Number </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" name="tin_number" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Depository Account </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" name="depository_account" />
                    </div>
                </div>
                <input type="hidden" name="created_by" value="{{ $user }}">
                <div class="form-row align-right" >
                    <button class="btn btn-info btn-sm">
                        <i class="ace-icon fa fa-save bigger-140"></i>
                        Apply
                    </button>
                    <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                        <i class="ace-icon fa fa-times bigger-140"></i>
                        Close
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@else
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="fa fa-plus"></i> Add Branch</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-xs-12 ">
            <form class="form-horizontal form-submit" method="POST" action="{{url('data/entry/branch/')}}" data-reload="branch">
                {{ csrf_field() }}
                <input type="hidden" name="company_id" value="{{$company_id}}">
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Branch Code </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" name="branch_code" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Branch Name </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" name="branch_name" />
                    </div>
                </div>
                <input type="hidden" name="created_by" value="{{ $user }}">
                <div class="form-row align-right" >
                    <button class="btn btn-info btn-sm">
                        <i class="ace-icon fa fa-save bigger-140"></i>
                        Apply
                    </button>
                    <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                        <i class="ace-icon fa fa-times bigger-140"></i>
                        Close
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    @include('includes.alerts')
                    <div class="clearfix">
                        <div class="form-group pull-right ">
                            <a class="btn btn-primary"  href="{{route('administrator.roles.create')}}">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Roles
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <div>
                        <table id="roles-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Menu Name</th>
                                    <th>Last Update</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
    $('#roles-table').DataTable( {
        errMode : 'none',
        ajax: "{{route('administrator.roles.list')}}",
        "processing": true,
        "serverSide": true,
        columns: [
            { data: "name" },
            { data: "updated_at" },
            {
                data: null,
                render : function ( data, type, full, meta ) {
                    var uri = encodeURIComponent(data.menu_name);
                    return '<a href="{{url('administrator/roles/edit')}}/'+data.id+'" class="btn-update" data-id="'+data.id+'" data-name="'+data.name+'"><i class="fa fa-pencil edit" title="edit" rel="tooltip"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-id="'+data.id+'" data-name="'+data.name+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    // remove lookup
    $(document).on('click', '.btn-delete', function() {
        var remove_id = $(this).attr('data-id');
        var remove_name = $(this).attr('data-name');
        sWarning("You are about to delete "+remove_name+" role?",function(){
            $.ajax({
                url: "{{ url('administrator/roles/delete') }}/"+remove_id,
                method: "POST",
                data: { _token : _token, _method : 'delete' }, // serializes all the form data
                // beforeSend : sLoading(),
                success : function(data) {
                    if( data.success ) {
                        location.reload();
                    }
                }
            });
        });
    });

</script>
@endsection
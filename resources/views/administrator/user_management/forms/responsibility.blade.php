@extends('layouts.app')

@section('styles')
    <style type="text/css">
        .deleted {
            text-decoration: line-through;
            color: red;
        }

    </style>
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/css/bootstrap-timepicker.css" />
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.css" />
    <link rel="stylesheet" href="/css/select2.css" />
@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Responsibility Form
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div style="display:none;" class="alert alert-warning"></div>
                                {!! Form::open(['url' => isset($details) ? route('administrator.responsibility.update', ['name' => $details->responsibility_name]) : route('administrator.responsibility.store'), 'class' => 'form-horizontal', 'id' => 'responsibility-form']) !!}
                                    @if( isset($details) )
                                    <input type="hidden" name="_method" value="put">
                                    <input type="hidden" name="id" value="{{ $details->responsibility_id }}">
                                    @endif
                                    <div class="form-group">
                                        <div class="pull-right btn-group">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                Save
                                            </button>
                                            <a href="{{route('administrator.responsibility')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="name"> Name </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control input-sm basic-info" name="name" id="name" value="{{ isset($details) ? $details->responsibility_name : ''}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="description"> Description </label>

                                        <div class="col-sm-6">
                                            <input type="text" class="form-control input-sm" name="description" id="description" value="{{ isset($details) ? $details->description : '' }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="description"> Menu </label>

                                        <div class="col-sm-6">
                                            <select id="menu" name="menu" class="form-control">
                                                <option value="">-- Select Menu --</option>
                                                @foreach($menus as $menu)
                                                <option value="{{$menu->menu_id}}" {{ isset($details->menu_id) ? ($details->menu_id == $menu->menu_id ? 'selected' : '') : '' }}>{{$menu->menu_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="effectivity"> Effectivity Date </label>

                                        <div class="col-sm-3">
                                            <input type="text" class="form-control input-sm date-picker" id="start_date" name="start_date" placeholder="Start Date" data-date-format="dd-M-yyyy" value="{{ isset($details) ? $details->effective_start_date : '' }}">
                                        </div>

                                        <div class="col-sm-3">
                                            <input type="text" class="form-control input-sm date-picker" id="end_date" name="end_date" placeholder="End Date" data-date-format="dd-M-yyyy" value="{{ isset($details) ? $details->effective_end_date : '' }}">
                                        </div>
                                    </div>

                                    <hr/>
                                    <div class="form-group">
                                        <div class="pull-right btn-group">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                Save
                                            </button>
                                            <a href="{{route('administrator.responsibility')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')

    <script src="/js/date-time/bootstrap-datepicker.js"></script>
    <script src="/js/date-time/bootstrap-timepicker.js"></script>
    <script src="/js/date-time/moment.js"></script>
    <script src="/js/date-time/bootstrap-datetimepicker.js"></script>
    <script src="/js/select2.js"></script>

    <script type="text/javascript">
        var form        = $('#responsibility-form'),
            alert       = $('.alert');

         @if (Session::has('message'))
            alert.html('{!! session('message') !!}').show();
        @endif

        $(function(){
            $('.select2').css('width','200px').select2({allowClear:true});
            $('.select2-container').css('width', '100%')
        });

        // stops the page from reloading/closing if the form is edited
        $('input').change(function() {
            if( $(this).val() != "" ) {
                window.onbeforeunload = function() {
                    return 'Are you sure you want to navigate away from this page?';
                };
            }
        });

        // save the form through AJAX call
        function save() {
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    if( data.success ) {
                        window.onbeforeunload = null;
                        window.location = '{{ route('administrator.responsibility') }}';
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    if( xhr.status == 500 ) {
                        alert.removeClass('alert-warning').addClass('alert-danger');
                        alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var msg = '<ul class="list-unstyled">';

                        $.each(xhr.responseJSON, function(key, val){
                            msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[0]+'</li>';
                            $('#'+key).parent().parent().addClass('has-error');
                        });
                        msg += '</ul>';

                        alert.removeClass('alert-danger').addClass('alert-warning');
                        alert.html(msg).show();
                    }

                    toggle_loading();
                }
            });
        }
    </script>
@endsection
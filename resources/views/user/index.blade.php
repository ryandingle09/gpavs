@extends('layouts.app')
@section('styles')

@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')
            @include('includes.alerts')
            <div class="row">
                <div class="col-xs-12">
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{route('user.create')}}">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        User List
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <table id="user-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Fullname</th>
                                <th>Username</th>
                                <!-- <th>User Tag</th> -->
                                <th width="5%"></th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script type="text/javascript">
    var auditor_datatable = $('#user-table').DataTable( {
        ajax: "{{route('user.list')}}",
        "processing": true,
        order: [[ 1, 'asc' ]],
        columns: [
            { data: "full_name" },
            { data: "user_name" },
            //{ data: "user_tag" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('user/edit')}}/'+data.user_id+'"><i class="fa fa-pencil" title="Edit" rel="tooltip"></i></a> &nbsp; <a href="{{ url('user') }}/'+data.user_id+'" class="delete" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    // remove auditor
    $(document).on('click', "#user-table .delete" ,function(){
        $(this).deleteItemFrom(auditor_datatable);  
        return false;
    });

</script>
@endsection

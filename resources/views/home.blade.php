@extends('layouts.app')

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <!-- /section:basics/content.breadcrumbs -->
        <div class="page-content">
            @include('includes.content_header')
            <div class="row">
                <div class="col-xs-12">
                    
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>{{env('APP_NAME')}} | {{ ucwords( str_replace(['_', '-'], ' ', Request::segment(1)) ) }}</title>
    <meta name="base_url" content="{{ url('/')}}">
    <meta name="token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="stylesheet" href="/css/bootstrap.css" />
    <link rel="stylesheet" href="/css/font-awesome.css" />
    <link rel="stylesheet" href="/css/jquery-ui.custom.css" />
    <link rel="stylesheet" href="/css/ace-fonts.css" />
    <link rel="stylesheet" href="/css/ace.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.bootstrap3.css" />
    <link rel="stylesheet" href="/css/selectize-custom.css" />
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
    <link rel="stylesheet" href="/css/override.css" />
    <!--[if lte IE 9]>
        <link rel="stylesheet" href="/css/ace-part2.css" />
        <link rel="stylesheet" href="/css/ace-ie.css" />
        <script src="/js/html5shiv.js"></script>
        <script src="/js/respond.js"></script>
    <![endif]-->

    <script type="text/javascript">
        var base_url = '{{url('/')}}';
        var curr_date = '{{date('d-M-Y')}}';
        var _token = '{{ csrf_token() }}';
        var isAutoCheckEnabled = parseInt({{ config('session.inactivity_check') }});
        var autoCheckInterval = parseInt({{ config('session.inactivity_check_interval') }});
    </script>
    <!-- basic scripts -->
        <!--[if !IE]> -->
        <script type="text/javascript">
            window.jQuery || document.write("<script src='/js/jquery.js'>"+"<"+"/script>");
        </script>

        <!-- <![endif]-->

        <!--[if IE]>
    <script type="text/javascript">
     window.jQuery || document.write("<script src='/js/jquery1x.js'>"+"<"+"/script>");
    </script>
    <![endif]-->
    @yield('styles')
</head>
<body class="{{ Auth::check() ? 'no-skin' : 'login-layout light-login' }}">
    @if( Auth::check() )
        <div class="mask">
            <div class="loading">
                <i class="fa fa-cog fa-spin"></i> Loading...
            </div>
        </div>
        @include('includes.header')

        <!-- main-container -->
        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try{ace.settings.check('main-container' , 'fixed')}catch(e){}
            </script>

            @include('includes.sidebar')
    @endif

    @yield('content')

    @if( Auth::check() )
        </div>
        <!-- /main-container -->

        @include('includes.footer')
    @endif
</body>
</html>

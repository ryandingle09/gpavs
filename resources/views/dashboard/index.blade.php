@extends('layouts.app')
@section('styles')

@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            <div class="page-header">
                <h1>
                    Data Entry
                </h1>
            </div>
            @include('includes.alerts')
            <div class="row">
                <div class="col-xs-4">
                    <form class="form-horizontal form-submit">
                        <div class="form-group">
                            <label class="col-sm-4 control-label align-left"> Product </label>
                            <div class="col-sm-8">
                                <select class="form-control input-sm" name="p_id"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label align-left"> Company </label>
                            <div class="col-sm-8">
                                <select class="form-control input-sm" name="company_id"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label align-left"> Branch </label>
                            <div class="col-sm-8">
                                <select class="form-control input-sm" name="branch_id"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label align-left"> Cashier ID </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control input-sm" name="cashier_id" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label align-left"> Redemption Date </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control input-sm redemption_date" name="redemption_date" value="{{date('Y-m-d')}}" />
                            </div>
                        </div>
                        <input type="hidden" name="created_by" value="{{ $user }}">
                        <div class="form-row align-right">
                            <button class="btn btn-info" type="button" onClick="javascript:alert('Scanner not available');return false;">
                                <i class="ace-icon fa fa-barcode bigger-140"></i>
                                SCAN BARCODES
                            </button>
                            <button class="btn btn-success open-modal-manual" type="button" data-target="#barcode-modal">
                                <i class="ace-icon fa fa-save bigger-140"></i>
                                MANUAL ENTRY
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col-xs-8">
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-default submit" href="javascript:;">
                                Submit for Approval
                            </a>
                        </div>
                    </div>
                    <table id="transaction-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Barcode</th>
                                <th>Denomination</th>
                                <th width="4%"></th>
                            </tr>
                        </thead>
                    
                        <tbody>
                        </tbody>
                    </table>
                    <table class="table table-bordered" width="100%">
                        <tfooter>
                            <tr>
                                <td><b>TOTAL COUNT : <span class="count pull-right">0</span></b></td>
                                <td><b>TOTAL AMOUNT : <span class="total pull-right">0.00</span></b></td>
                            </tr>
                        </tfooter>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

<!-- barcode modal -->
<div class="modal fade" data-backdrop="static" id="barcode-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-barcode"></i> Enter Barcode</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 ">
                        <form class="form-horizontal form-submit" method="POST" action="{{url('dashboard/transaction')}}" data-reload="transaction">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left"> Barcode </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control input-sm" name="barcode" autocomplete="off" />
                                </div>
                            </div>
                            <input type="hidden" name="created_by" value="{{ $user }}">
                            <div class="form-row align-right">
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-140"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-140"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_script')
<script type="text/javascript">
    var transaction_datatable = $('#transaction-table').DataTable( {
        ajax: {
            "url": "{{url('dashboard/transaction/list')}}",
            "data": function(d){
                d.redemption_date = $('input[name="redemption_date"]').val();
                counter();
            },
        },
        "processing": true,
        "serverSide": true,
        order: [[ 1, 'asc' ]],
        columns: [
            { data: "barcode" },
            { data: "denomination" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    var icons = '<a href="{{ url('dashboard/transaction') }}/'+data.transaction_id+'" class="delete" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    return icons;
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    // remove transaction
    $(document).on('click', "#transaction-table .delete" ,function(){
        $(this).deleteItemFrom(transaction_datatable);  
        return false;
    });

    var company_select = $('select[name="company_id"]').selectize({
        valueField: 'company_id',
        labelField: 'company_name',
        searchField: 'company_name',
        options: [],
        create: false,
        render: {
            option: function (item, escape) {
                return " <div>" +escape(item.company_name) + "</div>";
            }
        },
        load: function (query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: '{{ url("dashboard/dropdown/company/null") }}',
                type: 'GET',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback();
                }
            });
        },
        onChange: function(value) {
            if (!value.length) return;
            branch_select.clear();
            branch_select.disable();
            branch_select.load(function(callback) {
                this.clearOptions();
                this.renderCache = {};
                $.ajax({
                    url: '{{ url("dashboard/dropdown/branch/") }}/'+value,
                    success: function(results) {
                        branch_select.enable();
                        callback(results);
                    },
                    error: function() {
                        callback();
                    }
                })
            });
        }
    })[0].selectize;

    var branch_select = $('select[name="branch_id"]').selectize({
        valueField: 'branch_id',
        labelField: 'branch_name',
        searchField: 'branch_name',
        options: [],
        create: false,
        render: {
            option: function (item, escape) {
                return '<div>'+escape(item.branch_name) + '<div>';
            }
        }
    })[0].selectize;
    
    branch_select.disable();

    var product_select = $('select[name="p_id"]').selectize({
        valueField: 'p_id',
        labelField: 'p_name',
        searchField: 'p_name',
        options: [],
        create: false,
        render: {
            option: function (item, escape) {
                return " <div>" +escape(item.p_name) + "</div>";
            }
        },
        load: function (query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: '{{ url("dashboard/dropdown/product/null") }}',
                type: 'GET',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback();
                }
            });
        }
    })[0].selectize;

    $('input[name="redemption_date"').datepicker({
        format: "M-dd-yyyy",
        //startDate: 'today',
        keyboardNavigation: false,
        autoclose : true,
    }).datepicker("setDate", "0");

    // dymamic post service
    $(document).on('submit', ".form-submit" ,function(e){
        e.preventDefault();
        var dis     = $(this)
        var method  = dis.attr('method');
        var action  = dis.attr('action');
        var reload  = dis.data('reload') ? dis.data('reload') : '';
        var data    = new FormData($(this)[0]);
            data.append('company_id', $('select[name="company_id"]').val());
            data.append('branch_id', $('select[name="branch_id"]').val());
            data.append('cashier_id', $('input[name="cashier_id"]').val());
            data.append('redemption_date', $('input[name="redemption_date"]').val());
            data.append('p_id', $('select[name="p_id"]').val());

        $.ajax({
            type        : method,
            url         : action,
            cache       : false,
            data        : data,
            contentType : false,
            processData : false,
            dataType    : 'json',
            beforeSend  : sLoading(),
            success     : function(response){
                transaction_datatable.ajax.reload( null, false );
                $('input[name="barcode"]').val('');
                $.sSuccess(response.message);
                $('.close').trigger('click');
            }
        });
    });

    $(document).on('change', ".redemption_date" ,function(){
        transaction_datatable.ajax.reload( null, false );
        counter();
    });

    $(document).on('click', ".submit" ,function(){
        $.ajax({
            type        : 'POST',
            url         : '{{ url("dashboard/transaction/approval") }}',
            cache       : false,
            data        : {'redemption_date':$('input[name="redemption_date"]').val()},
            dataType    : 'json',
            beforeSend  : sLoading(),
            success     : function(response){
                if(response.status == 'success')
                    $.sSuccess(response.message);
                else
                    $.sInfo(response.message);
            }
        });
    });

    $(document).on('click', '.open-modal-manual',function(){
        $(".has-error").removeClass('has-error');
        $($(this).data('target')+' .alert .close').trigger('click');
        $($(this).data('target')+' form > input').val('');
        $($(this).data('target')).modal('show');
    });

    function counter(){
        $.get('dashboard/transaction/counter', {'redemption_date':$('input[name="redemption_date"]').val()}, function(response){
            $('.count').html(response.count ? response.count : '0');
            $('.total').html(response.total ? response.total : '0.00');
        });
    }

</script>
@endsection

<div class="footer">
    <div class="footer-inner">
        <!-- #section:basics/footer -->
        <div class="footer-content">
            <span class="bigger-120">
                <span class="blue bolder">SM Retail</span>
                {{env('APP_NAME')}} {{ config('app.instance') ? '('.config('app.instance').')' : '' }}
            </span>
        </div>

        <!-- /section:basics/footer -->
    </div>
</div>
<div class="modal fade" id="session-timeout-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Your session is about to end</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <a href="{{ url('logout')}}" class="btn btn-default">Continue to logout</a>
                <a href="" class="btn btn-primary" id="stay_logged">I want to stay</a>
            </div>
        </div>
    </div>
</div>

<!-- to up -->
<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>

<script src="/js/bootstrap.js"></script>
<!--[if lte IE 8]>
  <script src="/js/excanvas.js"></script>
<![endif]-->
<script src="/js/ace/ace.js"></script>
<script src="/js/ace/ace.ajax-content.js"></script>
<script src="/js/ace/ace.sidebar.js"></script>
<script src="/js/ace/ace.sidebar-scroll-1.js"></script>
<script src="/js/ace/ace.submenu-hover.js"></script>
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/plugins/selectize/selectize.js"></script>
<script src="/js/date-time/bootstrap-datepicker.js"></script>
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script src="/js/selectize-extension.js"></script>

<script src="{{ asset('plugins/bootstrap-growl/jquery.bootstrap-growl.min.js') }}"></script>
<script src="{{ asset('plugins/gasparesganga-jquery-loading-overlay/src/loadingoverlay.min.js') }}"></script>

<script src="/js/main.js"></script>
<!-- inline scripts related to this page -->
<script type="text/javascript">

    var shownInactiveModal = false;
    // if(isAutoCheckEnabled == 1){
    if(false){
        setInterval(function() {
            $.post('{{ route('session.check_idle') }}', { '_token' : _token }, function(data) {
                if (data.status == 2) {
                    // User was logged out. Redirect to login page
                    loggedOut()
                }else if (data.status == 1) {
                    if(!shownInactiveModal){
                        // Warn the user
                        $('#session-timeout-modal .modal-body').html(data.message);
                        $('#session-timeout-modal').modal('show');
                        countDown(data.time_remaining);
                        shownInactiveModal =true;
                    }
                }
            });
        }, autoCheckInterval); 
    }

    function countDown(duration){
        var seconds = duration;
        setInterval(function () {
            if(--seconds > -1){
                $('#session-timeout-modal .modal-body span').text( seconds % 60 );
            }else{
                loggedOut();
            }

        }, 1000);
    }

    function loggedOut(){
        $("#session-timeout-modal .modal-title").html("Session end.");
        $("#session-timeout-modal #stay_logged").hide();
        $('#session-timeout-modal .modal-body').html("Due to inactivity, you have been logged off from the application. Log on again to access this page.");
        setTimeout(function(){
            document.location.href = '{{ url('/logout') }}';
        },5000);
    }

</script>
@yield('footer_script')
@yield('vue_js')
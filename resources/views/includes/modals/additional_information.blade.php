<div class="modal fade" data-backdrop="static" id="additional-information-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Additional Information Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning alert-modal addinfo-errormsg"></div>
                {!! Form::open(['url' => '', 'class' => 'form-horizontal', 'id' => 'additional-information-form', 'style' => 'margin-top: 12px;']) !!}
                    <input type="hidden" name="_method" value="post">
                    <input type="hidden" name="id" value="">
                    <div id="ai_wrapper"></div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-ai">Save changes</button>
            </div>
        </div>
    </div>
</div>
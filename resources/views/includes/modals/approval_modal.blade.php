<!-- Show Approval Modal -->
<div class="modal fade " data-backdrop="static" id="approval-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                {{-- <h4 class="modal-title"> Approval Category : Approval Form </h4> --}}
                <h6 class="modal-title"> Approval Category :  <b id="approval-title-cat">n/a</b> </h6>
                <h6 class="modal-title"> Approval Subcategory :  <b id="approval-title-subcat">n/a</b> </h6>
            </div>
            <div class="modal-body">
                
                <div class="approval-wrapper scrollable" id="approval-wrapper"></div>
                <form id="approval-form" class="form-horizontal clearfix approval-form add-approval-form" action="{{ url('approval') }}" method="POST" style="display: none;">
                    <div class="form-group">
                        <div class="col-sm-9">
                            <textarea name="remarks" class="form-control input-sm noresize scrollable" rows="4"></textarea>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control  input-sm mrgB15" name="status_to" id="approval_status_select"> 
                                <option value="">No select</option>
                            </select>
                            <div class="text-right">
                                <input type="hidden" name="cat" value="">
                                <input type="hidden" name="subcat" value="">
                                <input type="hidden" name="ref_id" value="">
                                <input type="hidden" name="status" value="">
                                <input type="hidden" name="key" value="">
                                <input type="hidden" name="auditor_type" value="">
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="#" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>  
                            </div>
                        </div>
                    </div>
                </form>
                <form id="edit-approval-form" class="form-horizontal clearfix approval-form edit-approval-form" action="" method="POST" style="display: none;">
                    <div class="form-group">
                        <div class="col-sm-9">
                            <textarea name="remarks" class="form-control input-sm noresize scrollable" rows="4"></textarea>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control input-sm mrgB15" name="status" value="n/a" readonly="">
                            <div class="text-right">
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="#" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>  
                            </div>
                        </div>
                    </div>
                </form>
                <div class=" text-right approval-noform" style="display: none;">
                    <div class="form-group">
                        <a href="#" class="btn btn-warning btn-sm" data-dismiss="modal">
                            <i class="ace-icon fa fa-times bigger-120"></i>
                            Close
                        </a> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Show Approval Modal -->
@section('approval_modal_scripts')
<script type="text/template" id="status-item-template">
    <div class="approval-item">
        <div class="remarks-header">
            <span class="title"> [approver_fullname] change status from  "<i>[status_from]</i>" to "<i>[status_to]</i>"</span>
            <span class="date">[approval_date]</span>
        </div>
        <div class="remarks-body">
            <p>[remarks] </p>
            [edit_button]
        </div>
    </div>
</script>
<script type="text/javascript">
    var no_approval_html = '<div class="alert alert-info"> <strong> <i class="ace-icon fa fa-info"></i> </strong> No changes of status found. <br> </div';
    var approval_edit_button = '<a href="#" class="edit-approval pull-right" data-id="[a_id]" data-remarks="[a_remarks]" data-status="[a_status]"><i class="fa fa-pencil text-primary" title="Edit Remarks" rel="tooltip"></i></a>';

    $(document).on("click",".btn-approval",function(){
        $("#approval-wrapper").html(no_approval_html);
        $(".add-approval-form")[0].reset();
        $(".edit-approval-form")[0].reset();
        $(".add-approval-form, .edit-approval-form").hide();
        $(".approval-noform").show();
        var data = {
            key : $(this).attr('data-key'),
            status : $(this).attr('data-status'),
            ref_id : $(this).attr('data-ref-id'),
            cat : $(this).attr('data-cat'),
            subcat : $(this).attr('data-subcat'),
        };
        console.log(data);
        $.ajax({
            url : "{{ route('approval.approval_info') }}",
            method : "GET",
            data : data, 
            beforeSend : sLoading(), 
            success : function(response){
                var option = $("#approval_status_select");
                option.html("");
                if(response.data.next_status.length > 0){
                    $(".add-approval-form, .approval-noform").toggle();
                    $.each (response.data.next_status , function() {
                        option.append($("<option />").val(this.status).text(this.status));
                    });
                }
                data.auditor_type = response.data.auditor_type;
                $("#approval-modal").find('form.add-approval-form')
                    .attr('id',data.key+"-approval-form")
                    .supply(data);
                $("#approval-title-cat").text(data.cat);
                $("#approval-title-subcat").text(data.subcat);

                if(response.data.approvals.length > 0){
                    var listing_html = "";
                    $.each(response.data.approvals,function(k,v){
                        temp_btn = approval_edit_button.replace('[a_remarks]',v.remarks)
                                                    .replace('[a_id]',v.approval_id)
                                                    .replace('[a_status]',v.status_to);
                        ebtn = k == 0 ? temp_btn : '';
                        listing_html += $('#status-item-template')
                                            .html()
                                            .replace("[status_from]", v.status_from )
                                            .replace("[status_to]", v.status_to )
                                            .replace("[approval_date]", v.approval_date )
                                            .replace("[remarks]", v.remarks )
                                            .replace("[approver_fullname]", v.approver.full_name )
                                            .replace("[edit_button]", ebtn );
                    });
                    $("#approval-wrapper").html(listing_html).animate({scrollTop: 0 }, 'slow');
                }

                swal.close();
	            $("#approval-modal").modal('show');
            }
        });

        return false;
    });

    $(document).on('click',".edit-approval", function(){
        $(".edit-approval-form, .approval-noform").toggle();
        var edit_approval_url = "{{ url('approval') }}/" + $(this).data('id');
        $(".edit-approval-form").attr('action',edit_approval_url);
        $(".edit-approval-form [name='remarks']").val( $(this).data('remarks') );
        $(".edit-approval-form [name='status']").val( $(this).data('status') );
        return false;
    });

    $(document).on('submit','#edit-approval-form', function(){
        $.ajax({
            url : $(this).attr('action'),
            method : "PUT",
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                swal.close();
                $("a[data-id='"+response.data.approval_id+"']").parent().find('p').text(response.data.remarks);
                $("a[data-id='"+response.data.approval_id+"']").data('remarks', response.data.remarks);
                $(".edit-approval-form, .approval-noform").toggle();
            }
        })
        return false;
    });
</script>
@endsection

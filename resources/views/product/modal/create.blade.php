@if($type == 'product')
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="fa fa-plus"></i> Add Product</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-xs-12 ">
            <form class="form-horizontal form-submit" method="POST" action="{{url('product/product')}}" data-reload="product">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Type Code </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" name="p_type_code" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Product Name </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" name="p_name" />
                    </div>
                </div>
                <input type="hidden" name="created_by" value="{{ $user }}">
                <div class="form-row align-right" >
                    <button class="btn btn-info btn-sm">
                        <i class="ace-icon fa fa-save bigger-140"></i>
                        Apply
                    </button>
                    <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                        <i class="ace-icon fa fa-times bigger-140"></i>
                        Close
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@elseif($type == 'denomination')
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="fa fa-plus"></i> Add Denomination</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-xs-12 ">
            <form class="form-horizontal form-submit" method="POST" action="{{url('product/denomination')}}" data-reload="denomination">
                {{ csrf_field() }}
                <input type="hidden" name="p_id" value="{{$id}}">
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Denomination </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" name="amount" />
                    </div>
                </div>
                <input type="hidden" name="created_by" value="{{ $user }}">
                <div class="form-row align-right" >
                    <button class="btn btn-info btn-sm">
                        <i class="ace-icon fa fa-save bigger-140"></i>
                        Apply
                    </button>
                    <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                        <i class="ace-icon fa fa-times bigger-140"></i>
                        Close
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@else
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="fa fa-plus"></i> Add Barcode Segment</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-xs-12 ">
            <form class="form-horizontal form-submit" method="POST" action="{{url('product/segment')}}" data-reload="segment">
                {{ csrf_field() }}
                <input type="hidden" name="p_id" value="{{$id}}">
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Segment Name </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" name="segment_name" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> From </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" name="from" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> To </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" name="to" />
                    </div>
                </div>
                <input type="hidden" name="created_by" value="{{ $user }}">
                <div class="form-row align-right" >
                    <button class="btn btn-info btn-sm">
                        <i class="ace-icon fa fa-save bigger-140"></i>
                        Apply
                    </button>
                    <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                        <i class="ace-icon fa fa-times bigger-140"></i>
                        Close
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
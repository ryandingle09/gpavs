@if($type == 'product')
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Product</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-xs-12 ">
            <div id="alert"></div>
            <form class="form-horizontal form-submit" method="PUT" action="{{url('product/'.$data->p_id.'/product')}}" data-reload="product">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Type Code </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" value="{{$data->p_type_code}}" name="p_type_code" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Product Name </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" value="{{$data->p_name}}" name="p_name" />
                    </div>
                </div>
                <input type="hidden" name="updated_by" value="{{ $user }}">
                <input type="hidden" name="updated_at" value="{{ date('Y-m-d m:i:s') }}">
                <div class="form-row align-right" >
                    <button class="btn btn-info btn-sm">
                        <i class="ace-icon fa fa-save bigger-140"></i>
                        Apply
                    </button>
                    <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                        <i class="ace-icon fa fa-times bigger-140"></i>
                        Close
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@elseif($type == 'denomination')
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Denomination</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-xs-12 ">
            <form class="form-horizontal form-submit" method="PUT" action="{{url('product/'.$data->id.'/denomination')}}" data-reload="denomination">
                {{ csrf_field() }}
                <input type="hidden" name="p_id" value="{{$data->p_id}}">
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Denomination </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" value="{{str_replace('.00', '', $data->amount)}}" name="amount" />
                    </div>
                </div>
                <input type="hidden" name="updated_by" value="{{ $user }}">
                <input type="hidden" name="updated_at" value="{{ date('Y-m-d m:i:s') }}">
                <div class="form-row align-right" >
                    <button class="btn btn-info btn-sm">
                        <i class="ace-icon fa fa-save bigger-140"></i>
                        Apply
                    </button>
                    <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                        <i class="ace-icon fa fa-times bigger-140"></i>
                        Close
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@else
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Barcode Segment</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-xs-12 ">
            <form class="form-horizontal form-submit" method="PUT" action="{{url('product/'.$data->id.'/segment')}}" data-reload="segment">
                {{ csrf_field() }}
                <input type="hidden" name="p_id" value="{{$data->p_id}}">
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> Segment Name </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" value="{{$data->segment_name}}" name="segment_name" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> From </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" value="{{$data->from}}" name="from" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label align-left"> To </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" value="{{$data->to}}" name="to" />
                    </div>
                </div>
                <input type="hidden" name="updated_by" value="{{ $user }}">
                <input type="hidden" name="updated_at" value="{{ date('Y-m-d m:i:s') }}">
                <div class="form-row align-right" >
                    <button class="btn btn-info btn-sm">
                        <i class="ace-icon fa fa-save bigger-140"></i>
                        Apply
                    </button>
                    <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                        <i class="ace-icon fa fa-times bigger-140"></i>
                        Close
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
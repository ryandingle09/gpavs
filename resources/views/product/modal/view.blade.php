@if($type == 'product')
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="fa fa-info"></i> Product Details</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-xs-12 ">
            <table class="table">
                <tbody>
                    <tr>
                        <td>Type Code</td>
                        <td><b>{{$data->p_type_code}}</b></td>
                    </tr>
                    <tr>
                        <td>Product Name</td>
                        <td><b>{{$data->p_name}}</b></td>
                    </tr>
                </tbody>
            </table>
            <a href="" class="btn btn-warning btn-sm pull-right" data-dismiss="modal">
                <i class="ace-icon fa fa-times bigger-140"></i>
                Close
            </a>
        </div>
    </div>
</div>
@elseif($type == 'denomination')
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="fa fa-info"></i> Denomination Details</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-xs-12 ">
            <table class="table">
                <tbody>
                    <tr>
                        <td>Denomination</td>
                        <td><b>{{number_format($data->amount, 2)}}</b></td>
                    </tr>
                </tbody>
            </table>
            <a href="" class="btn btn-warning btn-sm pull-right" data-dismiss="modal">
                <i class="ace-icon fa fa-times bigger-140"></i>
                Close
            </a>
        </div>
    </div>
</div>
@else
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="fa fa-info"></i> Segment Details</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-xs-12 ">
            <table class="table">
                <tbody>
                    <tr>
                        <td>Segment Name</td>
                        <td><b>{{$data->segment_name}}</b></td>
                    </tr>
                    <tr>
                        <td>From</td>
                        <td><b>{{$data->from}}</b></td>
                    </tr>
                    <tr>
                        <td>To</td>
                        <td><b>{{$data->to}}</b></td>
                    </tr>
                </tbody>
            </table>
            <a href="" class="btn btn-warning btn-sm pull-right" data-dismiss="modal">
                <i class="ace-icon fa fa-times bigger-140"></i>
                Close
            </a>
        </div>
    </div>
</div>
@endif
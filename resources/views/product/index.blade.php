@extends('layouts.app')
@section('styles')

@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            <div class="page-header">
                <h1>
                    Product Setup
                </h1>
            </div>
            @include('includes.alerts')
            <div class="row">
                <div class="col-xs-12">
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="open-modal btn btn-primary" data-modal="data-entry-modal" data-route="{{url('product/create/product/null')}}" href="javascript:;">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Product List
                    </div>
                    <table id="product-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Product Type Code</th>
                                <th>Product Name</th>
                                <th width="6%"></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-12">
                    &nbsp;<br>
                </div>
                <div class="col-xs-4">
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="open-modal btn btn-primary create-denomination-btn" data-modal="data-entry-modal" data-route="{{url('product/create/segment')}}" disabled="disabled" href="javascript:;">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Deomination List
                    </div>
                    <table id="denomination-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Denomination</th>
                                <th width="20%" class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-8">
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="open-modal btn btn-primary create-segment-btn" data-modal="data-entry-modal" data-route="{{url('product/create/segment')}}" disabled="disabled" href="javascript:;">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Segment List
                    </div>
                    <table id="segment-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Segment</th>
                                <th>From</th>
                                <th>To</th>
                                <th width="10%"></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script type="text/javascript">
    var product_id      = '';
    var product_table   = $('#product-table').DataTable( {
        ajax: "{{url('product/list/product')}}",
        "processing": true,
        "serverSide": true,
        order: [[ 1, 'asc' ]],
        columns: [
            { data: "p_type_code" },
            { data: "p_name" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    var icons = '<a data-modal="product" data-route="{{ url('product/edit')}}/'+data.p_id+'/product" href="javascript:;" class="open-modal edit"><i class="fa fa-pencil" title="Edit" rel="tooltip"></i></a> &nbsp;';
                        icons += '<a data-modal="product" data-route="{{ url('product/show') }}/'+data.p_id+'/product" href="javascript:;" class="open-modal view" title="View" rel="tooltip"><i class="fa fa-eye"></i></a> &nbsp;';
                        icons += '<a href="{{ url('product') }}/'+data.p_id+'/product" class="delete" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    return icons;
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    var denomination_table = $('#denomination-table').DataTable( {
        ajax: "{{url('product/list/denomination')}}/"+product_id,
        "processing": true,
        "serverSide": true,
        order: [[ 1, 'asc' ]],
        columns: [
            { data: "amount".split(".00").join("").toLowerCase() },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    var icons = '<a data-modal="product" data-route="{{ url('product/edit')}}/'+data.id+'/denomination" href="javascript:;" class="open-modal edit"><i class="fa fa-pencil" title="Edit" rel="tooltip"></i></a> &nbsp;';
                        icons += '<a data-modal="product" data-route="{{ url('product/show') }}/'+data.id+'/denomination" href="javascript:;" class="open-modal view" title="View" rel="tooltip"><i class="fa fa-eye"></i></a> &nbsp;';
                        icons += '<a href="{{ url('product') }}/'+data.id+'/denomination" class="delete" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    return icons;
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    var segment_table = $('#segment-table').DataTable( {
        ajax: "{{url('product/list/segment')}}/"+product_id,
        "processing": true,
        "serverSide": true,
        order: [[ 1, 'asc' ]],
        columns: [
            { data: "segment_name" },
            { data: "from" },
            { data: "to" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    var icons = '<a data-modal="segment" data-route="{{ url('product/edit')}}/'+data.id+'/segment" href="javascript:;" class="open-modal edit"><i class="fa fa-pencil" title="Edit" rel="tooltip"></i></a> &nbsp;';
                        icons += '<a data-modal="segment" data-route="{{ url('product/show') }}/'+data.id+'/segment" href="javascript:;" class="open-modal view" title="View" rel="tooltip"><i class="fa fa-eye"></i></a> &nbsp;';
                        icons += '<a href="{{ url('product') }}/'+data.id+'/segment" class="delete" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    return icons;
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    // remove company
    $(document).on('click', "#product-table .delete" ,function(){
        $(this).deleteItemFrom(product_table, denomination_table, segment_table);
        return false;
    });

    // remove company
    $(document).on('click', "#denomination-table .delete" ,function(){
        $(this).deleteItemFrom(denomination_table);
        return false;
    });

    // remove company
    $(document).on('click', "#segment-table .delete" ,function(){
        $(this).deleteItemFrom(segment_table);
        return false;
    });

    // dymamic post service
    $(document).on('submit', ".form-submit" ,function(e){
        e.preventDefault();
        var dis     = $(this)
        var method  = dis.attr('method');
        var action  = dis.attr('action');
        var reload  = dis.data('reload') ? dis.data('reload') : '';
        var data    = dis.serialize();

        $.ajax({
            type        : method,
            url         : action,
            cache       : false,
            data        : data,
            beforeSend  : sLoading(),
            success     : function(response){
                if(reload !== '') {
                    if(reload == 'product') product_table.ajax.reload( null, false );
                    else if(reload == 'segment') segment_table.ajax.reload( null, false );
                    else denomination_table.ajax.url("{{url('product/list/denomination')}}/"+product_id).load();
                }
                $.sSuccess(response.message);
                $('.close').trigger('click');
            }
        });
    });

    $(document).on('click','#product-table tbody tr',function(){
        $(".selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');

        var data    = product_table.row( $(this) ).data();
        product_id  = data.p_id;
        denomination_table.ajax.url("{{url('product/list/denomination')}}/"+data.p_id).load();
        segment_table.ajax.url("{{url('product/list/segment')}}/"+data.p_id).load();
        $('.create-denomination-btn').removeAttr('disabled');
        $('.create-denomination-btn').attr("data-route", "{{url('product/create/denomination')}}/"+data.p_id);
        $('.create-segment-btn').removeAttr('disabled');
        $('.create-segment-btn').attr("data-route", "{{url('product/create/segment')}}/"+data.p_id);
    });

</script>
@endsection

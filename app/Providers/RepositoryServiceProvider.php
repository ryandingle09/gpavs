<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\LookupTypeRepository::class, \App\Repositories\LookupTypeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\LookupValueRepository::class, \App\Repositories\LookupValueRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ResponsibilityRepository::class, \App\Repositories\ResponsibilityRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserResponsibilityAssignmentRepository::class, \App\Repositories\UserResponsibilityAssignmentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\FunctionsRepository::class, \App\Repositories\FunctionsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MenuRepository::class, \App\Repositories\MenuRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AuditeeRepository::class, \App\Repositories\AuditeeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AuditorRepository::class, \App\Repositories\AuditorRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AuditorProficiencyRepository::class, \App\Repositories\AuditorProficiencyRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\HolidayRepository::class, \App\Repositories\HolidayRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AuditorPlannedLeaveRepository::class, \App\Repositories\AuditorPlannedLeaveRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ControlsMasterRepository::class, \App\Repositories\ControlsMasterRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ControlsDetailRepository::class, \App\Repositories\ControlsDetailRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ControlsTestProcRepository::class, \App\Repositories\ControlsTestProcRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RiskRepository::class, \App\Repositories\RiskRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RisksControlRepository::class, \App\Repositories\RisksControlRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BusinessProcessRepository::class, \App\Repositories\BusinessProcessRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BusinessProcessesStepRepository::class, \App\Repositories\BusinessProcessesStepRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BpStepsRiskRepository::class, \App\Repositories\BpStepsRiskRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AuditableEntityRepository::class, \App\Repositories\AuditableEntityRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AuditableEntitiesBpRepository::class, \App\Repositories\AuditableEntitiesBpRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RiskAssessmentScaleRepository::class, \App\Repositories\RiskAssessmentScaleRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CompanyProfileRepository::class, \App\Repositories\CompanyProfileRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\QuestionnaireRepository::class, \App\Repositories\QuestionnaireRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\QuestionnaireDetailRepository::class, \App\Repositories\QuestionnaireDetailRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AuditorGradingMatrixRepository::class, \App\Repositories\AuditorGradingMatrixRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MatrixRepository::class, \App\Repositories\MatrixRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MatrixDetailRepository::class, \App\Repositories\MatrixDetailRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EnterpriseRiskRepository::class, \App\Repositories\EnterpriseRiskRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EnterpriseRiskDetailRepository::class, \App\Repositories\EnterpriseRiskDetailRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ValueSetRepository::class, \App\Repositories\ValueSetRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AdditionalInfoMasterRepository::class, \App\Repositories\AdditionalInfoMasterRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AdditionalInfoDetailRepository::class, \App\Repositories\AdditionalInfoDetailRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ControlsTestAuditTypeRepository::class, \App\Repositories\ControlsTestAuditTypeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PlanRepository::class, \App\Repositories\PlanRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PlanProjectRepository::class, \App\Repositories\PlanProjectRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProjectRepository::class, \App\Repositories\ProjectRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BpObjectiveRepository::class, \App\Repositories\BpObjectiveRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PlanProjectScopeRepository::class, \App\Repositories\PlanProjectScopeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CompanyBranchRepository::class, \App\Repositories\CompanyBranchRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AeMbpRepository::class, \App\Repositories\AeMbpRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AeMbpActualRepository::class, \App\Repositories\AeMbpActualRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AeMbpSbpRepository::class, \App\Repositories\AeMbpSbpRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AuditableEntitiesActualRepository::class, \App\Repositories\AuditableEntitiesActualRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\TestProcRepository::class, \App\Repositories\TestProcRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AuditorLcmRepository::class, \App\Repositories\AuditorLcmRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AuditorTrainingRepository::class, \App\Repositories\AuditorTrainingRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProjectScopeIomRepository::class, \App\Repositories\ProjectScopeIomRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProjectAuditorRepository::class, \App\Repositories\ProjectAuditorRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProjectScopeRepository::class, \App\Repositories\ProjectScopeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProjectScopeAuditorRepository::class, \App\Repositories\ProjectScopeAuditorRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProjectScopeApgRepository::class, \App\Repositories\ProjectScopeApgRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProjectScopeApgFindingsRepository::class, \App\Repositories\ProjectScopeApgFindingsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProjectScopeDarRepository::class, \App\Repositories\ProjectScopeDarRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProjectScopeDarHighlightRepository::class, \App\Repositories\ProjectScopeDarHighlightRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProjectScopeDarSofRepository::class, \App\Repositories\ProjectScopeDarSofRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProjectScopeApgApprovalRepository::class, \App\Repositories\ProjectScopeApgApprovalRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProjectScopeApprovalRepository::class, \App\Repositories\ProjectScopeApprovalRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\IomDistListRepository::class, \App\Repositories\IomDistListRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PlanAuditorRepository::class, \App\Repositories\PlanAuditorRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PlanProjectAuditorRepository::class, \App\Repositories\PlanProjectAuditorRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PlanProjectScopeAuditorRepository::class, \App\Repositories\PlanProjectScopeAuditorRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProjectScopeSummaryRepository::class, \App\Repositories\ProjectScopeSummaryRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RiskAssessmentScaleAnnualAsvRepository::class, \App\Repositories\RiskAssessmentScaleAnnualAsvRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ApprovalSetupMasterRepository::class, \App\Repositories\ApprovalSetupMasterRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ApprovalSetupDetailRepository::class, \App\Repositories\ApprovalSetupDetailRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ApprovalFromStatusRepository::class, \App\Repositories\ApprovalFromStatusRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ApprovalToStatusRepository::class, \App\Repositories\ApprovalToStatusRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ApprovalRepository::class, \App\Repositories\ApprovalRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProjectScopeFreezeRepository::class, \App\Repositories\ProjectScopeFreezeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PlanGroupRepository::class, \App\Repositories\PlanGroupRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PlanGroupAuditorRepository::class, \App\Repositories\PlanGroupAuditorRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\HeatMapRepository::class, \App\Repositories\HeatMapRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MasterRepository::class, \App\Repositories\MasterRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\LogRepository::class, \App\Repositories\LogRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\TransactionRepository::class, \App\Repositories\TransactionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BranchRepository::class, \App\Repositories\BranchRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CompanyRepository::class, \App\Repositories\CompanyRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\DenominationRepository::class, \App\Repositories\DenominationRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SegmentRepository::class, \App\Repositories\SegmentRepositoryEloquent::class);
        //:end-bindings:
    }
}

<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Auth\Events\Login' => [
            'App\Listeners\LogSuccessfulLogin',
        ],
        'App\Events\CompanyWasCreated' => [
            'App\Listeners\CompanyWasCreatedListener',
        ],
        'App\Events\AuditableEntityWasCreated' => [
            'App\Listeners\CreateRiskAssessmentListener',
            'App\Listeners\ApplyMainBpListener',
        ],
        'App\Events\EntityMainBpWasCreated' => [
            'App\Listeners\EntityMainBpWasCreatedListener',
        ],
        'App\Events\EntitySubBpWasCreated' => [
            'App\Listeners\SyncToRiskAssessmentListener',
        ],
        'App\Events\EntityMainBpWasDeleted' => [
            'App\Listeners\DeleteInRiskAssessmentListener',
            'App\Listeners\EntityMainBpWasDeletedListener',
        ],
        'App\Events\EntitySubBpWasDeleted' => [
            'App\Listeners\DeleteInRiskAssessmentListener',
            'App\Listeners\DeleteSubBpInMainBpListener',
        ],
        'App\Events\MasterEntityDepartmentWasCreated' => [
            'App\Listeners\ApplyDepartmentToActualListener',
        ],
        'App\Events\MainBpMandayWasUpdated' => [
            'App\Listeners\ApplyMandaysToActualListener',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}

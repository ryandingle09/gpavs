<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Repositories\AuditableEntityRepository;
use App\Criteria\SelectTestProcudureCriteria;

class PopulateProjectScopeApg extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $ae_id;
    private $mbp_id;
    private $project_scope_id;
    private $ae_repo;
    private $user_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($ae_id, $mbp_id , $project_scope_id, $user_id)
    {
        $this->ae_id = $ae_id;
        $this->mbp_id = $mbp_id;
        $this->project_scope_id = $project_scope_id;
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $date = date('Y-m-d');
        $this->ae_repo = app('App\Repositories\AuditableEntitiesActualRepository');
        $this->apg_repo = app('App\Models\ProjectScopeApg');
        $this->ae_repo->pushCriteria(new SelectTestProcudureCriteria());
        $data = $this->ae_repo->findWhere([
                "auditable_entities_actual.auditable_entity_id" => $this->ae_id,
                'mbp.master_ae_mbp_id' => $this->mbp_id
            ]);
        $attributes = array_map(function($value) use ($date){
            return [
                'project_scope_id' => $this->project_scope_id,
                'control_seq' => $value['risk_control_seq'],
                'control_id' => $value['control_id'],
                'control_test_seq' => $value['control_test_seq'],
                'test_proc_id' => $value['test_proc_id'],
                'test_proc_narrative' => $value['test_proc_narrative'],
                'audit_objective' => $value['audit_objective'],
                'auditor_id' => null,
                'budgeted_mandays' => $value['budgeted_mandays'],
                'status' => "New",
                'created_by' => $this->user_id,
                'created_date' => $date,
                'last_update_by' => $this->user_id,
                'last_update_date' => $date,
            ];
        }, $data->toArray());
        $this->apg_repo->insert($attributes);
    }
}

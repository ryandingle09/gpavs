<?php

namespace App\Events;

use App\Events\Event;
use Artesaos\Defender\Permission;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * NewPermissionCreated Class
 * Add the new permission to menu list
 */
class NewPermissionCreated extends Event
{
    use SerializesModels;

    public $permission;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Permission $permission)
    {
        $this->permission = $permission;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}

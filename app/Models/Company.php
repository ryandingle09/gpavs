<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Company extends Model implements Transformable
{
    use TransformableTrait;

    protected $primaryKey 	= 'company_id';
    protected $fillable 	= [
    	'company_code',
    	'company_name',
    	'merchant_name',
    	'tin_number',
    	'depository_account',
    	'created_by',
    	'updated_by',
        'udpated_at'
    ];

    public function branch()
    {
        return $this->hasMany('App\Models\Branch', 'company_id', 'company_id');
    }

}

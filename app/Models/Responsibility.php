<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Responsibility extends Model implements Transformable
{
    use TransformableTrait;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'responsibility_id';
    protected $fillable = [
    	'responsibility_name',
        'menu_id',
    	'description',
    	'effective_start_date',
    	'effective_end_date',
    	'created_by',
    	'last_update_by',
    ];

    public function menu()
    {
    	return $this->hasOne('App\Models\Menu');
    }

    public function userAssignment()
    {
    	return $this->hasMany('App\Models\UserResponsibilityAssignment');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\User', 'last_update_by');
    }

    public function setEffectiveStartDateAttribute($value)
    {
        $this->attributes['effective_start_date'] = date('Y-m-d', strtotime($value) );
    }

    public function setEffectiveEndDateAttribute($value)
    {
        $this->attributes['effective_end_date'] = date('Y-m-d', strtotime($value) );
    }

    public function getEffectiveStartDateAttribute($value)
    {
        return strtotime($value) > 0 ? date('m-d-Y', strtotime($value) ) : null;
    }

    public function getEffectiveEndDateAttribute($value)
    {
        return strtotime($value) > 0 ? date('m-d-Y', strtotime($value) ) : null;
    }

    public function getLastUpdateDateAttribute($value)
    {
        return strtotime($value) > 0 ? date('m-d-Y', strtotime($value) ) : null;
    }
}

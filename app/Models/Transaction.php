<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Transaction extends Model implements Transformable
{
    use TransformableTrait;

    protected $primary_id = 'transaction_id';
    protected $fillable = [
    	'company_id',
    	'branch_id',
    	'transaction_date',
    	'redemption_date',
    	'barcode',
        'denomination',
    	'p_id',
    	'transaction_type',
    	'cashier_id',
    	'approved_by',
    	'approved_date',
    	'attribute_1',
    	'attribute_2',
    	'status',
    	'updated_by',
    	'created_at',
    	'updated_at'
    ];

    public function product()
    {
    	return $this->belongsTo('App\Models\Master', 'p_id', 'p_id');
    }

}

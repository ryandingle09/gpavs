<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class Menu extends Model implements Transformable
{   
    protected $table = "menus";

    use TransformableTrait, Updater;

    const PARENT_ROOT=0;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $fillable = [
        'parent_id',
        'name',
        'is_parent',
        'menu_sequence',
        'url',
        'icon',
        'created_by',
        'last_update_by',
    ];
    protected $dates = [
        'created_date',
        'last_update_date',
    ];

    public function parent() {
        return $this->hasOne('App\Models\Menu', 'id', 'parent_id');
    }

    public function children() {
        return $this->hasMany('App\Models\Menu', 'parent_id', 'id')->with('children')->orderBy('menu_sequence', 'asc');
    }

    /**
     * Get the menu tree
     * @return array
     */
    public static function tree($allowed = array()) {
        // dd($allowed);
        $menus = static::where('parent_id', '=', self::PARENT_ROOT);
        if( count($allowed) ) {
        //     $menus->join('role_menus', 'role_menus.menu_id', '=', 'menus.id')
                $menus->whereIn('menus.id',$allowed);
                // dd($menus->get());
        //           ->groupBy('menus.id');
        }
        // dd($menus->orderBy('menu_sequence', 'asc')->get());

        return $menus->orderBy('menu_sequence', 'asc')->get();
    }


    public function roles()
    {
        return $this->belongsToMany('App\Models\Role','role_menus');
    }

    public function scopePrimary($query)
    {
        return $query->select('menus.id');
    }

    public function getLastUpdateDateAttribute($value)
    {
        return strtotime($value) > 0 ? date('m-d-Y', strtotime($value) ) : null;
    }
}

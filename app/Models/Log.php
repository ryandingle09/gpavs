<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Log extends Model implements Transformable
{
    use TransformableTrait;

    protected $primaryKey = 'log_id';
    protected $fillable = [
    	'transaction_type',
    	'transaction_id',
    	'payment_trans_id',
    	'status',
    	'created_by',
    	'updated_by',
    	'updated_at'
    ];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Segment extends Model implements Transformable
{
    use TransformableTrait;

    protected $primaryKey 	= 'id';
    protected $fillable 	= [
    	'p_id',
    	'segment_name',
    	'from',
    	'to',
    	'created_by',
    	'updated_by',
        'udpated_at'
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Master', 'p_id', 'p_id');
    }

}

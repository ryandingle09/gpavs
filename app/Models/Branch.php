<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Branch extends Model implements Transformable
{
    use TransformableTrait;

    protected $primaryKey 	= 'branch_id';
    protected $fillable 	= [
    	'company_id',
    	'branch_code',
    	'branch_name',
    	'created_by',
    	'updated_by',
        'updated_at'
    ];

    public function company()
    {
    	return $this->belongsTo('App\Models\Company', 'company_id', 'company_id');
    }

}

<?php

namespace App\Models;

use App\Traits\Updater;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Auditor extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'auditor_id';
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'employee_number',
        'nickname',
        'gender',
        'position_code',
        'email_address',
        'supervisor_id',
        'mobile_number',
        'present_address',
        'provincial_address',
        'effective_start_date',
        'effective_end_date',
        'created_by',
        'last_update_by',
    ];

    protected $appends = [
        'full_name'
    ];

    public function setEffectiveStartDateAttribute($value)
    {
        $this->attributes['effective_start_date'] = ymd($value);
    }

    public function setEffectiveEndDateAttribute($value)
    {
        $this->attributes['effective_end_date'] = ymd($value);
    }

    public function getEffectiveStartDateAttribute($value)
    {
        return date_display($value);
    }

    public function getEffectiveEndDateAttribute($value)
    {
        return date_display($value);
    }

    public function getFullNameAttribute($value)
    {
        return preg_replace('/\s+/', ' ',$this->first_name.' '.$this->middle_name.' '.$this->last_name);
    }

    /**
     * Scope for auditor position
     * @param  Model $query
     * @return Model
     */
    public function scopePosAuditor($query)
    {
        return $query->where('position_code', 'Auditor');
    }

    /**
     * Scope for active auditors
     * @param  Model $query
     * @return Model
     */
    public function scopeActive($query)
    {
        return $query->where('effective_end_date', '>', Carbon::now());
    }
}

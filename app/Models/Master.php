<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Master extends Model implements Transformable
{
    use TransformableTrait;

    protected $primaryKey = 'p_id';
    protected $fillable = [
    	'p_type_code',
    	'p_name',
        'denomination',
    	'created_by',
    	'updated_by',
    	'updated_at'
    ];

    public function denomination()
    {
        return $this->hasMany('App\Models\Denomination', 'p_id', 'p_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class UserResponsibilityAssignment extends Model implements Transformable
{
    use TransformableTrait;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $table = 'user_resp_assignments';
    protected $primaryKey = null;
    protected $dates = ['effective_start_date', 'effective_end_date'];
    public $incrementing = false;

    protected $fillable = [
    	'user_id',
    	'responsibility_id',
    	'effective_start_date',
    	'effective_end_date',
    	'created_by',
    	'last_update_by',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function responsibility()
    {
    	return $this->belongsTo('App\Models\responsibility');
    }

    public function user_responsibility()
    {
        return $this->belongsToMany('App\User', 'user_resp_assignments');
    }

    public function setEffectiveStartDateAttribute($value)
    {
        $this->attributes['effective_start_date'] = date('Y-m-d', strtotime($value) );
    }

    public function setEffectiveEndDateAttribute($value)
    {
        $this->attributes['effective_end_date'] = date('Y-m-d', strtotime($value) );
    }

    public function getEffectiveStartDateAttribute($value)
    {
        return strtotime($value) > 0 ? date('m-d-Y', strtotime($value) ) : null;
    }

    public function getEffectiveEndDateAttribute($value)
    {
        return strtotime($value) > 0 ? date('m-d-Y', strtotime($value) ) : null;
    }
}

<?php
namespace App\Traits;

trait Updater
{
    protected static function boot()
    {
        parent::boot();

        /**
         * Auto update created_by and last_update_by when a model create a new record
         */
        static::creating( function($model) {
            $user_id = \Auth::check() ? \Auth::user()->user_id : 0;
            $model->created_by = $user_id;
            $model->last_update_by = $user_id;
        });

        /**
         * Auto update last_update_by when a model update a record
         */
        static::updating( function($model)  {
            $user_id = \Auth::check() ? \Auth::user()->user_id : 0;
            $model->last_update_by = $user_id;
        });

        // static::deleting(function($model)  {
        //     $model->deleted_by = \Auth::user()->user_id;
        //     $model->save();
        // });
    }
}
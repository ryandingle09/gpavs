<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Artesaos\Defender\Permission;
use App\Services\MenuService;

class MenuGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'menu:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate multi-level menu based on permissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(MenuService $service)
    {
        // get all permissions
        $permissions = Permission::all();
        if( $permissions->count() ) {
            foreach ($permissions as $permission) {
                // save exploded route as menu
                $route = explode('.', $permission->name);
                $service->saveMenu($route);
            }
        }

        return "Done generating menus.";
    }
}

<?php
namespace App\Services;

use App\Repositories\RiskAssessmentScaleRepository;
use App\Repositories\RiskAssessmentScaleAnnualAsvRepository;
use App\Models\RiskAssessmentScaleAnnualAsv;
use Auth;

class AdjustedSeverityValueService
{
	protected $year;
	protected $ras_repository;

	public function __construct(
		RiskAssessmentScaleRepository $ras_repository,
		RiskAssessmentScaleAnnualAsvRepository $rasaasv_repository
	)
	{
		$this->year = date('Y');
		$this->ras_repository = $ras_repository;
		$this->rasaasv_repository = $rasaasv_repository;
	}

	public function setYear($year)
	{
		if($year){
			$this->year = $year;
		}
	}

	public function populateAnnualAsv()
	{
		$ras_data = $this->ras_repository->findWhere([ 
			'type' => 'Actual',
			'auditable_entity_id' => 11119
		]);
		// dd($ras_data);
		$date = date('Y-m-d H:i:s');
		RiskAssessmentScaleAnnualAsv::where( 'year', $this->year )->delete();
		$array = [];
		foreach ($ras_data as $ras) {
			$adjusted_severity_value = $this->getTotalAdjustedSevVal(
				$this->year,
				$ras->auditable_entity_id,
				$ras->ae_mbp_id,
				$ras->bp_id,
				$ras->bp_objective_id,
				$ras->bp_steps_id,
				$ras->risk_id,
				$ras->type
			);

			array_push($array, [
				'year' => $this->year,
				'auditable_entity_id' => $ras->auditable_entity_id,
				'ae_mbp_id' => $ras->ae_mbp_id,
				 'bp_id' => $ras->bp_id,
				'bp_objective_id' => $ras->bp_objective_id,
				'bp_steps_id' => $ras->bp_steps_id,
				'risk_id' => $ras->risk_id,
				'adjusted_severity_value' => $adjusted_severity_value,
				'created_by' => Auth::user()->user_id,
				'created_date' => $date,
				'last_update_by' => Auth::user()->user_id,
				'last_update_date' => $date
			]);
		}
		RiskAssessmentScaleAnnualAsv::insert($array);
	}

	/*
	* Copy from adjusted_severity_value() stored procedure 
	* this will get the average adjusted severity value
	*/
	public function getAdjustedSeverityValue($conditions)
	{
		$data = $this->rasaasv_repository->findWhere($conditions);

		if($data->count()){
			if( $data->sum('adjusted_severity_value') )
				return $data->sum('adjusted_severity_value') / $data->count() ;
		}
		return 0;
	}

	/*
	* Copy from total_adjusted_sev_val() stored procedure 
	* get the auditable entity, business process and risk combination adjusted severity value
  	* it can also be used to get individual values for the risk assessment scale
  	* 
  	* 
  	*/
	public function getTotalAdjustedSevVal(
		$plan_year = null,
		$auditable_entity_id = null,
		$ae_mbp_id = null,
		$bp_id = null,
		$bp_objective_id = null,
		$bp_steps_id = null,
		$risk_id = null
	){
		$ras_repository = app(\App\Repositories\RiskAssessmentScaleRepository::class);
		$ae_mbp_repository = app(\App\Repositories\AeMbpActualRepository::class);
		$ras_with_attr = $ras_repository->withFullAttributes([ 
                                'ras.auditable_entity_id' => $auditable_entity_id,
                                'ras.ae_mbp_id' => $ae_mbp_id,
                                'ras.bp_id' => $bp_id,
                                'ras.bp_objective_id' => $bp_objective_id,
                                'ras.bp_steps_id' => $bp_steps_id,
                                'ras.risk_id' => $risk_id,
                                'ras.type' => "Actual",
                                'year' => $plan_year
                            ])->get(); 
		$adjusted_severity_value = $ras_with_attr->sum('adjusted_severity_value');
		$risk_scale_count = $ras_with_attr->count();

		$mbpa = $ae_mbp_repository->find($ae_mbp_id);
		$master_ae_mbp_id = $mbpa->master_ae_mbp_id;


		$cm_percentile_average = $ae_mbp_repository->getAverageCustomMeasures([ 
                                    'master_ae_mbp_id' => $master_ae_mbp_id, 
                                    'auditable_entity_id' => $auditable_entity_id
                                ]);

		return (($adjusted_severity_value / $risk_scale_count) + $cm_percentile_average) / 2;
	}
}
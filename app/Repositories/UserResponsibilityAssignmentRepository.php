<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserResponsibilityAssignmentRepository
 * @package namespace App\Repositories;
 */
interface UserResponsibilityAssignmentRepository extends RepositoryInterface
{
    //
}

<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AuditorRepository
 * @package namespace App\Repositories;
 */
interface AuditorRepository extends RepositoryInterface
{
    //
}

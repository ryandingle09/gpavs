<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MasterRepository;
use App\Models\Master;
use App\Validators\MasterValidator;

/**
 * Class MasterRepositoryEloquent
 * @package namespace App\Repositories;
 */
class MasterRepositoryEloquent extends BaseRepository implements MasterRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Master::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}

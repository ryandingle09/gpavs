<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\DenominationRepository;
use App\Models\Denomination;
use App\Validators\DenominationValidator;

/**
 * Class DenominationRepositoryEloquent
 * @package namespace App\Repositories;
 */
class DenominationRepositoryEloquent extends BaseRepository implements DenominationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Denomination::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}

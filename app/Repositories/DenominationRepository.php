<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DenominationRepository
 * @package namespace App\Repositories;
 */
interface DenominationRepository extends RepositoryInterface
{
    //
}

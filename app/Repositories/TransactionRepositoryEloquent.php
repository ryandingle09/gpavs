<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\TransactionRepository;
use App\Models\Transaction;
use App\Validators\TransactionValidator;
use DB;

/**
 * Class TransactionRepositoryEloquent
 * @package namespace App\Repositories;
 */
class TransactionRepositoryEloquent extends BaseRepository implements TransactionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Transaction::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getCounter($date)
    {
        return DB::table('transactions')
            ->join('masters', 'masters.p_id', '=', 'transactions.p_id')
            ->select(DB::raw('COUNT(gp_transactions.p_id) as count, SUM(gp_transactions.denomination) as total'))
            ->where('transactions.redemption_date', $date)
            ->orWhere(DB::raw("gp_transactions.status != 'approved'"))
            ->get()[0];
    }

    public function updateManual($values, $where)
    {
        return DB::table('transactions')
            ->where($where)
            //->where('status', 'incomplete')
            ->update($values);

    }
}

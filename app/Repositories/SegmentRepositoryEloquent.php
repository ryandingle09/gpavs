<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\SegmentRepository;
use App\Models\Segment;
use App\Validators\SegmentValidator;

/**
 * Class SegmentRepositoryEloquent
 * @package namespace App\Repositories;
 */
class SegmentRepositoryEloquent extends BaseRepository implements SegmentRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Segment::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getIndex($where)
    {
        return Segment::where($where)->orderBy('id','desc')->first();
    }
}

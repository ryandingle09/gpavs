<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TransactionRepository
 * @package namespace App\Repositories;
 */
interface TransactionRepository extends RepositoryInterface
{
    public function getCounter($date);
    public function updateManual($values, $where);
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MenuItemsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            // 'icon' => 'required',
            'url' => 'required_without:is_parent',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Menu title is required.',
            'url.required_without' => 'URL is required.'
        ];
    }
}

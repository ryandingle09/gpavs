<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Repositories\MasterRepository;

class MasterRequest extends Request
{
    public $master;

    public function __construct(MasterRepository $master)
    {
        $this->master = $master;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data   = $this->master->findWhere(['p_id' => $this->route('id')], ['p_type_code']);
        $unique = (isset($data[0]->p_type_code) && $data[0]->p_type_code == Request::input('p_type_code')) ? '' : '|unique:masters';

        if($this->route('type') == 'product')
        { 
            return [
                'p_type_code'   => 'required|max:10'.$unique.'',
                'p_name'        => 'required|max:50'
            ];
        }
        else return [];
        
    }

    public function messages()
    {
        return [
            'p_type_code.required'  => 'Product Type Code field is required.',
            'p_type_code.unique'    => 'Product Type Code has already been taken.',
            'p_type_code.max'       => 'Product Type Code max input cannot be greater than 10.',
            'p_name.required'       => 'Product Name field is required.',
            'p_name.max'            => 'Product Name max input cannot be greater than 50.',
        ];
    }
}

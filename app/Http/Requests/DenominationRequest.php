<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DenominationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->route('type') == 'denomination')
        { 
            return [
                'amount'          => 'required|numeric',
            ];
        }
        else return [];
    }

    public function messages()
    {
        return [
            'amount.required'      => 'Denomination field is required.',
            'amount.numeric'       => 'Denomination must be a number.',
        ];
    }
}

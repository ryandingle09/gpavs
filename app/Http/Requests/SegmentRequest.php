<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SegmentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->route('type') == 'segment')
        { 
            return [
                'segment_name'          => 'required',
                'from'                  => 'required|numeric',
                'to'                    => 'required|numeric',
            ];
        }
        else return [];
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserManagementRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|unique:users,user_name,'.$this->get('uid').',user_id',
            'emp_id' => 'required|integer|unique:users,employee_id,'.$this->get('uid').',user_id',
            'start_date' => 'required|date|before_equal:end_date',
            'end_date' => 'required|date',
            'resp.item.*' => 'required',
            'resp.start_date.*' => 'required|date',
            'resp.end_date.*' => 'required|date',
        ];
    }

    public function messages()
    {
        return [
            'emp_id.required' => 'Employee ID is required.',
            'emp_id.integer' => 'Employee ID must be number.',
            'emp_id.unique' => 'Employee ID already exists.',
            'start_date.before_equal' => 'Start date must be less than or equal End date.',
            'resp.item.*.required' => 'Responsiblity name is required.',
            'resp.start_date.*.required' => 'Responsiblity start date is required.',
            'resp.end_date.*.required' => 'Responsiblity end date is required.',
            'resp.start_date.*.date' => 'Responsiblity start date must be a date.',
            'resp.end_date.*.date' => 'Responsiblity end date must be a date.',
        ];
    }
}

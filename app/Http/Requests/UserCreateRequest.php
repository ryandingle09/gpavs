<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_tag = $this->get('user_tag');
        $rules = [
            'user_name' => "required|unique:users,user_name,NULL,user_id",
            'display_name' => "required",
            'password' => "required",
            'effective_start_date' => "required|date|before_equal:effective_end_date",
            'effective_end_date' => "required|date",
        ];
        return $rules;
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Repositories\CompanyRepository;

class CompanyRequest extends Request
{
    public $company;

    public function __construct(CompanyRepository  $company)
    {
        $this->company = $company;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        $data   = $this->company->findWhere(['company_id' => $this->route('id')], ['company_code']);
        $unique = (isset($data[0]->company_code) && $data[0]->company_code == Request::input('company_code')) ? '' : '|unique:companies';

        if($this->route('type') == 'company')
        { 
            return [
                'company_code'          => 'required|max:10'.$unique.'',
                'company_name'          => 'required|max:50',
                'merchant_name'         => 'required|max:50',
                'tin_number'            => 'required|max:20',
                'depository_account'    => 'required|max:20',
            ];
        }
        else return [];
    }
}

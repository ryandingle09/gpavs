<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = $this->segment(2) ? $this->segment(2) : "NULL";
        $rules = [
            'user_name' => "required|unique:users,user_name,".$user_id.",user_id",
            'display_name' => "required",
            'effective_start_date' => "required|date|before_equal:effective_end_date",
            'effective_end_date' => "required|date",
        ];
        return $rules;
    }
}

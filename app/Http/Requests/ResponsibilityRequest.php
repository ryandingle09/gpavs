<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ResponsibilityRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:responsibilities,responsibility_name,'.$this->get('id').',responsibility_id',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
        ];
    }
}

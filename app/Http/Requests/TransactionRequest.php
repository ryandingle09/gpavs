<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TransactionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id'        => 'required',
            'branch_id'         => 'required',
            'redemption_date'   => 'required|date',
            'p_id'              => 'required',
            'barcode'           => 'required|unique:transactions',
        ];
    }

    public function messages()
    {
        return [
            'company_id.required'   => 'The Company field is required',
            'branch_id.required'    => 'The Branch field is required',
            'p_id.required'         => 'The Product field is required',
            'barcode.required'      => 'The Barcode field id required',
            'barcode.unique'        => 'Please use unique barcode',
            'denomination.numeric'  => 'Denomination must be a number',
        ];
    }
}

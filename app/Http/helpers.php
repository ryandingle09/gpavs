<?php
if (! function_exists('global_config')) {
    /**
     * Get a GlobalConfigService::getData result.
     *
     * @return App\Services\GlobalConfigService::getData
     */
    function global_config($column)
    {
        return app('App\Services\GlobalConfigService')->getData($column);
    }
}

if (! function_exists('menu')) {
    /**
     * Get a MenuService instance.
     *
     * @return App\Services\MenuService
     */
    function menu()
    {
        return app('App\Services\MenuService');
    }
}

if (! function_exists('getSidebarMenu')) {
    /**
     * Get user's menu.
     *
     * @return array
     */
    function getSidebarMenu()
    {
        return menu()->getUserMenu();
    }
}

if (! function_exists('ymd')) {

    function ymd($string){
        return $string && $string != '0000-00-00' ? date('Y-m-d',strtotime($string)) : null;
    }

}

if (! function_exists('mjy')) {

    function mjy($string){
        return $string && $string != '0000-00-00' ? date('M j. Y',strtotime($string)) : null;
    }

}

if (! function_exists('date_display')) {

    function date_display($string){
        return $string && $string != '0000-00-00' ? date( config('app.date_display') ,strtotime($string) ) : null;
    }

}

if (! function_exists('datetime_display')) {

    function datetime_display($string){
        return $string && $string != '0000-00-00' ? date( config('app.datetime_display') ,strtotime($string)) : null;
    }

}

if (! function_exists('time_display')) {

    function time_display($string){
        return $string ? date(config('app.time_display'),strtotime($string)) : null;
    }
}

if (! function_exists('lookup_url')) {
    /**
     * Get a MenuService instance.
     * @param Based on the config.iapms.lookups $path
     * @return Generated url of selected lookup type
     */
    function lookup_url($path)
    {
        return url('administrator/lookup/edit/'.config($path));
    }
}

if (! function_exists('percentile')) {
    /**
     * Get a MenuService instance.
     * @param Based on the config.iapms.lookups $path
     * @return Generated url of selected lookup type
     */
    function percentile( $rank, $total_items )
    {
        return ( ( 100 * ( (int) $rank - 0.5 ) ) / $total_items);
    }
}

if (! function_exists('percentile_rank')) {
    /**
     * Description here
     * @param Based on the config.iapms.lookups $path
     * @return Generated url of selected lookup type
     */
    function percentile_rank( $data_set, $value, $highscore_type)
    {
        $data_set = array_unique( $data_set);
        $total_items = count($data_set);
        if( $highscore_type == 'Floor'){
            sort($data_set);
            $rank = array_search($value, $data_set) + 1;
            return percentile($rank,$total_items);
        }else{
            rsort($data_set);
            $rank = array_search($value, $data_set) + 1;
            return percentile($rank,$total_items);
        }
    }
}

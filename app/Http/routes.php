<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web'] ], function () {

    // Authentication Start
    Route::get('login', ['as' => 'auth.index',  'uses' => 'AuthController@index']);
    Route::post('login', ['as' => 'auth.login',  'uses' => 'AuthController@login']);
    Route::get('logout', ['as' => 'auth.logout',  'uses' => 'AuthController@logout']);
    // Authentication End

    // Session Manager Start
    Route::post('session/check-idle', ['uses' => 'SessionController@checkIdle', 'as' => 'session.check_idle']);
    // Session Manager End

    // Authenticated Routes
    Route::group(['middleware' => [ 'auth' , 'reset_last_activity' ] ], function(){
        // Administrators Start
        Route::get('/', ['as' => 'dashboard',  'uses' => 'DashboardController@index']);
        // Administrators End

        Route::group(['prefix' => 'administrator'], function(){

            # Users : don not use this group anymore
            Route::group(['prefix' => 'users'], function(){
                Route::get('/', ['as' => 'administrator.users',  'uses' => 'UserManagementController@index']);
                Route::post('/', ['as' => 'administrator.users.store',  'uses' => 'UserManagementController@store']);
                Route::get('/create', ['as' => 'administrator.users.create',  'uses' => 'UserManagementController@create']);
                Route::get('/edit/{username}', ['as' => 'administrator.users.edit',  'uses' => 'UserManagementController@edit']);
                Route::put('/update/{username}', ['as' => 'administrator.users.update',  'uses' => 'UserManagementController@update']);
                Route::get('/list', ['as' => 'administrator.users.list',  'uses' => 'UserManagementController@getUserList']);
                Route::delete('/delete/{id}', ['as' => 'administrator.users.delete',  'uses' => 'UserManagementController@destroy']);
            });

            # roles
            Route::group(['prefix' => 'roles'], function(){
                Route::get('/', ['as' => 'administrator.roles',  'uses' => 'RolesController@index']);
                Route::post('/', ['as' => 'administrator.roles.store',  'uses' => 'RolesController@store']);
                Route::get('/create', ['as' => 'administrator.roles.create',  'uses' => 'RolesController@create']);
                Route::get('/list', ['as' => 'administrator.roles.list',  'uses' => 'RolesController@getRoleList']);
                Route::get('/edit/{name}', ['as' => 'administrator.roles.edit',  'uses' => 'RolesController@edit']);
                Route::put('/update/{name}', ['as' => 'administrator.roles.update',  'uses' => 'RolesController@update']);
                Route::delete('/delete/{id}', ['as' => 'administrator.roles.delete',  'uses' => 'RolesController@destroy']);
            });

            # permissions
            Route::group(['prefix' => 'permissions'], function(){
                Route::get('/', ['as' => 'administrator.permissions',  'uses' => 'PermissionsController@index']);
                Route::post('/', ['as' => 'administrator.permissions.store',  'uses' => 'PermissionsController@store']);
                Route::get('/list/{role?}', ['as' => 'administrator.permissions.list',  'uses' => 'PermissionsController@getPermissionList']);
                Route::put('/update/{id}', ['as' => 'administrator.permissions.update',  'uses' => 'PermissionsController@update']);
                Route::delete('/delete/{id}', ['as' => 'administrator.permissions.delete',  'uses' => 'PermissionsController@destroy']);
            });

            # Responsibilities
            Route::group(['prefix' => 'responsibility'], function(){
                Route::get('/', ['as' => 'administrator.responsibility',  'uses' => 'ResponsibilityController@index']);
                Route::post('/', ['as' => 'administrator.responsibility.store',  'uses' => 'ResponsibilityController@store']);
                Route::get('/create', ['as' => 'administrator.responsibility.create', 'uses' => 'ResponsibilityController@create']);
                Route::get('/list', ['as' => 'administrator.responsibility.list',  'uses' => 'ResponsibilityController@getresponsibilityList']);
                Route::get('/edit/{name}', ['as' => 'administrator.responsibility.edit',  'uses' => 'ResponsibilityController@edit']);
                Route::put('/update/{name}', ['as' => 'administrator.responsibility.update',  'uses' => 'ResponsibilityController@update']);
                Route::delete('/delete/{id}', ['as' => 'administrator.responsibility.delete',  'uses' => 'ResponsibilityController@destroy']);
            });

            # Menus
            Route::group(['prefix' => 'menu'], function(){
                Route::get('/', ['as' => 'administrator.menu',  'uses' => 'MenuController@index']);
                Route::post('/', ['as' => 'administrator.menu.store',  'uses' => 'MenuController@store']);
                Route::get('/create', ['as' => 'administrator.menu.create',  'uses' => 'MenuController@create']);
                Route::get('/edit/{id}', ['as' => 'administrator.menu.edit',  'uses' => 'MenuController@edit']);
                Route::put('/update/{id}', ['as' => 'administrator.menu.update',  'uses' => 'MenuController@update']);
                Route::delete('/delete/{id}', ['as' => 'administrator.menu.delete',  'uses' => 'MenuController@destroy']);
                Route::put('/update-hierarchy', ['as' => 'administrator.menu.update_hierarchy',  'uses' => 'MenuController@updateHierarchy']);
                # Functions
                Route::group(['prefix' => 'function'], function(){
                    Route::get('/', ['as' => 'administrator.menu.function',  'uses' => 'FunctionController@index']);
                    Route::post('/', ['as' => 'administrator.menu.function.store',  'uses' => 'FunctionController@store']);
                    Route::get('/create', ['as' => 'administrator.menu.function.create',  'uses' => 'FunctionController@create']);
                    Route::get('/edit/{name}', ['as' => 'administrator.menu.function.edit',  'uses' => 'FunctionController@edit']);
                    Route::put('/update/{name}', ['as' => 'administrator.menu.function.update',  'uses' => 'FunctionController@update']);
                    Route::get('/list', ['as' => 'administrator.menu.function.list',  'uses' => 'FunctionController@getFunctionList']);
                });

                # Global config
                
            });
        });
        // End Administrator

        // User Start
        Route::group(['prefix' => 'user'], function(){
            Route::get('list', ['as' => 'user.list',  'uses' => 'UserController@list']);
            Route::get('create', ['as' => 'user.create',  'uses' => 'UserController@create']);
            Route::post('/', ['as' => 'user.store',  'uses' => 'UserController@store']);
            Route::get('/', ['as' => 'user.index',  'uses' => 'UserController@index']);
            Route::get('edit/{id}', ['as' => 'user.edit',  'uses' => 'UserController@edit']);
            Route::put('{id}', ['as' => 'user.update',  'uses' => 'UserController@update']);
            Route::delete('{id}', ['as' => 'data.destroy',  'uses' => 'UserController@destroy']);
        });
        // User End

        // Company Branch Setup Start
        Route::group(['prefix' => 'cb_setup'], function(){
            Route::get('/', ['as' => 'data.index',  'uses' => 'CBSetupController@index']);
            Route::post('{type}', ['as' => 'data.store',  'uses' => 'CBSetupController@store']);
            Route::put('{id}/{type}', ['as' => 'data.update',  'uses' => 'CBSetupController@update']);
            Route::get('create/{type}/{id}', ['as' => 'data.create',  'uses' => 'CBSetupController@create']);
            Route::get('edit/{id}/{type}', ['as' => 'data.edit',  'uses' => 'CBSetupController@edit']);
            Route::get('show/{id}/{type}', ['as' => 'data.show',  'uses' => 'CBSetupController@show']);
            Route::get('list/{type}', ['as' => 'data.list',  'uses' => 'CBSetupController@list']);
            Route::get('list/{type}/{id}', ['as' => 'data.list',  'uses' => 'CBSetupController@list']);
            Route::delete('{id}/{type}', ['as' => 'data.destroy',  'uses' => 'CBSetupController@destroy']);
        });
        // Company Branch Setup End

        // Product Start
        Route::group(['prefix' => 'product'], function(){
            Route::get('/', ['as' => 'product.index',  'uses' => 'ProductController@index']);
            Route::post('/{type}', ['as' => 'product.store',  'uses' => 'ProductController@store']);
            Route::put('/{id}/{type}', ['as' => 'product.update',  'uses' => 'ProductController@update']);
            Route::get('create/{type}/{id}', ['as' => 'product.create',  'uses' => 'ProductController@create']);
            Route::get('edit/{id}/{type}', ['as' => 'product.edit',  'uses' => 'ProductController@edit']);
            Route::get('show/{id}/{type}', ['as' => 'product.show',  'uses' => 'ProductController@show']);
            Route::get('list/{type}', ['as' => 'product.list',  'uses' => 'ProductController@list']);
            Route::get('list/{type}/{id}', ['as' => 'product.list',  'uses' => 'ProductController@list']);
            Route::delete('{id}/{type}', ['as' => 'product.destroy',  'uses' => 'ProductController@destroy']);
        });
        // Product End

        // Dashboard Items Start
        Route::group(['prefix' => 'dashboard'], function(){
            Route::get('transaction/list', ['as' => 'trans.list',  'uses' => 'DashboardController@list']);
            Route::post('transaction', ['as' => 'trans.create',  'uses' => 'DashboardController@store']);
            Route::delete('transaction/{id}', ['as' => 'trans.delete',  'uses' => 'DashboardController@destroy']);
            Route::get('dropdown/{type}/{id}', ['as' => 'trans.list',  'uses' => 'DashboardController@dropdown']);
            Route::get('transaction/counter', ['as' => 'trans.counter',  'uses' => 'DashboardController@counter']);
            Route::post('transaction/approval', ['as' => 'trans.approval',  'uses' => 'DashboardController@approval']);
        });
        // Dashboard Items End

    });
    // Authenticated Routes
});

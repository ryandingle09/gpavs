<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\ResponsibilityRequest;
use App\Models\Responsibility;
use App\Repositories\MenuRepository;
use App\Repositories\ResponsibilityRepository;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class ResponsibilityController extends Controller
{
	const VIEW_PATH = 'administrator.user_management';

	private $responsibility;

	function __construct( ResponsibilityRepository $responsibility )
	{
		$this->responsibility = $responsibility;
	}

    public function index()
    {
    	return view(self::VIEW_PATH.'.responsibility');
    }

    public function store(ResponsibilityRequest $request)
    {
    	$attributes = [
    		'responsibility_name' => $request->get('name'),
    		'description' => $request->get('description'),
    		'menu_id' => $request->get('menu'),
    		'effective_start_date' => $request->get('start_date'),
    		'effective_end_date' => $request->get('end_date'),
    		'created_by' => \Auth::user()->user_id,
    		'last_update_by' => \Auth::user()->user_id,
    	];

    	$new = $this->responsibility->create($attributes);

    	$request->session()->flash('message', 'New responsibility has been created!');

    	return response()->json(['success' => true]);
    }

    public function create(MenuRepository $menu)
    {
    	$menus = $menu->all();

    	return view(self::VIEW_PATH.'.forms.responsibility', compact('menus'));
    }

    public function edit($name, Request $request, MenuRepository $menu)
    {
    	$details = $this->responsibility->findByField(['responsibility_name' => $name])->first();
    	$menus = $menu->all();
    	// check if $name responsibility exists, if not redirect to create page
    	if( is_null($details) ) {
    		$request->session()->flash('message', 'Responsibility "'.$name.'" does not exists! You can create it here.');

    		return redirect(route('administrator.responsibility.create'));
    	}

    	return view(self::VIEW_PATH.'.forms.responsibility', compact('details', 'menus'));
    }

    public function update(ResponsibilityRequest $request, $name, MenuRepository $menu)
    {
    	$responsibility = $this->responsibility->findByField(['responsibility_name' => $name])->first();

    	// check if $name responsibility exists, if not redirect to create page
    	if( is_null($responsibility) ) {
    		$request->session()->flash('message', 'Responsibility "'.$name.'" does not exists! You can create it here.');

    		return redirect(route('administrator.responsibility.create'));
    	}

    	$attributes = [
    		'responsibility_name' => $request->get('name'),
    		'description' => $request->get('description'),
    		'menu_id' => $request->get('menu'),
    		'effective_start_date' => $request->get('start_date'),
    		'effective_end_date' => $request->get('end_date'),
    		'last_update_by' => \Auth::user()->user_id,
    	];

    	$update = $responsibility->update($attributes);

    	$request->session()->flash('message', $request->get('name').' has been updated!');

    	return response()->json(['success' => true]);
    }

    /**
     * Get all the responisbilty
     * @return Datatables
     */
    public function getresponsibilityList()
    {
    	$responsiblities = Responsibility::with(['updatedBy' => function($q){
    		$q->select(['user_id', 'user_name']);
    	}])->get();
    	return Datatables::of($responsiblities)->make(true);
    }
}

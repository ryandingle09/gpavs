<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Yajra\Datatables\Datatables;
use App\Repositories\MasterRepository;
use App\Repositories\DenominationRepository;
use App\Repositories\SegmentRepository;
use App\Http\Requests;
use App\Http\Requests\MasterRequest;
use App\Http\Requests\DenominationRequest;
use App\Http\Requests\SegmentRequest;
use Auth;

class ProductController extends Controller
{
    const VIEW_PATH = 'product';

    public $masterRepository;
    public $denominationRepository;

    public function __construct(MasterRepository $masterRepository, DenominationRepository $denominationRepository, SegmentRepository $segmentRepository)
    {
        $this->masterRepository         = $masterRepository;
        $this->denominationRepository   = $denominationRepository;
        $this->segmentRepository        = $segmentRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'user' => Auth::id()
        ];
        return view(self::VIEW_PATH.'/index', $data);
    }

    public function list($type, $id = null)
    {
        $data = $this->masterRepository->all();
        switch ($type) {
            case 'denomination':
                $data = $this->denominationRepository->findWhere(['p_id' => $id]);
                break;
            case 'segment':
                $data = $this->segmentRepository->findWhere(['p_id' => $id]);
                break;
            default:
                $data = $this->masterRepository->all();
                break;
        }
        
        return Datatables::of($data)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type, $id = null)
    {
        $data = [
            'type'  => $type, 
            'user'  => Auth::id(),
            'id'    => $id
        ];
        return view(self::VIEW_PATH.'/modal/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MasterRequest $masterRequest, DenominationRequest $denominationRequest, SegmentRequest $segmentRequest, $type)
    {
        $data = [];

        if($type == 'product')
        {
            $data = $this->masterRepository->create($masterRequest->except('_token'));
        }
        else if($type == 'segment')
        {
            /*checking duplication process of from and to*/
            $check  = $this->segmentRepository->findWhere(['p_id' => $segmentRequest->input('p_id')]);

            foreach($check as $segments)
            {                    
                $count   = sizeof($this->segmentRepository->findWhere(['p_id' => $segmentRequest->input('p_id'), 'from' => $segmentRequest->input('from'), 'to' => $segmentRequest->input('to')]));
                if($count !== 0)
                    return response()->json(['from' => ['From Segment has already been used on this product'], 'to' => ['To Segment has already been used on this product']], 422);
            }

            /*checking if from is greater less to "to value" in the last inserted segment*/
            $check2 = $this->segmentRepository->getIndex(['p_id' => $segmentRequest->input('p_id')]);

            if(isset($check2->to) && $check2->to >= $segmentRequest->input('from'))
            {
                return response()->json(['from' => ['From Segment must be greater than to '.$check2->to.'']], 422);
            }

            if($segmentRequest->input('from') >= $segmentRequest->input('to'))
            {
                return response()->json(['to' => ['To Segment must be greater than to From Segment. ']], 422);
            }

            $data = $this->segmentRepository->create($segmentRequest->except('_token'));
        }
        else
        {
            $count   = sizeof($this->denominationRepository->findWhere(['p_id' => $denominationRequest->input('p_id'), 'amount' => number_format($denominationRequest->input('amount'), 2)]));
            if($count !== 0)
            {
                return response()->json(['amount' => ['Denomination has already been used on this product']], 422);
            }

            $data = $this->denominationRepository->create($denominationRequest->except('_token'));
        }
        
        return response()->json(['message' => 'Successfully Added', 'data' => $data], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $type)
    {
        $item = [];

        switch ($type) {
            case 'denomination':
                $item = $this->denominationRepository->with(['product'])->findWhere(['id' => $id]);
                break;
            case 'segment':
                $item = $this->segmentRepository->with(['product'])->findWhere(['id' => $id]);
                break;
            default:
                $item = $this->masterRepository->with(['denomination'])->findWhere(['p_id' => $id]);
                break;
        }

        $data = [
            'type' => $type, 
            'user' => Auth::id(),
            'data' => $item[0],
        ];

        return view(self::VIEW_PATH.'/modal/view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $type)
    {
        
        $item = [];

        switch ($type) {
            case 'denomination':
                $item = $this->denominationRepository->with(['product'])->findWhere(['id' => $id]);
                break;
            case 'segment':
                $item = $this->segmentRepository->with(['product'])->findWhere(['id' => $id]);
                break;
            default:
                $item = $this->masterRepository->with(['denomination'])->findWhere(['p_id' => $id]);
                break;
        }

        $data = [
            'type' => $type, 
            'user' => Auth::id(),
            'data' => $item[0],
        ];
        return view(self::VIEW_PATH.'/modal/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MasterRequest $masterRequest, DenominationRequest $denominationRequest, SegmentRequest $segmentRequest, $id, $type)
    {
        $data = [];

        if($type == 'product')
        {
            $data = $this->masterRepository->update($masterRequest->except('_token'), $id);
        }
        else if($type == 'segment')
        {
            /*checking duplication process of from and to*/
            $check  = $this->segmentRepository->findWhere(['id' => $id]);

            if($check[0]->from != $segmentRequest->input('from') && $check[0]->to != $segmentRequest->input('to'))
            {
                $count   = sizeof($this->segmentRepository->findWhere(['p_id' => $segmentRequest->input('p_id'), 'from' => $segmentRequest->input('from'), 'to' => $segmentRequest->input('to')]));
                if($count !== 0)
                    return response()->json(['from' => ['From Segment has already been used on this product'], 'to' => ['To Segment has already been used on this product']], 422);
                
            }

            /*checking if from is greater less to "to value" in the last or previous inserted segment*/
            $previous   = $this->segmentRepository->getIndex(['p_id' => $segmentRequest->input('p_id'), ['id','<', $id]]);
            $next       = $this->segmentRepository->getIndex(['p_id' => $segmentRequest->input('p_id'), ['id','>', $id]]);

            if($previous)
            {
                if($previous->to >= $segmentRequest->input('from'))
                    return response()->json(['from' => ['From Segment must be greater than to '.$previous->to.'.']], 422);
            }

            if($next)
            {
                if($next->from <= $segmentRequest->input('to'))
                    return response()->json(['from' => ['To Segment must be less than to '.$next->from.'.']], 422);
            }

            if($segmentRequest->input('from') >= $segmentRequest->input('to'))
            {
                return response()->json(['to' => ['To Segment must be greater than to From Segmen. ']], 422);
            }

            $data = $this->segmentRepository->update($masterRequest->except('_token'), $id);
        }
        else
        {
            $check  = $this->denominationRepository->findWhere(['id' => $id]);

            if($check[0]->amount !== number_format($denominationRequest->input('amount'), 2))
            {
                $count  = sizeof($this->denominationRepository->findWhere(['p_id' => $denominationRequest->input('p_id'), 'amount' => $denominationRequest->input('amount')]));
                if($count !== 0)
                    return response()->json(['amount' => ['Denomination has already been used on this product']], 422);
            }
            
            $data = $this->denominationRepository->update($denominationRequest->except('_token'), $id);
        }
        
        return response()->json(['message' => 'Successfully Updated', 'data' => $data], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $type)
    {
        switch ($type) {
            case 'denomination':
                $this->denominationRepository->delete($id);
                break;
            case 'segment':
                $this->segmentRepository->delete($id);
                break;
            default:
                $this->masterRepository->delete($id);
                $this->denominationRepository->deleteWhere(['p_id' => $id]);
                $this->segmentRepository->deleteWhere(['p_id' => $id]);
                break;
        }

        return response()->json(['message' => 'Successfully Deleted.'], 200);    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Role;
use App\Models\Permission;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Requests\RoleRequest;

class RolesController extends Controller
{
    const VIEW_PATH = 'administrator.user_management';

    public function index()
    {
        return view( self::VIEW_PATH.'.roles' );
    }

    public function store(RoleRequest $request)
    {
        $permissions = $request->get('permissions');
        $menus = $request->get('menus');
        $role = new Role;
        $role->name = $request->get('name');
        $role->save();
        $role->permissions()->attach($permissions);
        $role->menus()->attach($menus);
        menu()->clearCache();
        $request->session()->flash('success_message', 'Role has been created.');
        return redirect()->route('administrator.roles.edit', [$role->id]);
    }

    public function create()
    {
        $user_menus = [];
        $permissions = Permission::all();
        return view(self::VIEW_PATH.'.forms.role',compact('permissions','user_menus'));
    }

    public function edit($id)
    {
        $permissions = Permission::all();
        $details = Role::where('id', $id)->first();
        $user_permissions = $details->permissions->pluck('id')->toArray();
        $user_menus = $details->menus->pluck('id')->toArray();
        return view(self::VIEW_PATH.'.forms.role', compact('details', 'user_permissions', 'permissions','user_menus'));
    }

    public function update($id, RoleRequest $request)
    {
        $permissions = $request->get('permissions');
        $menus = $request->get('menus');
        $role = Role::find($id);
        $role->name = $request->get('name');
        $role->save();
        $role->permissions()->detach();
        $role->permissions()->attach($permissions);
        $role->menus()->detach();
        $role->menus()->attach($menus);
        menu()->clearCache();
        $request->session()->flash('success_message', 'Role has been updated.');
        return redirect()->route('administrator.roles');
    }

    public function destroy($id, Request $request)
    {
        $role = Role::find($id);
        $name = $role->name;
        $role->permissions()->detach();
        $role->delete();
        $request->session()->flash('success_message', $name.' has been deleted.');
        return response()->json(['success' => true]);
    }

    public function getRoleList()
    {
        return Datatables::of(Role::query())->make(true);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Yajra\Datatables\Datatables;
use App\Repositories\CompanyRepository;
use App\Repositories\BranchRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\MasterRepository;
use App\Repositories\DenominationRepository;
use App\Repositories\SegmentRepository;
use App\Repositories\LogRepository;
use App\Http\Requests\TransactionRequest;
use Auth;

class DashboardController extends Controller
{
    const VIEW_PATH = 'dashboard';

    public $companyRepository;
    public $branchRepository;
    public $logRepository;
    public $transaction;
    public $masterRepository;
    public $denominationRepository;

    public function __construct(CompanyRepository $companyRepository, BranchRepository $branchRepository, TransactionRepository $transactionRepository, MasterRepository $masterRepository, LogRepository $logRepository, DenominationRepository $denominationRepository, SegmentRepository $segmentRepository)
    {
        $this->companyRepository        = $companyRepository;
        $this->branchRepository         = $branchRepository;
        $this->transactionRepository    = $transactionRepository;
        $this->masterRepository         = $masterRepository;
        $this->logRepository            = $logRepository;
        $this->denominationRepository   = $denominationRepository;
        $this->segmentRepository        = $segmentRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'user' => Auth::id()
        ];
        return view(self::VIEW_PATH.'/index', $data);
    }

    public function list(Request $request)
    {
        $data = $this->transactionRepository->with(['product'])->findWhere([
            'redemption_date' => ymd($request->get('redemption_date')),
            ['status','!=','approved']
        ]);
        return Datatables::of($data)->make(true);
    }

    public function dropdown($type, $id = null)
    {
        $data = [];

        switch ($type) {
            case 'branch':
                $data = $this->branchRepository->findWhere(['company_id' => $id]);
                break;
            case 'product':
                $data = $this->masterRepository->all();
                break;
            default:
                $data = $this->companyRepository->all();
                break;
        }

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransactionRequest $request)
    {

        $product        = $request->input('p_id');
        $item           = [
            'barcode'           => $request->input('barcode'),
            'company_id'        => $request->input('company_id'),
            'branch_id'         => $request->input('branch_id'),
            'cashier_id'        => $request->input('cashier_id'),
            'transaction_date'  => date('Y-m-d'),
            'p_id'              => $product,
            'status'            => 'incomplete',
            'redemption_date'   => ymd($request->input('redemption_date'))
        ];

        //checking the barcode denomination by segmentation
        $segments = $this->segmentRepository->findWhere(['p_id' => $product]);

        $count = 0;

        //loop segments then match in the barcode numbers and length
        foreach($segments as $segment)
        {
            $string = " ".$request->input('barcode');
            $diff   = abs($segment->from - $segment->to) + 1; 
            $amount = substr($string, $segment->from, $diff);

            $find   = sizeof($this->denominationRepository->findWhere(['p_id' => $product, 'amount' => $amount]));

            if($find !== 0)
            {
                $item['denomination'] = $amount;
                $count ++;
            }
        }

        if($count == 0)
        {
            return response()->json(['barcode' => ['Denomination amount does not exist on the selected product']], 422);
        }

        $data = $this->transactionRepository->create($item);
        return response()->json(['message' => 'Successfully Added.', 'data'=> $data], 200);
    }

    public function counter(Request $request)
    {
        $redemption_date    = ymd($request->get('redemption_date'));
        $counter            = $this->transactionRepository->getCounter($redemption_date);
        return response()->json($counter ,200);
    }

    public function approval(Request $request)
    {
        $redemption_date = ymd($request->input('redemption_date'));
        $counter         = $this->transactionRepository->getCounter($redemption_date);
        $status          = [];

        if($counter->count == 0)
        {
            $status = [
                'status'  => 'info',
                'message' => 'No submitted item for approval.'
            ];
        }
        else
        {
            $this->transactionRepository->updateManual(
                ['status'           => 'approval'],
                ['redemption_date'  => $redemption_date, 'status' => 'incomplete']
            );

            $status = [
                'status'  => 'success',
                'message' => 'Transactions successfully submitted for APPROVAL.'
            ];
        }

        return response()->json($status, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->transactionRepository->deleteWhere(['transaction_id' => $id]);
        return response()->json(['message' => 'Successfully Deleted.'], 200);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

class SessionController extends Controller
{
    public function checkIdle(Request $request)
    {
    	/*
         * TODO abstract this logic away to the domain
         */

        // Configuration convert to seconds
    	$max_idle_before_logout = config('session.lifetime') * 60;
        $max_idle_before_warning = $max_idle_before_logout - 60;
        // Calculate the number of seconds since the use's last activity
        $idle_time = date('U') - $request->session()->get('last_active');
        $is_warning_displayed = $request->session()->get('is_warning_displayed');
    	$warning_time = $max_idle_before_logout - $max_idle_before_warning;
        $time_remaining = $max_idle_before_logout - $idle_time;
        $time_data = [
            'time_remaining' => $time_remaining,
            'logout_time' => (int) $max_idle_before_logout,
            'warning_time' => $max_idle_before_warning,
            'idle_time' => $idle_time,
            'is_warning_displayed' => $is_warning_displayed
        ];
        // Warn user they will be logged out if idle for too long
    	if ($idle_time > $max_idle_before_warning && !$is_warning_displayed ) {
    		$request->session()->set('is_warning_displayed', true);
            $data = array_merge([ 
                'status' => 1,
                'message' => "Session will be expired in <span> ".$time_remaining." </span> seconds."
            ],$time_data);
    		return response()->json($data);
    	}
        // Log out user if idle for too long
    	if ($idle_time > $max_idle_before_logout && $is_warning_displayed ) {
            Auth::logout();
    		$request->session()->set('is_warning_displayed', false);
            $data = array_merge([ 
                'status' => 2,
                'message' => "Session expired."
            ],$time_data);
            return response()->json($data);
    	}
        // User is still active
        $data = array_merge([ 
            'status' => 0,
            'message' => $is_warning_displayed ? "Will be logged out soon." : "User is still active."
        ],$time_data);
        return response()->json($data);
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\PermissionRequest;
use App\Services\MenuService;
use App\Models\Role;
use App\Models\Permission;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class PermissionsController extends Controller
{
    const VIEW_PATH = 'administrator.user_management';

    public function index(MenuService $menu_service)
    {
        $permissions = Permission::all()->pluck('id','name');
        $permission_array = $menu_service->convertDotToArray($permissions);
        // dd($permission_array);
        return view( self::VIEW_PATH.'.permissions', compact('permission_array') );
    }

    public function store(PermissionRequest $request)
    {
        $attributes = [
            'name' => $request->get('name'),
            'readable_name' => $request->get('description'),
        ];
        $permission = Permission::create($attributes);

        $request->session()->flash('success_message', 'New permission has been created!');

        return response()->json(['success' => true]);
    }

    public function update($id, Request $request)
    {
        $permission = Permission::find($id);

        $attributes = [
            'name' => $request->get('name'),
            'readable_name' => $request->get('description'),
        ];

        $permission->update($attributes);

        $request->session()->flash('message', $request->get('description').' has been updated!');

        return response()->json(['success' => true]);
    }

    public function destroy($id, Request $request)
    {
        $permission = Permission::find($id);
        $name = $permission->name;
        $permission->delete();

        $request->session()->flash('success_message', $name.' has been deleted!');

        return response()->json(['success' => true]);
    }

    public function getPermissionList($role = null)
    {
        if( !is_null($role) ) {
            $role = Role::find($role);
            $permissions = !is_null($role) ? $role->permissions : Permission::query();
        }
        else {
            $permissions = Permission::query();
        }

        return Datatables::of($permissions)->make(true);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Yajra\Datatables\Datatables;
use App\Http\Requests\CompanyRequest;
use App\Http\Requests\BranchRequest;
use App\Repositories\CompanyRepository;
use App\Repositories\BranchRepository;
use Auth;

class CBSetupController extends Controller
{
    const VIEW_PATH = 'CBSetup';

    public $companyRepository;
    public $branchRepository;

    public function __construct(CompanyRepository $companyRepository, BranchRepository $branchRepository)
    {
        $this->companyRepository        = $companyRepository;
        $this->branchRepository         = $branchRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(self::VIEW_PATH.'/index');
    }

    public function list($type, $id = null)
    {
        $data = $this->companyRepository->all();
        switch ($type) {
            case 'branch':
                $data = $this->branchRepository->findWhere(['company_id' => $id]);
                break;
            default:
                $data = $this->companyRepository->all();
                break;
        }
        
        return Datatables::of($data)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type, $id = null)
    {
        $data = [
            'type'          => $type, 
            'user'          => Auth::id(),
            'company_id'    => $id
        ];
        return view(self::VIEW_PATH.'/modal/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $companyRequest, BranchRequest $branchRequest, $type)
    {
        $data = [];

        if($type == 'company')
            $data = $this->companyRepository->create($companyRequest->except('_token'));
        else
            $data = $this->branchRepository->create($branchRequest->except('_token'));
        
        return response()->json(['message' => 'Successfully Added', 'data' => $data], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $type)
    {
        $item = ($type == 'company') ? $this->companyRepository->findWhere(['company_id' => $id]) : $this->branchRepository->with(['company'])->findWhere(['branch_id' => $id]);
        $data = [
            'type' => $type, 
            'user' => Auth::id(),
            'data' => $item[0],
        ];
        return view(self::VIEW_PATH.'/modal/view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $type)
    {
        $item = ($type == 'company') ? $this->companyRepository->findWhere(['company_id' => $id]) : $this->branchRepository->findWhere(['branch_id' => $id]);
        $data = [
            'type' => $type, 
            'user' => Auth::id(),
            'data' => $item[0],
        ];
        return view(self::VIEW_PATH.'/modal/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $companyRequest, BranchRequest $branchRequest, $id, $type)
    {
        $data = [];

        if($type == 'company')
            $data = $this->companyRepository->update($companyRequest->except('_token'), $id);
        else
            $data = $this->branchRepository->update($branchRequest->except('_token'), $id);
        
        return response()->json(['message' => 'Successfully Updated', 'data' => $data], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $type)
    {
        switch ($type) {
            case 'branch':
                $this->branchRepository->delete($id);
                break;
            default:
                $this->companyRepository->delete($id);
                $this->branchRepository->deleteWhere(['company_id' => $id]);
                break;
        }

        return response()->json(['message' => 'Successfully Deleted.'], 200);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class ResetLastActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->session()->set('last_active', date('U'));
        $request->session()->set('is_warning_displayed', false);
        return $next($request);
    }
}

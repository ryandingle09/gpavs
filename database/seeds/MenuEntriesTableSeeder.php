<?php

use Illuminate\Database\Seeder;

class MenuEntriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menu_entries')->delete();
        
        \DB::table('menu_entries')->insert(array (
            0 => 
            array (
                'menu_id' => 1,
                'permission_id' => 4,
            ),
            1 => 
            array (
                'menu_id' => 1,
                'permission_id' => 5,
            ),
            2 => 
            array (
                'menu_id' => 1,
                'permission_id' => 6,
            ),
            3 => 
            array (
                'menu_id' => 1,
                'permission_id' => 7,
            ),
            4 => 
            array (
                'menu_id' => 1,
                'permission_id' => 8,
            ),
            5 => 
            array (
                'menu_id' => 1,
                'permission_id' => 9,
            ),
            6 => 
            array (
                'menu_id' => 1,
                'permission_id' => 27,
            ),
            7 => 
            array (
                'menu_id' => 1,
                'permission_id' => 28,
            ),
            8 => 
            array (
                'menu_id' => 1,
                'permission_id' => 29,
            ),
            9 => 
            array (
                'menu_id' => 1,
                'permission_id' => 30,
            ),
            10 => 
            array (
                'menu_id' => 1,
                'permission_id' => 31,
            ),
            11 => 
            array (
                'menu_id' => 1,
                'permission_id' => 32,
            ),
            12 => 
            array (
                'menu_id' => 1,
                'permission_id' => 33,
            ),
            13 => 
            array (
                'menu_id' => 1,
                'permission_id' => 34,
            ),
            14 => 
            array (
                'menu_id' => 1,
                'permission_id' => 35,
            ),
            15 => 
            array (
                'menu_id' => 5,
                'permission_id' => 10,
            ),
            16 => 
            array (
                'menu_id' => 5,
                'permission_id' => 11,
            ),
            17 => 
            array (
                'menu_id' => 5,
                'permission_id' => 12,
            ),
            18 => 
            array (
                'menu_id' => 5,
                'permission_id' => 51,
            ),
            19 => 
            array (
                'menu_id' => 5,
                'permission_id' => 52,
            ),
            20 => 
            array (
                'menu_id' => 5,
                'permission_id' => 55,
            ),
            21 => 
            array (
                'menu_id' => 5,
                'permission_id' => 57,
            ),
            22 => 
            array (
                'menu_id' => 5,
                'permission_id' => 59,
            ),
            23 => 
            array (
                'menu_id' => 5,
                'permission_id' => 61,
            ),
            24 => 
            array (
                'menu_id' => 10,
                'permission_id' => 42,
            ),
            25 => 
            array (
                'menu_id' => 1,
                'permission_id' => 62,
            ),
            26 => 
            array (
                'menu_id' => 15,
                'permission_id' => 63,
            ),
            27 => 
            array (
                'menu_id' => 18,
                'permission_id' => 65,
            ),
            28 => 
            array (
                'menu_id' => 19,
                'permission_id' => 66,
            ),
            29 => 
            array (
                'menu_id' => 21,
                'permission_id' => 68,
            ),
            30 => 
            array (
                'menu_id' => 20,
                'permission_id' => 69,
            ),
            31 => 
            array (
                'menu_id' => 20,
                'permission_id' => 67,
            ),
            32 => 
            array (
                'menu_id' => 5,
                'permission_id' => 70,
            ),
            33 => 
            array (
                'menu_id' => 26,
                'permission_id' => 72,
            ),
            34 => 
            array (
                'menu_id' => 25,
                'permission_id' => 71,
            ),
            35 => 
            array (
                'menu_id' => 27,
                'permission_id' => 73,
            ),
            36 => 
            array (
                'menu_id' => 28,
                'permission_id' => 74,
            ),
            37 => 
            array (
                'menu_id' => 30,
                'permission_id' => 76,
            ),
            38 => 
            array (
                'menu_id' => 31,
                'permission_id' => 77,
            ),
            39 => 
            array (
                'menu_id' => 32,
                'permission_id' => 78,
            ),
            40 => 
            array (
                'menu_id' => 33,
                'permission_id' => 79,
            ),
        ));
        
        
    }
}
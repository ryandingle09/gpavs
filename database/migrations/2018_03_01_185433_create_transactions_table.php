<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transactions', function(Blueprint $table) {
            $table->bigIncrements('transaction_id');
            $table->bigInteger('company_id');
            $table->bigInteger('branch_id');
            $table->date('transaction_date');
            $table->date('redemption_date');
            $table->string('barcode', 20);
            $table->bigInteger('p_id');
            $table->bigInteger('cashier_id')->nullable()->comment('for DS');
            $table->bigInteger('approved_by')->nullable();
            $table->date('approved_date', 50)->nullable();
            $table->string('attribute_1', 20)->nullable();
            $table->string('attribute_2', 20)->nullable();
            $table->string('status', 20)->nullable()->default('NULL')->comment('incomplete / approval / approved / rejected');
            $table->bigInteger('updated_by')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transactions');
	}

}

/*This script is dependent to the following library
* --JQuery
* --Sweet Alert : http://t4t5.github.io/sweetalert/
* --Selectize : http://selectize.github.io/selectize.js/
* the above dependencies should be placed above of this script.
*/

/* Set up default ajax function for all ajax calls
* Source : https://api.jquery.com/jquery.ajaxsetup/
* Please refer to the source above to override these set up
*/
var base_url = $("meta[name='base_url']").attr('content');
var token = $("meta[name='token']").attr('content');
var rates = { 1 : 'green', 2 : 'yellow-green' , 3 : 'yellow' , 4 : 'orange2' , 5 : 'red' };
var rate_class = 'star-on-png';
var setRatingColors = function(val, over) {
    last_rate = $(this).attr('data-id');
    rate_class = rates[val];

    if( over && last_rate > 0) {
        rate_class = last_rate;
    }
    $(this).find('.star-on-png,.star-half-png').addClass(rate_class).removeClass('grey');
    $(this).find('.star-off-png').removeClass(rate_class).addClass('grey');
};

$.ajaxSetup({
    beforeSend: function (xhr)
    {
        xhr.setRequestHeader("X-CSRF-TOKEN",token);      
        $(".has-error").removeClass('has-error');
        // $(".alert").remove();
    },
    error: function (xhr) {
        switch(parseInt(xhr.status)){
            case 401: 
                $.sError("Session expired. Please refresh the page.");
                break;
            case 422:
                $(".alert").remove();
                $(".has-error").removeClass('has-error');
                swal.close();
                var lastKey = null;
                html = '<div class="alert alert-warning"> <button type="button" class="close" data-dismiss="alert"> <i class="ace-icon fa fa-times"></i> </button>';
                $.each(xhr.responseJSON,function(k,v){
                    lastKey = k;
                    html += '<strong> <i class="ace-icon fa fa-times bigger-110 red"></i> </strong>'+v[0]+'<br>';
                    $('[name="'+k+'"]').parents(".form-group").addClass('has-error');
                });
                html += '</div>';
                // console.log(lastKey); // uncomment this if it is not displaying error
                $('[name="'+lastKey+'"]').parents('form').before(html);
                break;
            default:
                $.sError("Error "+xhr.status+" : "+xhr.statusText);
            break;
        }
    }
});

$.LoadingOverlaySetup({
    // color           : "rgba(0, 0, 0, 0.4)",
    maxSize         : "40px",
    minSize         : "20px",
    resizeInterval  : 0,
    size            : "50%",
    zIndex : 10
});

/* Statement to remove alert message of datatable
* Source : https://datatables.net/reference/option/%24.fn.dataTable.ext.errMode
*/
if(typeof($.fn.dataTable) != "undefined"){
    $.fn.dataTable.ext.errMode = 'none';
}

// dymamic modal serverside
$(document).on('click', ".open-modal" ,function(e){
    e.preventDefault();
    var dis     = $(this)
    var route   = $(this).attr('data-route');
    var param1  = dis.data('param1') ? dis.data('param1') : '';
    var param2  = dis.data('param2') ? dis.data('param2') : '';
    var param3  = dis.data('param3') ? dis.data('param3') : '';
    var modal   = dis.data('modal');
    var size    = dis.data('size') ? dis.data('size') : '';

    var html = '<div class="modal fade '+size+'" data-backdrop="static" id="'+modal+'">';
        html += '<div class="modal-dialog modal-md">';
        html += '<div class="modal-content">';
        html += '<div class="modal-body">';
        html += '<h3 class="text-center">Loading ....</h3>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';

    var clear = '<div class="modal-body">';
        clear += '<h3 class="text-center">Loading ....</h3>';
        clear += '</div>';

    if($('#'+modal).length == 0){
        $('body').append(html);
        $('#'+modal).modal('show');
    }else{
        $('#'+modal+' .modal-content').html(clear);
        $('#'+modal).modal('show');
    }

    $.get(route, {'param1': param1, 'param2': param2, 'param3':param3}, function(response){
        $('#'+modal+' .modal-content').html(response);
    });
});

// attach datepicker on dynamic form
$('body').on('focus', ".date-picker", function(){
    $(this).datepicker({
        format: "M-dd-yyyy",
        startDate: 'today',
        keyboardNavigation: false,
        autoclose : true,
        todayHighlight: true,
    });
});
// This statements manage the active class based on the current url
$(".nav.nav-list a").each(function() {   
    currentUrl =location.protocol + '//' + location.host + location.pathname;
    if (this.href == currentUrl) {
        $(this).parent().addClass("active");
        $(this).parents('li').addClass("open");
        $(this).parents('.submenu').addClass("nav-show").show();
    }
});
// adjust the datatable style
$('.dataTables_wrapper table').parent().css('padding', '0');
$('.dataTables_wrapper table').css('width', '100%');
$('body').tooltip({selector: '[rel=tooltip]'}); 


function load_datatable(datatable, url)
{
    if(url != undefined){
        datatable.ajax.url(url).load();
    }else{
        datatable.ajax.reload(null,false);
    }
}


function toggle_loading() {
    if( $('.mask').hasClass('ajax') ) {
        $('.mask').removeClass('ajax');
    }
    else {
        $('.mask').addClass('ajax');
    }
}

function sAlert(title,text){
    swal({
      title: title,
      text:  text,
      type : 'info'
    });
}

function sSuccess(text,title,callBack){
    title = (typeof title === 'undefined') ? "Success!" : title ;
    swal({
      title: title,
      text:  text,
      type : 'success'
    },callBack);
}

function sInfo(text,callBack){
    text = (typeof text === 'undefined') ? "Lorem ipsum dolor" : text ;
    swal({
        title: "Note",
        text: text,
        type: "info",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },callBack);
}

function sWarning(text,callBack){
    text = (typeof text === 'undefined') ? "Are you sure about this changes?" : text ;
    swal({
        title: "Continue?",
        text: text,
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },callBack);
}

function sLoading(text){
    text = (typeof text === 'undefined') ? "Please wait while we are connecting to server." : text ;
    swal({
        title: "Loading...",
        text: text,
        type: "info",
        showConfirmButton: false,
        showLoaderOnConfirm: true,
    });
}

function sError(text){
    text = (typeof text === 'undefined') ? "Cannot display error." : text ;
    swal({
        title: "Oops! Something went wrong.",
        text:  text,
        type : 'error'
    });
}

// JQUERY EXTENSION

$.fn.showError = function(errorResponse,alertWrapperElement){
    return this.each(function(){
        form = $(this);
        form.find('.has-error').removeClass('has-error');
        alertHtml = '<div class="alert alert-warning"> <button type="button" class="close" data-dismiss="alert"> <i class="ace-icon fa fa-times"></i> </button>';
        $.each(errorResponse,function(k,v){
            alertHtml += '<strong> <i class="ace-icon fa fa-times bigger-110 red"></i> </strong>'+v[0]+'<br>';
            form.find('[name="'+k+'"]').parents('.form-group').addClass('has-error');
        });
        alertHtml += '</div>';
        alertWrapperElement.html(alertHtml);
    });
}

$.fn.showSuccess = function(message){
    return this.each(function(){
        wrapper = $(this);
        alertHtml = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"> <i class="ace-icon fa fa-times"></i> </button><strong> <i class="ace-icon fa fa-check bigger-110 green"></i> </strong>'+message+'<br></div>';
        wrapper.html(alertHtml);
    });
}

$.fn.showInfo = function(message){
    return this.each(function(){
        wrapper = $(this);
        alertHtml = '<div class="alert alert-info"> <button type="button" class="close" data-dismiss="alert"> <i class="ace-icon fa fa-times"></i> </button><strong> <i class="ace-icon fa fa-info bigger-110 blue"></i> </strong>'+message+'<br></div>';
        wrapper.html(alertHtml);
    });
}

$.fn.scroll = function(options){
    var settings = $.extend({
        adjustment : 0,
    }, options );
    return this.each(function(){
        var element = $(this);
        $('html,body').animate({
            scrollTop: ( $(element).offset().top + settings.adjustment)
        });
    });
};

$.fn.supply = function(data){
    return this.each(function(){
        var form = $(this);
        data = Object.flatten(data);
        $.each(data,function(k,v){
            var element = form.find('[name="'+k+'"]');
            var type = element.attr('type');
            switch(type){
                case 'checkbox':
                        element.prop('checked', element.val() == v);
                    break;
                case 'radio':
                    element.filter('[value="'+v+'"]').attr('checked', 'checked');
                    break;
                default:
                    element.val(v);
            }   
        });
    });
};

$.fn.resetModalHeight = function(){
    var backdrop = this.find(".modal-backdrop");
    var dialog = this.find(".modal-dialog"); 
    final_height = dialog.height() + 60;
    if(final_height > $(window).height()){
        backdrop.height(dialog.height() + 60);
    }
}

$.fn.makeLabel = function(defaultValue){
    var defaultValue = (typeof defaultValue === 'undefined') ? "n/a" : defaultValue ;
    return this.each(function(){
        var element = $(this);
        var value = element.val() ? element.val() : defaultValue;
        var html = '<span class="label label-xlg label-primary label-wrap arrowed" >' + value +'</span>';
        element.hide();
        element.siblings('.arrowed').remove();
        element.before(html);
    });
};

$.fn.deleteItemFrom = function(datatable, datatable2, datatable3){
    var url = $(this).attr("href");
    $.sWarning("Are you sure you want to delete this item?",function(){
        $.ajax({
            url : url,
            method : "delete",
            success : function(response){
                if(datatable != undefined){
                    datatable.ajax.reload( null, false );
                }
                if(datatable2 != undefined){
                    datatable2.ajax.reload( null, false );
                }
                if(datatable3 != undefined){
                   datatable3.ajax.reload( null, false );
                }
                $.sSuccess(response.message);
                // $(".alert-wrapper").aSuccess(response.message);
                // sSuccess(response.message);
            }
        });
    });
}

$.fn.aError = function(text){
    return this.each(function(){
        var wrapper = $(this);
        var html = '<div class="alert alert-warning"> <button type="button" class="close" data-dismiss="alert"> <i class="fa fa-times"></i> </button><strong> <i class="fa fa-exclamation-triangle red"></i> </strong>'+text+'<br></div>';
        $(this).html(html);
    });
}

$.fn.aSuccess = function(text){
    var html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"> <i class="fa fa-times"></i> </button><strong> <i class="fa fa-check"></i> </strong>'+text+'<br></div>';
    $(this).html(html);
}

$.fn.aFormErrors = function(errorResponse,form){
    return this.each(function(){
        alertWrapperElement = $(this);
        form.find('.has-error').removeClass('has-error');
        alertHtml = '<div class="alert alert-warning"> <button type="button" class="close" data-dismiss="alert"> <i class="ace-icon fa fa-times"></i> </button>';
        $.each(errorResponse,function(k,v){
            alertHtml += '<strong> <i class="ace-icon fa fa-times bigger-110 red"></i> </strong>'+v[0]+'<br>';
            form.find('[name="'+k+'"]').parents('.form-group').addClass('has-error');
        });
        alertHtml += '</div>';
        alertWrapperElement.html(alertHtml);
    });
}

$.sSuccess = function(text,callback,title){
    title = (typeof title === 'undefined') ? "Success!" : title ;
    swal({
      title: title,
      text:  text,
      type : 'success'
    },callback)
}

$.sWarning = function(text,callback,title){
    title = (typeof title === 'undefined') ? "Continue?" : title ;
    swal({
        title: title,
        text: text,
        type: "warning",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    },callback);
};

$.sError = function(text,callback,title){
    title = (typeof title === 'undefined') ? "Oops!" : title ;
    swal({
        title: title,
        text: text,
        type: "error",
        showCancelButton: false,
    },callback);
}

$.sInfo = function(text,callback,title){
    title = (typeof title === 'undefined') ? "Note" : title ;
    test = swal({
        title: title,
        text: text,
        type: "info",
        showCancelButton: false,
    },callback);
    console.log(test);
}

$.fn.modalFormErrorHandler = function(xhr){
    var commons = $(this).attr('id').replace("-form", "");;
    switch(parseInt(xhr.status)){
        case 422:
            swal.close();
            alertWrapper = $("#"+commons+"-alert");
            $("#"+commons+"-form").showError(xhr.responseJSON,alertWrapper);
            $("#"+commons+"-modal").resetModalHeight();
            break;
        default:
            $.sError("Error "+xhr.status+" : "+xhr.statusText);
            break;
    }
}

$.fn.formErrorHandler = function(xhr){
    var commons = $(this).attr('id').replace("-form", "");;
    switch(parseInt(xhr.status)){
        case 422:
            swal.close();
            alertWrapper = $("#"+commons+"-alert");
            $("#"+commons+"-form").showError(xhr.responseJSON,alertWrapper);
            break;
        case 500:
            $.sError("Error "+xhr.status+" : "+xhr.statusText);
        break;
    }
}

$.gSuccess = function(text,callback,title){
    title = (typeof title === 'undefined') ? "Success!" : title ;
    $.bootstrapGrowl(text, {
        type: 'success',
        allow_dismiss: true
    });
}

Object.flatten = function(data) {
    var result = {};
    function recurse (cur, prop) {
        if (Object(cur) !== cur) {
            result[prop] = cur;
        } else if (Array.isArray(cur)) {
             for(var i=0, l=cur.length; i<l; i++)
                 recurse(cur[i], prop + "[" + i + "]");
            if (l == 0)
                result[prop] = [];
        } else {
            var isEmpty = true;
            for (var p in cur) {
                isEmpty = false;
                recurse(cur[p], prop ? prop+"_"+p : p);
            }
            if (isEmpty && prop)
                result[prop] = {};
        }
    }
    recurse(data, "");
    return result;
}

$.sDelay = function(callback , delay){
    sLoading();
    delay = (typeof delay === 'undefined') ? 1000 : delay ;
    setTimeout(function(){
        swal.close();
        callback();
    },delay);
}

$.fn.clearForm = function(){
    $(".alert").remove();
    $(".has-error").removeClass('has-error');
    this[0].reset();
}

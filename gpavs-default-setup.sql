-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 21, 2018 at 01:08 AM
-- Server version: 5.7.9-log
-- PHP Version: 7.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gpavs`
--

create database `gpavs`;
use `gpavs`;

-- --------------------------------------------------------

--
-- Table structure for table `gp_branches`
--

CREATE TABLE `gp_branches` (
  `branch_id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `branch_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `branch_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gp_branches`
--

INSERT INTO `gp_branches` (`branch_id`, `company_id`, `branch_code`, `branch_name`, `updated_by`, `created_by`, `created_at`, `updated_at`) VALUES
(15, 1, '1425', 'MMSM - Megacenter', NULL, 1, NULL, NULL),
(16, 1, '0241', 'MMSM - Sta. Rosa', NULL, 1, NULL, NULL),
(17, 2, '1090', 'MSDD - General Santos', NULL, 1, NULL, NULL),
(18, 2, '1063', 'MSDD - Lanang', NULL, 1, NULL, NULL),
(19, 3, '0095', 'MSD - Davao', NULL, 1, NULL, NULL),
(20, 3, '0032', 'MSC - Cebu', NULL, 1, NULL, NULL),
(21, 4, '0352', 'MSCC - Consolacion', NULL, 1, NULL, NULL),
(22, 4, '1663', 'MSCC - Seaside Cebu', NULL, 1, NULL, NULL),
(23, 5, '0001', 'SM Mart - Cubao', NULL, 1, NULL, NULL),
(24, 5, '0004', 'SM Mart - Harrison', NULL, 1, NULL, NULL),
(25, 5, '0000', 'SM Mart - Head Office', NULL, 1, NULL, NULL),
(26, 5, '0002', 'SM Mart - Quiapo', NULL, 1, NULL, NULL),
(27, 6, '0000', 'SM Retail - Head Office', NULL, 1, NULL, NULL),
(28, 7, '1132', 'AMC - Aura', NULL, 1, NULL, NULL),
(29, 7, '0142', 'AMC - Bacolod', NULL, 1, NULL, NULL),
(30, 7, '0014', 'AMC - Bacoor', NULL, 1, NULL, NULL),
(31, 7, '0138', 'AMC - Baguio', NULL, 1, NULL, NULL),
(32, 7, '0445', 'AMC - Baliwag', NULL, 1, NULL, NULL),
(33, 8, '0000', 'ATP - Head Office', NULL, 1, NULL, NULL),
(34, 9, '0000', 'HHC Head Office', NULL, 1, NULL, NULL),
(35, 10, '0000', 'SMCO - Head Office', NULL, 1, NULL, NULL),
(36, 11, '0000', 'SSMI - Head Office', NULL, 1, NULL, NULL),
(37, 7, '0000', 'SVI - Head Office', NULL, 1, NULL, NULL),
(38, 12, '0000', 'Forever - Head Office', NULL, 1, NULL, NULL),
(39, 13, '0000', 'HMS Head Office', NULL, 1, NULL, NULL),
(40, 14, '0000', 'PFRD Head Office', NULL, 1, NULL, NULL),
(41, 16, '0000', 'PGES Head Office', NULL, 1, NULL, NULL),
(42, 17, '0000', 'RHC - Head Office', NULL, 1, NULL, NULL),
(43, 18, '0009', 'WKEZ DS Megamall', NULL, 1, NULL, NULL),
(44, 19, '0148', 'AMC - Batangas', NULL, 1, NULL, NULL),
(45, 19, '0245', 'AMC - Bay City', NULL, 1, NULL, NULL),
(46, 19, '1204', 'AMC - BF Paranaque', NULL, 1, NULL, NULL),
(47, 19, '0065', 'AMC - Bicutan', NULL, 1, NULL, NULL),
(48, 19, '1296', 'AMC - Cabanatuan', NULL, 1, NULL, NULL),
(49, 19, '0120', 'AMC - Cagayan de Oro', NULL, 1, NULL, NULL),
(50, 19, '0275', 'AMC - Calamba', NULL, 1, NULL, NULL),
(51, 19, '1267', 'AMC - Cauayan', NULL, 1, NULL, NULL),
(52, 19, '0032', 'AMC - Cebu', NULL, 1, NULL, NULL),
(53, 19, '0238', 'AMC - Clark', NULL, 1, NULL, NULL),
(54, 19, '0352', 'AMC - Consolacion', NULL, 1, NULL, NULL),
(55, 19, '0000', 'AMC - Head Office', NULL, 1, NULL, NULL),
(56, 19, '0001', 'AMC - Cubao', NULL, 1, NULL, NULL),
(57, 20, '0148', 'MSP - Batangas', NULL, 1, NULL, NULL),
(58, 20, '0120', 'MSP - Cagayan De Oro', NULL, 1, NULL, NULL),
(59, 20, '0094', 'MSP - Pampanga', NULL, 1, NULL, NULL),
(60, 20, '1896', 'MSP - San Jose Del Monte', NULL, 1, NULL, NULL),
(61, 20, '0276', 'MSP - Taytay', NULL, 1, NULL, NULL),
(62, 21, '0142', 'MSB - Bacolod', NULL, 1, NULL, NULL),
(63, 21, '0014', 'MSB - Bacoor', NULL, 1, NULL, NULL),
(64, 21, '0138', 'MSB - Baguio', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gp_companies`
--

CREATE TABLE `gp_companies` (
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `company_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `merchant_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tin_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `depository_account` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` bigint(20) DEFAULT '1',
  `created_by` bigint(20) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gp_companies`
--

INSERT INTO `gp_companies` (`company_id`, `company_code`, `company_name`, `merchant_name`, `tin_number`, `depository_account`, `updated_by`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '005', 'Metro Manila Shopping Mecca Corp.', 'DS', '139', '139005', 1, 1, NULL, NULL),
(2, '292', 'Mindanao Shoppers Daily Destination Corp.', 'DS', '141', '141292', 1, 1, NULL, NULL),
(3, '084', 'Multi-Stores Corporation', 'DS', '145', '145084', 1, 1, NULL, NULL),
(4, '290', 'My Shoppinglane Cebu, Corp', 'DS', '146', '146290', 1, 1, NULL, NULL),
(5, '101', 'SM Mart, Inc.', 'DS', '149', '149101', 1, 1, NULL, NULL),
(6, '102', 'SM Retail, Inc.', 'DS', '154', '154102', 1, 1, NULL, NULL),
(7, '239', 'Accessories Management Corporation', 'DS', '177', '177239', 1, 1, NULL, NULL),
(8, '311', 'Alfamart Trading Philippines, Inc.', 'DS', '160', '160311', 1, 1, NULL, NULL),
(9, '276', 'Hyperhome Corp.', 'DS', '161', '161276', 1, 1, NULL, NULL),
(10, '142', 'Sanford_Marketing Corporation', 'DS', '162', '162142', 1, 1, NULL, NULL),
(11, '2', 'Super Shopping Market, Inc.', 'DS', '163', '1632', 1, 1, NULL, NULL),
(12, '007', 'Supervalue, Inc.', 'DS', '164', '164007', 1, 1, NULL, NULL),
(13, '267', 'Forever Agape & Glory, Inc.', 'DS', '167', '167267', 1, 1, NULL, NULL),
(14, '132', 'HMS Development Corporation', 'DS', '168', '168132', 1, 1, NULL, NULL),
(15, '314', 'Premium Fashion Retail Designs, Inc.', 'DS', '170', '170314', 1, 1, NULL, NULL),
(16, '317', 'Premium Global Essences Stores, Inc.', 'DS', '171', '171317', 1, 1, NULL, NULL),
(17, '184', 'Russfield Holdings Corp.', 'DS', '172', '172184', 1, 1, NULL, NULL),
(18, '324', 'Walk EZ Retail Corp.', 'DS', '173', '173324', 1, 1, NULL, NULL),
(19, '239', 'Accessories Management Corporation', 'DS', '182', '182239', 1, 1, NULL, NULL),
(20, '049', 'Madison Shopping Plaza, Inc.', 'DS', '90', '90049', 1, 1, NULL, NULL),
(21, '074', 'Mainstream Business, Inc.', 'DS', '97', '97074', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gp_controls_master`
--

CREATE TABLE `gp_controls_master` (
  `control_id` int(10) UNSIGNED NOT NULL,
  `control_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `control_code` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `component_code` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `principle_code` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `focus_code` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `assertions` longtext COLLATE utf8_unicode_ci NOT NULL,
  `control_types` longtext COLLATE utf8_unicode_ci NOT NULL,
  `key_control_flag` varchar(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `frequency` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mode` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `control_source` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attribute1` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attribute2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attribute3` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attribute4` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attribute5` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attribute6` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attribute7` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attribute8` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attribute9` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attribute10` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_date` date NOT NULL,
  `last_update_by` int(10) UNSIGNED NOT NULL,
  `last_update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gp_controls_master`
--

INSERT INTO `gp_controls_master` (`control_id`, `control_name`, `control_code`, `component_code`, `principle_code`, `focus_code`, `assertions`, `control_types`, `key_control_flag`, `frequency`, `mode`, `control_source`, `attribute1`, `attribute2`, `attribute3`, `attribute4`, `attribute5`, `attribute6`, `attribute7`, `attribute8`, `attribute9`, `attribute10`, `created_by`, `created_date`, `last_update_by`, `last_update_date`) VALUES
(1, 'Supervisory Control', 'PPFA.C-001', '["CE"]', '{"CE":["P.3"]}', '{"P.3":["F.11"]}', 'a:1:{i:0;s:23:"Existence or Occurrence";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'N', NULL, 'Manual', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(2, 'Asset Requisition Documentation', 'PPFA.C-002', '["CA"]', '{"CA":["P.12"]}', '{"P.12":["F.49"]}', 'a:2:{i:0;s:23:"Existence or Occurrence";i:1;s:19:"Rights & Obligation";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'N', NULL, 'Manual', 'SOP 7.1.4.1 Asset Management Policies ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(3, 'Approved Budget', 'PPFA.C-003', '["RA"]', '{"RA":["P.6a"]}', '{"P.6a":["F.23"]}', 'a:2:{i:0;s:19:"Rights & Obligation";i:1;s:23:" Valuation & Allocation";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'N', NULL, 'Manual', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(4, 'Bidding Proposal Solicitation', 'PPFA.C-004', '["CE"]', '{"CE":["P.3"]}', '{"P.3":["F.11"]}', 'a:2:{i:0;s:19:"Rights & Obligation";i:1;s:23:" Valuation & Allocation";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'N', NULL, 'Manual', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(5, 'Selection and Approval Matrix', 'PPFA.C-005', '["MA"]', '{"MA":["P.16"]}', '{"P.16":["F.71"]}', 'a:2:{i:0;s:19:"Rights & Obligation";i:1;s:23:" Valuation & Allocation";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'N', NULL, 'Manual', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(6, 'PO Creation and Releasing', 'PPFA.C-006', '["CA"]', '{"CA":["P.11"]}', '{"P.11":["F.46"]}', 'a:1:{i:0;s:19:"Rights & Obligation";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'N', NULL, 'Manual, System - SAP', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(7, 'PO Checking and Item Receiving', 'PPFA.C-007', '["CA"]', '{"CA":["P.12"]}', '{"P.12":["F.41"]}', 'a:3:{i:0;s:12:"Completeness";i:1;s:20:" Rights & Obligation";i:2;s:23:" Valuation & Allocation";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'N', NULL, 'Manual', 'SOP 7.1.4.1 Asset Management Policies ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(8, 'Asset Inventory List (AIL)', 'PPFA.C-008', '["CA"]', '{"CA":["P.11"]}', '{"P.11":["F.44"]}', 'a:2:{i:0;s:12:"Completeness";i:1;s:23:" Valuation & Allocation";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'N', NULL, 'System - SAP', 'SOP 7.1.4.1 Asset Management Policies ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(9, 'Asset Barcode Sticker', 'PPFA.C-009', '["CA"]', '{"CA":["P.12"]}', '{"P.12":["F.49"]}', 'a:2:{i:0;s:12:"Completeness";i:1;s:23:" Valuation & Allocation";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'N', NULL, 'Manual', 'SOP 7.1.4.1 Asset Management Policies ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(10, 'Updating AIL', 'PPFA.C-010', '["I & C"]', '{"I & C":["P.13"]}', '{"P.13":["F.56"]}', 'a:3:{i:0;s:12:"Completeness";i:1;s:22:"Valuation & Allocation";i:2;s:25:"Presentation & Disclosure";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'N', NULL, 'System - SAP', 'SOP 7.1.4.1 Asset Management Policies ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(11, 'Annual Physical Count', 'PPFA.C-011', '["CA"]', '{"CA":["P.12"]}', '{"P.12":["F.48"]}', 'a:3:{i:0;s:12:"Completeness";i:1;s:22:"Valuation & Allocation";i:2;s:25:"Presentation & Disclosure";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'N', NULL, 'Manual', 'SOP 7.1.4.1 Asset Management Policies ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(12, 'Invoice Receipt (IR) Processsing', 'PPFA.C-012', '["CA"]', '{"CA":["P.11"]}', '{"P.11":["F.44"]}', 'a:2:{i:0;s:19:"Rights & Obligation";i:1;s:23:" Valuation & Allocation";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'N', NULL, 'SAP', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(13, 'Payment Processing', 'PPFA.C-013', '["CA"]', '{"CA":["P.11"]}', '{"P.11":["F.44"]}', 'a:2:{i:0;s:19:"Rights & Obligation";i:1;s:23:" Valuation & Allocation";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'N', NULL, 'System - SAP', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(14, 'Payment Adjustments', 'PPFA.C-014', '["RA"]', '{"RA":["P.10"]}', '{"P.10":["F.23"]}', 'a:2:{i:0;s:19:"Rights & Obligation";i:1;s:23:" Valuation & Allocation";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'N', NULL, 'Manual', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(15, 'Comparison of Receiving Documents', 'PPFA.C-015', '["CA"]', '{"CA":["P.12"]}', '{"P.12":["F.49"]}', '', '', 'N', NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(16, 'Checking FAST document', 'PPFA.C-016', '["CA"]', '{"CA":["P.12"]}', '{"P.12":["F.49"]}', 'a:3:{i:0;s:12:"Completeness";i:1;s:22:"Valuation & Allocation";i:2;s:25:"Presentation & Disclosure";}', '', 'N', NULL, '', 'SOP 7.1.4.1 Asset Management Policies./ SOP 11.2.3 Transfer of Assets - Site to Site', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(17, 'Calculation of Deprecgption Expense', 'PPFA.C-017', '["CA"]', '{"CA":["P.11"]}', '{"P.11":["F.44"]}', 'a:2:{i:0;s:22:"Valuation & Allocation";i:1;s:26:" Presentation & Disclosure";}', 'a:1:{i:0;s:20:"Primary - Corrective";}', 'N', NULL, 'System - SAP', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(18, 'Reporting of Cancelled or Discontinued Projects', 'PPFA.C-018', '["CA"]', '{"CA":["P.12"]}', '{"P.12":["F.49"]}', 'a:2:{i:0;s:23:"Existence or Occurrence";i:1;s:19:"Rights & Obligation";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'N', NULL, 'Manual', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(19, 'Monitoring of Unserved POs', 'PPFA.C-019', '["RA"]', '{"RA":["P.6a"]}', '{"P.6a":["F.23"]}', 'a:1:{i:0;s:12:"Completeness";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'N', NULL, 'Manual', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(20, 'No control in place', 'NCIP', '', '', '', '', '', 'N', NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-06', 1, '2017-09-06'),
(26, 'BAI09.01.01', 'BAI09.01.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'Regularly', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-11-28', 6, '2017-11-29'),
(27, 'BAI09.01.02', 'BAI09.01.02', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'Regularly', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-11-29', 6, '2017-11-29'),
(28, 'BAI09.01.03', 'BAI09.01.03', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'Regularly', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-11-29', 6, '2017-11-29'),
(29, 'BAI03.01.04', 'BAI03.01.04', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'Regularly', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-11-29', 6, '2017-11-29'),
(30, 'BAI09.01.04', 'BAI09.01.04', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'Regularly', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-11-29', 6, '2017-11-29'),
(31, 'BAI09.01.05', 'BAI09.01.05', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'Regularly', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-11-29', 6, '2017-11-29'),
(32, 'BAI09.01.06', 'BAI09.01.06', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'Regularly', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-11-29', 6, '2017-11-29'),
(33, 'DSS06.01.01', 'DSS06.01.01', '["CA","CE","RA"]', '{"CA":["P.11","P.10"],"CE":["P.5"],"RA":["P.6a","P.6b","P.6c","P.6d","P.6e"]}', '{}', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-11'),
(34, 'BAI10.01.01', 'BAI10.01.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(35, 'DSS04.01.01', 'DSS04.01.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(36, 'APO13.01.01', 'APO13.01.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(37, 'APO10.01.01', 'APO10.01.01', '["CA","CE","RA"]', '{"CA":["P.10","P.11"],"CE":["P.5"],"RA":["P.6a","P.6b","P.6c","P.6d"]}', '{}', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-11'),
(38, 'DSS05.01.01', 'DSS05.01.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(39, 'BAI03.01.01', 'BAI03.01.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(40, 'BAI09.02.01', 'BAI09.02.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(41, 'BAI09.02.02', 'BAI09.02.02', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(42, 'BAI09.02.03', 'BAI09.02.03', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(43, 'BAI09.02.04', 'BAI09.02.04', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(44, 'BAI09.02.05', 'BAI09.02.05', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(45, 'BAI09.02.06', 'BAI09.02.06', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(46, 'BAI09.02.07', 'BAI09.02.07', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(47, 'BAI09.02.08', 'BAI09.02.08', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(48, 'BAI09.02.09', 'BAI09.02.09', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(49, 'BAI09.03.01', 'BAI09.03.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(50, 'BAI09.03.02', 'BAI09.03.02', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(51, 'BAI09.03.03', 'BAI09.03.03', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(52, 'BAI09.03.04', 'BAI09.03.04', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(53, 'BAI09.03.05', 'BAI09.03.05', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(54, 'BAI09.03.06', 'BAI09.03.06', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(55, 'BAI09.03.07', 'BAI09.03.07', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(56, 'BAI09.03.08', 'BAI09.03.08', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(57, 'BAI09.03.09', 'BAI09.03.09', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-01', 6, '2017-12-01'),
(58, 'APO05.06.01', 'APO05.06.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(59, 'APO05.06.02', 'APO05.06.02', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(60, 'APO05.06.03', 'APO05.06.03', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'As Needed', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(61, 'APO10.05.01', 'APO10.05.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'In place', 'Not Applicable', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(62, 'APO10.05.02', 'APO10.05.02', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'Custom Period', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(63, 'APO10.05.03', 'APO10.05.03', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'Custom Period', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(64, 'APO10.05.04', 'APO10.05.04', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'As Needed', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(65, 'APO10.05.05', 'APO10.05.05', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'Yearly', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(66, 'APO10.05.06', 'APO10.05.06', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'Yearly', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(67, 'BAI03.04.01', 'BAI03.04.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(68, 'BAI03.04.02', 'BAI03.04.02', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'Custom Period', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(69, 'BAI03.04.03', 'BAI03.04.03', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'Custom Period', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(70, 'BAI03.04.04', 'BAI03.04.04', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'As Needed', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(71, 'BAI03.04.05', 'BAI03.04.05', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'As Needed', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(72, 'BAI03.10.01', 'BAI03.10.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(73, 'BAI03.10.02', 'BAI03.10.02', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'As Needed', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(74, 'BAI03.10.03', 'BAI03.10.03', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'As Needed', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(75, 'BAI03.10.04', 'BAI03.10.04', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'Custom Period', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(76, 'BAI03.10.05', 'BAI03.10.05', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'As Needed', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(77, 'BAI10.03.01', 'BAI10.03.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'Daily', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(78, 'BAI10.03.02', 'BAI10.03.02', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'As Needed', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(79, 'BAI10.03.03', 'BAI10.03.03', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Corrective";}', 'Y', 'As Needed', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(80, 'BAI10.03.04', 'BAI10.03.04', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'As Needed', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(81, 'DSS04.07.01', 'DSS04.07.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'Daily', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(82, 'DSS04.07.02', 'DSS04.07.02', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'Custom Period', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(83, 'DSS04.07.03', 'DSS04.07.03', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(84, 'DSS04.07.04', 'DSS04.07.04', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'Custom Period', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(85, 'DSS04.07.05', 'DSS04.07.05', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'Custom Period', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(86, 'DSS05.02.01', 'DSS05.02.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(87, 'DSS05.02.02', 'DSS05.02.02', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(88, 'DSS05.02.03', 'DSS05.02.03', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(89, 'DSS05.02.04', 'DSS05.02.04', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'As Needed', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(90, 'DSS05.02.05', 'DSS05.02.05', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'As Needed', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(91, 'DSS05.02.06', 'DSS05.02.06', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(92, 'DSS05.02.07', 'DSS05.02.07', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(93, 'DSS05.02.08', 'DSS05.02.08', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'Yearly', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(94, 'DSS05.02.09', 'DSS05.02.09', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'Yearly', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(95, 'DSS05.03.01', 'DSS05.03.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(96, 'DSS05.03.02', 'DSS05.03.02', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(97, 'DSS05.03.03', 'DSS05.03.03', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'As Needed', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(98, 'DSS05.03.04', 'DSS05.03.04', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(99, 'DSS05.03.05', 'DSS05.03.05', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(100, 'DSS05.03.06', 'DSS05.03.06', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(101, 'DSS05.03.07', 'DSS05.03.07', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(102, 'DSS05.03.08', 'DSS05.03.08', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(103, 'DSS05.03.09', 'DSS05.03.09', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'As Needed', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(104, 'DSS05.04.01', 'DSS05.04.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(105, 'DSS05.04.02', 'DSS05.04.02', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(106, 'DSS05.04.03', 'DSS05.04.03', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(107, 'DSS05.04.04', 'DSS05.04.04', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(108, 'DSS05.04.05', 'DSS05.04.05', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(109, 'DSS05.04.06', 'DSS05.04.06', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'Yearly', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(110, 'DSS05.04.07', 'DSS05.04.07', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(111, 'DSS05.04.08', 'DSS05.04.08', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(112, 'DSS05.06.01', 'DSS05.06.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(113, 'DSS05.06.02', 'DSS05.06.02', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(114, 'DSS05.06.03', 'DSS05.06.03', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(115, 'DSS05.06.04', 'DSS05.06.04', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(116, 'DSS05.06.05', 'DSS05.06.05', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'As Needed', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-07', 6, '2017-12-07'),
(117, 'DSS05.05.01', 'DSS05.05.01', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Directive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-08', 6, '2017-12-08'),
(118, 'DSS05.05.02', 'DSS05.05.02', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-08', 6, '2017-12-08'),
(119, 'DSS05.05.03', 'DSS05.05.03', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:19:"Primary - Detective";}', 'Y', 'As Needed', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-08', 6, '2017-12-08'),
(120, 'DSS05.05.04', 'DSS05.05.04', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-08', 6, '2017-12-08'),
(121, 'DSS05.05.05', 'DSS05.05.05', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-08', 6, '2017-12-08'),
(122, 'DSS05.05.06', 'DSS05.05.06', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-08', 6, '2017-12-08'),
(123, 'DSS05.05.07', 'DSS05.05.07', '', '', '', 'a:1:{i:0;s:14:"Not Applicable";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'Yearly', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-08', 6, '2017-12-08'),
(124, 'DSS06.02.01', 'DSS06.02.01', '["CA","CE","RA"]', '{"CA":["P.10","P.11"],"CE":["P.5"],"RA":["P.6a","P.6b","P.6c","P.6d","P.6e"]}', '{}', 'a:1:{i:0;s:21:"Rights or Obligations";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'As Needed', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-08', 6, '2017-12-11'),
(125, 'DSS06.02.02', 'DSS06.02.02', '["CA","CE","RA"]', '{"CA":["P.10","P.11"],"CE":["P.5"],"RA":["P.6a","P.6b","P.6c","P.6d","P.6e"]}', '{}', 'a:1:{i:0;s:21:"Rights or Obligations";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'As Needed', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-08', 6, '2017-12-11'),
(126, 'DSS06.02.03', 'DSS06.02.03', '["CA","CE","RA"]', '{"CA":["P.10","P.11"],"CE":["P.5"],"RA":["P.6a","P.6b","P.6c","P.6d"]}', '{}', 'a:1:{i:0;s:12:"Completeness";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'As Needed', 'Manual', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-08', 6, '2017-12-11'),
(127, 'DSS06.02.04', 'DSS06.02.04', '["CA","CE","RA"]', '{"CA":["P.11","P.10"],"CE":["P.5"],"RA":["P.6a","P.6b","P.6c","P.6d","P.6e"]}', '{}', 'a:1:{i:0;s:12:"Completeness";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'As Needed', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-08', 6, '2017-12-11'),
(128, 'DSS06.02.05', 'DSS06.02.05', '["CA","CE","RA"]', '{"CA":["P.11","P.10"],"CE":["P.5"],"RA":["P.6a","P.6b","P.6c","P.6d","P.6e"]}', '{}', 'a:1:{i:0;s:23:"Valuation or Allocation";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-08', 6, '2017-12-11'),
(129, 'DSS06.02.06', 'DSS06.02.06', '["CA","CE","RA"]', '{"CA":["P.10","P.11"],"CE":["P.5"],"RA":["P.6a","P.6b","P.6c","P.6d","P.6e"]}', '{}', 'a:1:{i:0;s:23:"Valuation or Allocation";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-08', 6, '2017-12-11'),
(130, 'DSS06.02.07', 'DSS06.02.07', '["CA","CE","RA"]', '{"CA":["P.11","P.10"],"CE":["P.5"],"RA":["P.6a","P.6b","P.6c","P.6d","P.6e"]}', '{}', 'a:2:{i:0;s:12:"Completeness";i:1;s:23:"Valuation or Allocation";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-08', 6, '2017-12-11'),
(131, 'DSS06.02.08', 'DSS06.02.08', '["CA","CE","RA"]', '{"CA":["P.10","P.11"],"CE":["P.5"],"RA":["P.6a","P.6b","P.6c","P.6d","P.6e"]}', '{}', 'a:2:{i:0;s:12:"Completeness";i:1;s:23:"Valuation or Allocation";}', 'a:1:{i:0;s:20:"Primary - Preventive";}', 'Y', 'In place', 'Automated', 'na', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-12-08', 6, '2017-12-11');

-- --------------------------------------------------------

--
-- Table structure for table `gp_controls_test_audit_types`
--

CREATE TABLE `gp_controls_test_audit_types` (
  `control_test_id` int(10) UNSIGNED NOT NULL,
  `audit_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_date` date NOT NULL,
  `last_update_by` int(10) UNSIGNED NOT NULL,
  `last_update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gp_controls_test_audit_types`
--

INSERT INTO `gp_controls_test_audit_types` (`control_test_id`, `audit_type`, `created_by`, `created_date`, `last_update_by`, `last_update_date`) VALUES
(1, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(2, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(3, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(4, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(5, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(6, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(7, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(8, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(9, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(10, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(11, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(12, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(13, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(14, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(15, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(16, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(17, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(18, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(19, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(20, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(21, 'FAS Audit', 1, '2017-09-06', 1, '2017-09-06'),
(24, 'APO', 6, '2017-12-08', 6, '2017-12-08'),
(25, 'APO', 6, '2017-12-08', 6, '2017-12-08'),
(26, 'APO', 6, '2017-12-08', 6, '2017-12-08'),
(27, 'APO', 6, '2017-12-08', 6, '2017-12-08'),
(28, 'APO', 6, '2017-12-08', 6, '2017-12-08'),
(29, 'APO', 6, '2017-12-08', 6, '2017-12-08'),
(30, 'APO', 6, '2017-12-08', 6, '2017-12-08'),
(31, 'APO', 6, '2017-12-08', 6, '2017-12-08'),
(32, 'APO', 6, '2017-12-08', 6, '2017-12-08'),
(33, 'APO', 6, '2017-12-08', 6, '2017-12-08'),
(34, 'APO', 6, '2017-12-08', 6, '2017-12-08'),
(35, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(36, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(37, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(38, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(39, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(40, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(41, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(42, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(43, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(44, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(45, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(46, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(47, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(48, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(49, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(50, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(51, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(52, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(53, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(54, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(55, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(56, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(57, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(58, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(59, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(60, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(61, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(62, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(63, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(64, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(65, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(66, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(67, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(68, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(69, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(70, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(71, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(72, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(73, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(74, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(75, 'BAI', 6, '2017-12-08', 6, '2017-12-08'),
(76, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(77, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(78, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(79, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(80, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(81, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(82, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(83, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(84, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(85, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(86, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(87, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(88, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(89, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(90, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(91, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(92, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(93, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(94, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(95, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(96, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(97, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(98, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(99, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(100, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(101, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(102, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(103, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(104, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(105, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(106, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(107, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(108, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(109, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(110, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(111, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(112, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(113, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(114, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(115, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(116, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(117, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(118, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(119, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(120, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(121, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(122, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(123, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(124, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(125, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(126, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(127, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(128, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(129, 'DSS', 6, '2017-12-08', 6, '2017-12-08'),
(4, 'A/R Trade and Non-Trade Audit', 8, '2018-01-15', 8, '2018-01-15');

-- --------------------------------------------------------

--
-- Table structure for table `gp_controls_test_proc`
--

CREATE TABLE `gp_controls_test_proc` (
  `control_test_id` int(10) UNSIGNED NOT NULL,
  `control_id` int(10) UNSIGNED NOT NULL,
  `control_test_seq` int(11) NOT NULL,
  `test_proc_id` int(10) UNSIGNED NOT NULL,
  `budgeted_mandays` decimal(4,1) NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_date` date NOT NULL,
  `last_update_by` int(10) UNSIGNED NOT NULL,
  `last_update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gp_controls_test_proc`
--

INSERT INTO `gp_controls_test_proc` (`control_test_id`, `control_id`, `control_test_seq`, `test_proc_id`, `budgeted_mandays`, `created_by`, `created_date`, `last_update_by`, `last_update_date`) VALUES
(1, 1, 1, 1, '0.5', 1, '2017-04-03', 1, '2017-04-03'),
(2, 2, 2, 2, '0.5', 1, '2017-04-03', 1, '2017-04-03'),
(3, 3, 3, 3, '2.0', 1, '2017-04-03', 1, '2017-04-03'),
(4, 4, 4, 4, '2.0', 1, '2017-04-03', 1, '2017-04-03'),
(5, 5, 5, 5, '2.0', 1, '2017-04-03', 1, '2017-04-03'),
(6, 6, 6, 6, '1.5', 1, '2017-04-03', 1, '2017-04-03'),
(7, 7, 7, 7, '5.0', 1, '2017-04-03', 1, '2017-04-03'),
(8, 8, 8, 8, '0.5', 1, '2017-04-03', 1, '2017-04-03'),
(9, 9, 8, 8, '0.5', 1, '2017-04-03', 1, '2017-04-03'),
(10, 10, 9, 9, '1.0', 1, '2017-04-03', 1, '2017-04-03'),
(11, 11, 10, 10, '1.0', 1, '2017-04-03', 1, '2017-04-03'),
(12, 12, 11, 11, '1.0', 1, '2017-04-03', 1, '2017-04-03'),
(13, 13, 12, 12, '3.0', 1, '2017-04-03', 1, '2017-04-03'),
(14, 14, 13, 13, '1.0', 1, '2017-04-03', 1, '2017-04-03'),
(15, 15, 14, 14, '1.0', 1, '2017-04-03', 1, '2017-04-03'),
(16, 16, 15, 15, '1.0', 1, '2017-04-03', 1, '2017-04-03'),
(17, 17, 16, 16, '1.0', 1, '2017-04-03', 1, '2017-04-03'),
(18, 18, 17, 17, '3.0', 1, '2017-04-03', 1, '2017-04-03'),
(19, 19, 18, 18, '1.0', 1, '2017-04-03', 1, '2017-04-03'),
(20, 20, 19, 19, '2.0', 1, '2017-04-03', 1, '2017-04-03'),
(21, 20, 20, 20, '0.5', 1, '2017-04-03', 1, '2017-04-03'),
(24, 58, 1, 21, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(25, 59, 1, 21, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(26, 60, 1, 21, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(27, 37, 1, 21, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(28, 61, 1, 21, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(29, 62, 1, 21, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(30, 63, 1, 21, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(31, 64, 1, 21, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(32, 65, 1, 21, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(33, 66, 1, 21, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(34, 36, 1, 21, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(35, 39, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(36, 29, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(37, 67, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(38, 68, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(39, 69, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(40, 70, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(41, 71, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(42, 72, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(43, 73, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(44, 74, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(45, 75, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(46, 76, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(47, 26, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(48, 27, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(49, 28, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(50, 30, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(51, 31, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(52, 32, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(53, 40, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(54, 41, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(55, 42, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(56, 43, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(57, 44, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(58, 45, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(59, 46, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(60, 47, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(61, 48, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(62, 49, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(63, 50, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(64, 51, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(65, 52, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(66, 53, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(67, 54, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(68, 55, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(69, 56, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(70, 57, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(71, 34, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(72, 77, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(73, 78, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(74, 79, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(75, 80, 1, 22, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(76, 35, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(77, 81, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(78, 82, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(79, 83, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(80, 84, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(81, 85, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(82, 38, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(83, 86, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(84, 87, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(85, 88, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(86, 89, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(87, 90, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(88, 91, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(89, 92, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(90, 93, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(91, 94, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(92, 95, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(93, 96, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(94, 97, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(95, 98, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(96, 99, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(97, 100, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(98, 101, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(99, 102, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(100, 103, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(101, 104, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(102, 105, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(103, 106, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(104, 107, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(105, 108, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(106, 109, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(107, 110, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(108, 111, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(109, 117, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(110, 118, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(111, 119, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(112, 120, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(113, 121, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(114, 122, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(115, 123, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(116, 112, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(117, 113, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(118, 114, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(119, 115, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(120, 116, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(121, 33, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(122, 124, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(123, 125, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(124, 126, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(125, 127, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(126, 128, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(127, 129, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(128, 130, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08'),
(129, 131, 1, 24, '0.0', 6, '2017-12-08', 6, '2017-12-08');

-- --------------------------------------------------------

--
-- Table structure for table `gp_denominations`
--

CREATE TABLE `gp_denominations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `p_id` bigint(20) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gp_denominations`
--

INSERT INTO `gp_denominations` (`id`, `p_id`, `amount`, `updated_by`, `created_by`, `created_at`, `updated_at`) VALUES
(8, 10, '100.00', NULL, 17, '2018-03-08 07:06:02', '2018-03-08 07:06:02'),
(9, 10, '200.00', NULL, 17, '2018-03-08 07:06:07', '2018-03-08 07:06:07'),
(10, 10, '300.00', NULL, 17, '2018-03-08 07:06:13', '2018-03-08 07:06:13'),
(11, 9, '150.00', 17, 17, '2018-03-08 07:06:30', '2018-03-19 19:38:56'),
(12, 9, '250.00', NULL, 17, '2018-03-08 07:06:37', '2018-03-08 07:06:37'),
(13, 9, '350.00', NULL, 17, '2018-03-08 07:06:42', '2018-03-08 07:06:42'),
(14, 9, '170.00', 17, 17, '2018-03-20 03:38:39', '2018-03-19 19:39:01');

-- --------------------------------------------------------

--
-- Table structure for table `gp_functions`
--

CREATE TABLE `gp_functions` (
  `function_id` int(10) UNSIGNED NOT NULL,
  `function_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(240) COLLATE utf8_unicode_ci DEFAULT NULL,
  `executable_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `executable_type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `effective_start_date` date NOT NULL,
  `effective_end_date` date DEFAULT NULL,
  `created_by` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` date NOT NULL,
  `last_update_by` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `last_update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gp_global_config`
--

CREATE TABLE `gp_global_config` (
  `auditor_mandays_factor_rate` int(11) NOT NULL,
  `auditor_timeliness` int(11) NOT NULL,
  `auditor_quality` int(11) NOT NULL,
  `auditor_documentation` int(11) NOT NULL,
  `auditor_caf` int(11) NOT NULL,
  `reviewer_timeliness` int(11) NOT NULL,
  `reviewer_quality` int(11) NOT NULL,
  `reviewer_documentation` int(11) NOT NULL,
  `iom_auditee_assist_narrative` text COLLATE utf8_unicode_ci,
  `iom_subject` text COLLATE utf8_unicode_ci,
  `iom_introduction` text COLLATE utf8_unicode_ci,
  `iom_background` text COLLATE utf8_unicode_ci,
  `iom_objectives` text COLLATE utf8_unicode_ci,
  `iom_auditee_assistance` text COLLATE utf8_unicode_ci,
  `iom_staffing` text COLLATE utf8_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_date` date NOT NULL,
  `last_update_by` int(10) UNSIGNED NOT NULL,
  `last_update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gp_global_config`
--

INSERT INTO `gp_global_config` (`auditor_mandays_factor_rate`, `auditor_timeliness`, `auditor_quality`, `auditor_documentation`, `auditor_caf`, `reviewer_timeliness`, `reviewer_quality`, `reviewer_documentation`, `iom_auditee_assist_narrative`, `iom_subject`, `iom_introduction`, `iom_background`, `iom_objectives`, `iom_auditee_assistance`, `iom_staffing`, `created_by`, `created_date`, `last_update_by`, `last_update_date`) VALUES
(90, 97, 1, 1, 1, 98, 1, 1, NULL, 'sdfs', 'dsf', 'sdf', 'fsdf', 'sdf', 'ds', 17, '2018-01-25', 17, '2018-01-25');

-- --------------------------------------------------------

--
-- Table structure for table `gp_logs`
--

CREATE TABLE `gp_logs` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `transaction_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Barcode / Receipt',
  `transaction_id` bigint(20) NOT NULL,
  `payment_trans_id` bigint(20) NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gp_masters`
--

CREATE TABLE `gp_masters` (
  `p_id` bigint(20) UNSIGNED NOT NULL,
  `p_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `p_type_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gp_masters`
--

INSERT INTO `gp_masters` (`p_id`, `p_name`, `p_type_code`, `updated_by`, `created_by`, `created_at`, `updated_at`) VALUES
(9, 'Sodexo', '0001', 0, 17, '2018-03-08 07:05:33', '2018-03-08 07:05:33'),
(10, 'Gift Card', '0002', 0, 17, '2018-03-08 07:05:47', '2018-03-08 07:05:47');

-- --------------------------------------------------------

--
-- Table structure for table `gp_menus`
--

CREATE TABLE `gp_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_parent` int(11) NOT NULL,
  `menu_sequence` tinyint(1) NOT NULL DEFAULT '0',
  `url` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `last_update_date` date DEFAULT NULL,
  `last_update_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gp_menus`
--

INSERT INTO `gp_menus` (`id`, `parent_id`, `name`, `is_parent`, `menu_sequence`, `url`, `icon`, `created_by`, `created_date`, `last_update_date`, `last_update_by`) VALUES
(25, 0, 'Data Entry', 0, 0, '/', 'fa-home', 1, '2017-06-29', '2018-03-05', 17),
(28, 0, 'Administrator', 1, 1, NULL, 'fa-cogs', 1, '2017-06-29', '2018-03-05', 17),
(30, 26, 'Auditor', 1, 6, NULL, 'fa-users', 1, '2017-06-29', '2017-08-03', 1),
(31, 26, 'Auditee', 0, 7, '/auditee', 'fa-users', 1, '2017-06-29', '2017-08-03', 1),
(32, 26, 'Enterprise Risk', 0, 8, 'enterprise_risk', 'fa-warning', 1, '2017-06-29', '2017-08-03', 1),
(33, 26, 'Risk Assessment Scale', 1, 9, NULL, 'fa-balance-scale', 1, '2017-06-29', '2017-12-29', 17),
(34, 26, 'Matrix Definition', 0, 10, '/matrix_definition', 'fa-braille', 1, '2017-06-29', '2017-08-03', 1),
(35, 26, 'Questionnaire', 0, 11, '/questionnaire', 'fa-question-circle', 1, '2017-06-29', '2017-08-03', 1),
(36, 26, 'Value Set', 0, 12, '/value_set', 'fa-tags', 1, '2017-06-29', '2017-08-03', 1),
(37, 26, 'Additional Information', 0, 13, '/additional_information', 'fa-info-circle', 1, '2017-06-29', '2017-08-03', 1),
(38, 26, 'Auditable Entities', 1, 0, NULL, 'fa-road', 1, '2017-06-29', '2017-12-20', 6),
(39, 26, 'Business Process', 0, 1, '/business_process', 'fa-briefcase', 1, '2017-06-29', '2017-07-25', 1),
(40, 26, 'Risks', 0, 2, '/risks', 'fa-warning', 1, '2017-06-29', '2017-07-25', 1),
(41, 26, 'Controls Registry', 0, 3, '/controls_registry', 'fa-edit', 1, '2017-06-29', '2017-07-25', 1),
(42, 26, 'Inquiry', 0, 4, '/auditable_entities/inquiry', 'fa-file-text-o', 1, '2017-06-29', '2017-07-25', 1),
(43, 26, 'Test Procedures', 0, 5, '/test_procedures', 'fa-file', 1, '2017-06-29', '2017-07-25', 1),
(44, 38, 'Company Profile', 0, 0, 'company_profile', 'fa-automobile', 1, '2017-06-29', '2017-12-20', 6),
(45, 38, 'Master', 0, 1, '/auditable_entities/master', 'fa-building', 1, '2017-06-29', '2017-06-29', 1),
(46, 38, 'Actual', 0, 2, 'auditable_entities', 'fa-building-o', 1, '2017-06-29', '2017-06-29', 1),
(47, 30, 'List', 0, 0, '/auditor', 'fa-list', 1, '2017-06-29', '2017-06-29', 1),
(48, 30, 'Grading Factors', 0, 1, '/auditor/grading_factors', 'fa-check-square-o', 1, '2017-06-29', '2017-06-29', 1),
(49, 30, 'Setup', 1, 2, NULL, 'fa-wrench', 1, '2017-06-29', '2017-06-29', 1),
(50, 49, 'Leave', 0, 0, '/leave', 'fa-plane', 1, '2017-06-29', '2017-12-20', 17),
(51, 49, 'Holiday', 0, 1, '/auditor/setup/holiday', 'fa-calendar-minus-o', 1, '2017-06-29', '2017-06-29', 1),
(53, 28, 'Users', 0, 2, 'user', 'fa-users', 1, '2017-06-29', '2018-03-05', 17),
(54, 28, 'Roles', 0, 3, '/administrator/roles', 'fa-user-circle', 1, '2017-06-29', '2018-03-05', 17),
(56, 28, 'Menu', 0, 4, '/administrator/menu', 'fa-list', 1, '2017-06-29', '2018-03-05', 17),
(58, 62, 'Engagement Planning', 0, 0, '/engagement-planning', 'fa-calendar', 1, '2017-08-03', '2017-08-09', 1),
(59, 62, 'Engagement Update', 0, 1, '/project-engagement-update', 'fa-calendar', 1, '2017-08-09', '2017-08-09', 1),
(60, 62, 'Fieldwork', 0, 2, '/fieldwork', 'fa-calendar', 1, '2017-08-09', '2017-08-10', 1),
(61, 62, 'Fieldwork Review', 0, 3, '/fieldwork-review', 'fa-calendar', 1, '2017-08-09', '2017-08-10', 1),
(64, 63, 'Planning Input', 0, 0, '/plan', 'fa-save', 1, '2017-09-18', '2018-01-18', 2),
(66, 63, 'Planning Inquiry', 0, 1, '/plan/inquiry', 'fa-search', 1, '2017-09-19', '2018-02-08', 17),
(67, 63, 'Smart Guide', 0, 2, '/plan-guide', 'fa-link', 1, '2017-10-09', '2018-02-08', 17),
(68, 33, 'Actual', 0, 1, 'risk_assessment?type=actual', 'fa-cog', 17, '2017-12-29', '2017-12-29', 17),
(69, 33, 'Master', 0, 0, 'risk_assessment', 'fa-cog', 17, '2017-12-29', '2017-12-29', 17),
(72, 71, 'Auditor Management Input', 0, 0, '/auditor-management/management-input', 'fa-cog', 17, '2018-02-08', '2018-02-08', 17),
(73, 71, 'Tasks Schedule Inquiry', 0, 1, 'auditor-management/tasks-schedule-inquiry', 'fa-cog', 17, '2018-02-08', '2018-02-08', 17),
(74, 28, 'Company Branch Setup', 0, 0, '/cb_setup', 'fa-cog', 17, '2018-03-01', '2018-03-05', 17),
(75, 28, 'Product Setup', 0, 1, '/product', 'fa-cog', 17, '2018-03-05', '2018-03-05', 17);

-- --------------------------------------------------------

--
-- Table structure for table `gp_migrations`
--

CREATE TABLE `gp_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gp_migrations`
--

INSERT INTO `gp_migrations` (`migration`, `batch`) VALUES
('2018_03_01_185419_create_companies_table', 1),
('2018_03_01_185427_create_branches_table', 1),
('2018_03_01_185433_create_transactions_table', 1),
('2018_03_01_185439_create_logs_table', 1),
('2018_03_01_185445_create_masters_table', 1),
('2018_03_08_111602_create_denominations_table', 2),
('2018_03_19_150404_create_segments_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `gp_options`
--

CREATE TABLE `gp_options` (
  `option_id` int(10) UNSIGNED NOT NULL,
  `option_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `option_value` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gp_options`
--

INSERT INTO `gp_options` (`option_id`, `option_name`, `option_value`) VALUES
(1, 'PASV_LAST_RUN', '2018-02-05 14:07:28');

-- --------------------------------------------------------

--
-- Table structure for table `gp_password_resets`
--

CREATE TABLE `gp_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gp_pasv_monitor`
--

CREATE TABLE `gp_pasv_monitor` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL,
  `progress` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `last_completed` datetime NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_date` date NOT NULL,
  `last_update_by` int(10) UNSIGNED NOT NULL,
  `last_update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gp_pasv_monitor`
--

INSERT INTO `gp_pasv_monitor` (`id`, `status`, `progress`, `message`, `last_completed`, `created_by`, `created_date`, `last_update_by`, `last_update_date`) VALUES
(1, 1, 1, '70 / 13938', '2018-01-18 13:07:26', 1, '2018-01-17', 0, '2018-01-19');

-- --------------------------------------------------------

--
-- Table structure for table `gp_permissions`
--

CREATE TABLE `gp_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `readable_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gp_permission_role`
--

CREATE TABLE `gp_permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT '-1',
  `expires` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gp_permission_user`
--

CREATE TABLE `gp_permission_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT '-1',
  `expires` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gp_roles`
--

CREATE TABLE `gp_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gp_roles`
--

INSERT INTO `gp_roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'administrator', '2016-11-06 20:53:35', '2016-11-06 20:53:35'),
(2, 'cashier', '2016-11-06 21:15:02', '2018-03-20 09:28:38'),
(3, 'approver', '2016-11-06 21:15:13', '2018-03-20 09:28:25');

-- --------------------------------------------------------

--
-- Table structure for table `gp_role_has_permissions`
--

CREATE TABLE `gp_role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gp_role_menus`
--

CREATE TABLE `gp_role_menus` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gp_role_menus`
--

INSERT INTO `gp_role_menus` (`role_id`, `menu_id`) VALUES
(3, 25),
(2, 25),
(1, 28),
(1, 74),
(1, 75),
(1, 53),
(1, 54),
(1, 56);

-- --------------------------------------------------------

--
-- Table structure for table `gp_role_user`
--

CREATE TABLE `gp_role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gp_segments`
--

CREATE TABLE `gp_segments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `p_id` bigint(20) NOT NULL,
  `segment_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gp_segments`
--

INSERT INTO `gp_segments` (`id`, `p_id`, `segment_name`, `from`, `to`, `updated_by`, `created_by`, `created_at`, `updated_at`) VALUES
(2, 9, 'segment 1', 1, 3, 17, 17, '2018-03-19 08:28:04', '2018-03-20 07:08:45'),
(3, 9, 'segment 2', 4, 7, 17, 17, '2018-03-20 02:10:27', '2018-03-20 07:09:14'),
(4, 9, 'segment 3', 9, 11, 17, 17, '2018-03-20 06:35:19', '2018-03-20 07:09:43');

-- --------------------------------------------------------

--
-- Table structure for table `gp_sessions`
--

CREATE TABLE `gp_sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gp_sessions`
--

INSERT INTO `gp_sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('07726f2e9d40178ce9159347b9cc6be86bebb380', 17, '::1', 'Mozilla/5.0 (Windows NT 6.1; rv:59.0) Gecko/20100101 Firefox/59.0', 'YTo4OntzOjY6Il90b2tlbiI7czo0MDoiczJLZ2VOZFlxRkc2aUc3MlZtOTVOcWtYTW1EWTJxNnBHZnRPekxZNCI7czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjM6InVybCI7YTowOnt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU6Imh0dHA6Ly9ncGF2czo5MCI7fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjE3O3M6MTE6Imxhc3RfYWN0aXZlIjtzOjEwOiIxNTIxNTkzNDE1IjtzOjIwOiJpc193YXJuaW5nX2Rpc3BsYXllZCI7YjowO3M6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTIxNTkzNDE1O3M6MToiYyI7aToxNTIxNTkzMzU0O3M6MToibCI7czoxOiIwIjt9fQ==', 1521593416),
('0c8ef1cadaa5b40b41ac9df44d766a862bcf3546', 17, '::1', 'Mozilla/5.0 (Windows NT 6.1; rv:59.0) Gecko/20100101 Firefox/59.0', 'YTo4OntzOjY6Il90b2tlbiI7czo0MDoiVE1iZ1JXWnNYSHVXM0RNM2R2SWtFbHVucEkxRnVwOFhYQ2ZxaGVJViI7czozOiJ1cmwiO2E6MDp7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjE1OiJodHRwOi8vZ3BhdnM6OTAiO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6MTE6Imxhc3RfYWN0aXZlIjtzOjEwOiIxNTIxNTQ0MDI2IjtzOjIwOiJpc193YXJuaW5nX2Rpc3BsYXllZCI7YjowO3M6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjE3O3M6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTIxNTQ0MDI2O3M6MToiYyI7aToxNTIxNTA1NzM4O3M6MToibCI7czoxOiIwIjt9fQ==', 1521544026),
('aae2df723d6d82f1c6f9084bc0d94775c2da6349', NULL, '::1', 'Mozilla/5.0 (Windows NT 6.1; rv:59.0) Gecko/20100101 Firefox/59.0', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoid3NjQjhxY1hQVmxySzRJU1dCQ0I1MlRwSWRCaXFJdjlzSThxR3JoRCI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czoxNToiaHR0cDovL2dwYXZzOjkwIjt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU6Imh0dHA6Ly9ncGF2czo5MCI7fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTIxNTkzMzU0O3M6MToiYyI7aToxNTIxNTkzMzU0O3M6MToibCI7czoxOiIwIjt9czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1521593354),
('d64e612889947c8d2dbcf59b7a0dbcc03bf2572c', NULL, '::1', 'Mozilla/5.0 (Windows NT 6.1; rv:59.0) Gecko/20100101 Firefox/59.0', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiM3B2ZkdxbEp4bjVYUFNTM09LeDM3NUtqOU8zcTJudmpZdGc2VGRYOSI7czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE1MjE1OTMzNTQ7czoxOiJjIjtpOjE1MjE1OTMzNTQ7czoxOiJsIjtzOjE6IjAiO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1521593354);

-- --------------------------------------------------------

--
-- Table structure for table `gp_test_proc`
--

CREATE TABLE `gp_test_proc` (
  `test_proc_id` int(10) UNSIGNED NOT NULL,
  `test_proc_name` varchar(240) COLLATE utf8_unicode_ci NOT NULL,
  `test_proc_narrative` longtext COLLATE utf8_unicode_ci NOT NULL,
  `audit_objective` text COLLATE utf8_unicode_ci NOT NULL,
  `budgeted_mandays` decimal(5,2) NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_date` date NOT NULL,
  `last_update_by` int(10) UNSIGNED NOT NULL,
  `last_update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gp_test_proc`
--

INSERT INTO `gp_test_proc` (`test_proc_id`, `test_proc_name`, `test_proc_narrative`, `audit_objective`, `budgeted_mandays`, `created_by`, `created_date`, `last_update_by`, `last_update_date`) VALUES
(1, 'Segregation of Duties', '1.   Ask for a copy of the Purchasing FAS (PFAS) Table of Organization (TOC). Examine the roles and responsiblities of each personnel and ensure that duties within the department  are properly separated.      \n2.   Verify/investigate exceptions, if any.  ', 'No single individual shall have the authority to execute incompatible duties', '0.50', 1, '2017-09-06', 1, '2017-09-06'),
(2, 'Validity and Approval of Purchase Requisition Documents', '1.  Ask for a copy of the following:\n     a.  Requisition documents:\n           -  Fixed Assets and Supplies Requisition Slip (FASRS) and \n           -  Store Opening Checklist (for new stores)\n     b.  Authorization/Approval Matrix\n2. Check if requisition documents are properly approved. Validate approval against the set Authorization/Approval Matrix.\n3. Take note and verify exceptions, if any,  ', 'All asset requisitions are valid and duly approved by the authorized approving officers.', '0.50', 1, '2017-09-06', 1, '2017-09-06'),
(3, 'Budget Approval for New Stores', '1.  Ask for a copy of  the following:\n     •  Approved Store Opening Checklist with reference PO \n     •  Approved budget from PFAS \n2. Ensure that the budget is approved by authorized approving officer.  Ask a copy of  Approval matrix, (if any) .\n3. View the PO Value  (+VAT)  in tcode ME23N or SE16N table EKPO\n     Table: EKPO\n     Purchasing Doc: reference PO\n     Net Value\n4. Compare actual PO Value vs. Budget.\n5. Obtain explanation from PFAS for the exception noted, if any, \n6. Inquire with Accounting how budget is monitored.', 'To set an approved budget for fixed assets to be purchased', '2.00', 1, '2017-09-06', 1, '2017-09-06'),
(4, 'Bid Process for New Stores', '1.   Select a sample from the approved Store Opening Checlist.\n2.   For each sample selected, check if competitive bidding process was carried out by examining the following documents:\n      • Canvass Sheet\n      • Quotation\n3.   Check if bidding process was done with sufficient number of supplier responses.\n4.   Verify/investigate exceptions noted, if any.', 'To award the contract to the supplier with the optimum bid price and within the approved budget.', '2.00', 1, '2017-09-06', 1, '2017-09-06'),
(5, 'Selection of Supplier for New Stores', '1.   Using the sample Quotations, verify whether offers undergo technical specification/consultation (if applicable) or were evaluated by concerned personnel.\n2.   Check and assess the mechanisms use in evaluating and selecting potentgpl suppliers.\n3.   Ensure that selected supplier is properly approved:\n    ?  P 50,000.00 and below - PFAS Manager\n    ?  P above P 50,000.00 - PFAS Executive', 'To award the contract to the supplier with the optimum bid price and within the approved budget.', '2.00', 1, '2017-09-06', 1, '2017-09-06'),
(6, 'Purchase Order Processing', '1.  Based on the approved asset checklist with reference PO number of sample branch, select a judgemental  sample in checking of accuracy and validity of  PO. \n       •  View in tcode ME23N or in SE16N table EKPO the PO Value .  \n           Purcahsing Doc:  reference PO\n           Net Value: {Blank}\n2. Match PO Value (+VAT ) and supplier versus approved supplier\'s  quotation.\n3. Obtain explanation from PFAS for the exception noted, if any. ', 'POs are created and released to the vendor selected through the bidding process. ', '1.50', 1, '2017-09-06', 1, '2017-09-06'),
(7, 'Receiving of Items', '1.  Ask a copy of the approved asset checklist / FASRS and the corresponding reference PO of each asset from PFAS for the sample  branch (recently opened store)\n2.  View in ME23N the PO details and take note of the following: \n    •  Description\n    •  Quantity GR\'d (movement type 101)\n    •  Asset code \n    • (PO) Net Price Amout (net of VAT)\n3. Compare step 1 and step 2.\n4. Verify with PFAS personnel for the discrepancies noted, if any, ', 'Ensure completeness and accuracy of delivered items and timely processing receipt in the system', '5.00', 1, '2017-09-06', 1, '2017-09-06'),
(8, 'Asset Tagging', '1.  Generate Asset Inventory List (AIL) using tcode ZFI_asset. \n     •  Company: {Sample}\n     •  Site: {Sample}\n2.  Select a judgemental sample of assets, ensure that each asset has its corresponding asset tag.\n3.  Check correctness of the following details on the asset sticker. Should tally on AIL:\n     • Asset Code\n     •  asset class\n     •  Asset description\n     •  Company\n     •  Site\n     •  Cost center\n4. Verify with PFAS discrepancies noted, if any.', 'Fixed Assets are completely and accurately recorded in the system and properly tagged with asset sticker.', '0.50', 1, '2017-09-06', 1, '2017-09-06'),
(9, 'Asset Inventory', '1.  Generate Asset Inventory List (AIL) using tcode ZFI_asset. \n     •  Company: {Sample}\n     •  Site: {Sample}\n2.  Compare the AIL versus approved Store Opening Checklist and FASRS. Use the asset code as reference.\n     •  View in ME23N the asset code of the asset using the PO as reference.\n3.  Perform reconcilgption of the discrepancies noted, if any. View in tcode AS03 the asset details. \n     •  In the asset checklist/ FASRS but not in AIL. \n        ?  Possible with reclassification of asset class - check "asset values" SAP tab field.\n        ?  Wrong site and cost center - check "Time dependent" SAP tab field. Take note of the asset under cost center of SVI {330038}; SMCo {17017}, SSMI {58913}\n     •  Incorrect asset quantity and description -  check "General " SAP tab field and validate the quantity GR\'d in ME23N. \n     •  In the AIL but not listed in the store opening checklist/ FASRS - View in ME23N the PO Header text the site owner.\n     •  Determine duplicate POs made (The same asset description with different asset  code and exceeded  the required/ approved assets requested) - examine the corresponding requisition documents.', 'Assets recorded in the system are  complete and accurate.', '1.00', 1, '2017-09-06', 1, '2017-09-06'),
(10, 'Asset Physical Inventory', '1.  Generate Asset Inventory List (AIL) using tcode ZFI_asset. \n     •  Company: {Sample}\n     •  Site: {Sample}\n2. Conduct physical inventory of selected assets .\n     •  Refrigeration units\n     •  Asset under cost center  (DC - Asset Monitoring)\n     •  CCTVs\n   Note: Auditor may select judgemental sample of assets to be physically checked.\n3.  For missing assets, borrow all  FAST documents (transfer site-site) and (if possible) Delivery Receipt (DR) direct delivery.\n     •  If listed to FAST or DR and confirmed received, conduct investigation and obtain explanation from accountable department head.\n      •  If no documents retrieved, view the PO history \n           ?  get thecorresponding reference PO no in tcode ME23. \n          ?  view in tcode SE16N who processed GR and inquire with the concerned personnel and  conduct investigation. ', 'Assets recorded in the system are  complete and accurate.', '1.00', 1, '2017-09-06', 1, '2017-09-06'),
(11, 'Creation of Invoice Receipt', '1.  Generate a list of Purchase orders in SAP. Use tcode ME2L (Purchasing documents per supplier type): \n     LAYOUT: //FAS_AUDIT/FILENAME: PODtls.xls \n    •  supplier:  All suppliers -NT\n    •  Document type: All\n    •  Site:  All\n    •  Document No. 8000000000 to 9999999999\n    •  Document date:  2008 to audit date\n2.  Generate goods receipt of POs obtained in step 1. Use tcode SE16N. Filename: GRDtls.xls. Ensure to generate the hash total when saving.\n    •  Table:  EKBE\n    •  Purchasing Doc.:  Obtained in step 1\n    •  Movement type : 101 to 102\n    •  Posting date:  {Blank}\n    •  Quantity: {Blank}\n    •  Amount in LC:  {Blank}\n    •  Entered on:  {Blank}\n    •  Created by:  {Blank}\n3.  Generate delivery date of POs obtained in step 1. Use tcode SE16N. Filename:  PODel.xls\n    •  Table :  EKET\n    •  Purchasing Doc:  Obtained in step 1\n    •  Delivery date:  {Blank}\n4.  Create "new collection" on AX core client.\n5.  From the output list " E_OutstandingPOAsset_WP" and "E_OutstandingPOServices_WP" check:\n     •  Purchase Order with zero value on "Amount to be Gr\'d" but with value on "Amount to be Invoiced"\n6.  From the output list "E_POGrDAmount_WP", identify which PO is with processed GR but no ERS, view in tcode ME23N the PO History.\n7.  Discuss with Accounting any delays or non processing of ERS.', 'Only fixed assets deliveries with valid POare processed for payment.', '1.00', 1, '2017-09-06', 1, '2017-09-06'),
(12, 'Outstanding Accounts Payable', '1. Generate outstanding accounts payable NT in tcode FBL1N and fill out the following entry fields: \n    •  Supplier: All suppliers-NT\n    •  Company code: {Sample}\n    •  Open items: {Blank}\n 2. Using the extracted file, perform the following:\n     •  Determine POs of Asset, Services and Consumables\n        ? Asset - "9"\n        ? Services - "8"\n        ? Consumables - "7"\n     •  Determine age of AP from doc date to extraction date. if possible, locate from Accounting the on hold Sales Invoice/ Service Invoice and inquire reason for non processing.', 'To ensure that only valid POs are to be processed for payment and deduction from supplier\'s account are accurate, timely and complete', '3.00', 1, '2017-09-06', 1, '2017-09-06'),
(13, 'Outstanding Accounts Payable for Addback From Supplier\'s Account', '1. Generate outstanding accounts payable NT in tcode FBL1N and fill out the following entry fields: \n    •  Supplier: All suppliers-NT\n    •  Company code: {Sample}\n    •  Open items: {Blank}\n 2. Using the extracted file, perform the following:\n     •  Determine POs of Asset, Services and Consumables\n        ? Asset - "9"\n        ? Services - "8"\n        ? Consumables - "7"\n 3. Determine nature of those amounts in positive balance. Ask assistance from Accounting\n     •  For noted cancelled PO with processed GR and ERS. View in ME23N. \n         ? With payment to supplier, check if there was an addback made to supplier\'s account, get the corresponding document number from Accounting and view in FBL1N.\n4. Discuss with Accounting discrepancies noted, if any.', 'To ensure that only valid POs are to be processed for payment and deduction from supplier\'s account are accurate, timely and complete', '1.00', 1, '2017-09-06', 1, '2017-09-06'),
(14, 'Accuracy of Completed Purchase Order', '1.  Generate a list of Purchase orders in SAP. Use tcode ME2L (Purchasing documents per supplier type): LAYOUT: //FAS_AUDIT / FILENAME: PODtls.xls \n    •  Supplier:  All suppliers -NT\n    •  Document type: All\n    •  Site:  All\n    •  Document No. 8000000000 to 9999999999\n    •  Document date:  2008 to audit date\n2.  Generate goods receipt of POs obtained in step 1. Use tcode SE16N. Filename: GRDtls.xls. Ensure to get the hash total when saving\n    •  Table:  EKBE\n    •  Purchasing Doc.:  Obtained in step 1\n    •  Movement type : 101 to 102\n    •  Posting date:  {Blank}\n    •  Quantity: {Blank}\n    •  Amount in LC:  {Blank}\n    •  Entered on:  {Blank}\n    •  Created by:  {Blank}\n3.  Generate delivery date of POs obtained in step 1. Use tcode SE16N. Filename:  PODel.xls\n    •  Table :  EKET\n    •  Purchasing Doc:  Obtained in step 1\n    Delivery date:  {Blank}\n4.  Create "new collection" on AX core client.\n5.  From the output list "E_POGRDamt_WP". Check in tcode ME23N the PO history\n6.  Determine which PO is with discrepancy between GR (WE) and ERS (RE-L) amount.\nNote: The output shows all POs with unbalanced PO amount and GR amount regardless of the nature.  These include POs with multiple line items and some line items were deleted and no GR\n7.  For the discrepancies noted, ask the corresponding approved quotation/ canvass sheet and check the correct amount as indicated in the canvass sheet/ quotation.\n8.  Take note of the discrepancies noted and discuss with PFAS and Accounting.', 'To ensure that only valid POs are to be processed for payment and deduction from supplier\'s account are accurate, timely and complete', '1.00', 1, '2017-09-06', 1, '2017-09-06'),
(15, 'Transfer of Asset', '1.  Generate AIL under DC Asset Monitoring  cost center for company level\n2.  Use tcode ZFI_asset  \n3.  Fill out the following selected SAP entry fileds.  \n     •  Company: {Sample Company}\n     •  Cost Center: SVI {330038}; SMCo {17017}, SSMI {58913}\n4.  View PO details in tcode ME23N and check in the PO text the branch where the assets are  intended. \n5.   Provide the list of AIL to concerned DC/Warehouse and let them indicate the status of the assets. If necessary, visit the site and conduct physical  inventory (select a judgemental \n     sample) of assets.\n6. Verify and investigate discrepancies if any,\n    •  Assets physically transferred to new site owner but no update in the AIL.\n       ?  Validate to FAST documents and verify to branch the physical existence, ask for a picture of assets with asset sticker attached.\n       ?  Inquire with the receiving branch the corresponding FAST documents if   forwarded  to Accounting FA for update in the system.\n    •   For assets with physical inventory, check the date of acquisition  date in the AIL  and the requestor in tcode ME23N and obtain explanation of not transfer to respective site. ', 'Fixed Assets are completely turned-over to the requesting site/ department.', '1.00', 1, '2017-09-06', 1, '2017-09-06'),
(16, 'Asset Class - Project in Progress', '1. Generate AIL under Project In Progress (PIP) "6000" per company level\n2. Use Tcode ZFI_asset and fill out the following selected entry fields:\n    •  Company code: {Sample}\n    •  Asset class: {6000}\n3.  From the extracted file, determine which  assets already met the critergp but not yet re-classed to correct asset class. View the reference Purchase Order in Tcode SE16N table M_MEKKA\n    • Completed PO (GR) - View PO history in ME23N. \n    • Goods Receipt (GR) processed is already 90% or the remaining balance is retention - View the billing plan and Goods Receipt in tcode ME23N\n    •  Store is already operating. - Either the Auditor may inquire with FAS or Operations or view in SAP if with posted sales already in tcode FBL5N \n4.  Generate AIL of branches which are not yet Operating. (Ask the list from Operations for stores with site code but not yet operating).\n5.  Use tcode ZFI_asset and fill out the following selected entry fields.\n     •  Company code: {Sample}\n     •  Site: {Not Yet Operating}\n     • Asset class {Blank}\n6.  From the extracted file, determine in the "asset class" column which assets were directly recorded to correct  asset class\n7.  Take note of the exceptions, if any, and discuss with Accounting FA.', 'Fixed assets (under the Projects in Progress) are recorded to the correct asset class if:\n-  Project is complete;\n-  90% of the project is GRd:\n-  Store is already in operation.', '1.00', 1, '2017-09-06', 1, '2017-09-06'),
(17, 'Unprocessed Purchase Order', '1.  Generate a list of Purchase orders in SAP. Use tcode ME2L (Purchasing documents per supplier type): LAYOUT: //FAS_AUDIT / FILENAME: PODtls.xls \n    •  supplier: {All suppliers-NT}  \n    •  Document type : {All}\n    •  Site : All Sites\n    •  Document No.: 8000000000 to 9999999999\n    •  Document date: 2008 to audit date\n2.  Generate goods receipt of POs obtained in step 1. Use tcode SE16N.\n    •  Table: Ekbe\n    •  Purchasing doc: ontained in step 1\n    •  Movement type: 101 to 102\n    •  Posting date: {blank}\n    •  Quantity:{Blank}\n    •  Amount in LC: {Blank}\n    •  Entered on: {Blank}\n    •  Created by: {Blank}\n   Notes: FILENAME: GRDtls.xls , ensure to get the hash total upon saving.\n3. Generate delivery date of POS obtained in step 1. Use tcode SE16N.FILENAME:PODel.xls\n    • Table: EKET\n    •  Purchasing Doc - obtained in step 1\n    •  Delivery Date: {Blank} \n4.  Create "new Collection" on AX core client. \n5.   From the output list " E_OutstandingPOAsset_WP" and "E_OutstandingPOServices_WP" and perform the following:\n    •  From the list of PO, select a judgemental sample of purchases. For each purchase check the requisition documents such as ( FASRS, budget, and canvass sheet).\n    •  View in tcode ME23N the PO details/history .\n      ?  Unfinished project with subsequent payments - Inquire with PFAS for the reason of delay\n      ?   Finished projects - Inquire with PFAS for the reason of delay\n      ?  Cancelled project with processed GR and ERS -\n           i.   Ask a copy of Memo for the cancellation of  the project\n           ii.  View in tcode ME23N if GR and ERS are reversed, \n           iii.  If paid, check if with addback made from supplier\'s account ask assistance from Accounting\n     Not yet started project with DP - Inquire with PFAS for the reason of delay', 'POs are valid and delivered within the agreed period. ', '3.00', 1, '2017-09-06', 1, '2017-09-06'),
(18, 'Unserved Purchase Order', '1. Generate a list of Purchase orders in SAP. Use tcode ME2L (Purchasing documents per supplier type): LAYOUT: //FAS_AUDIT / FILENAME: PODtls.xls \n    •  supplier:  All suppliers - NT\n    •  Document Type:  All\n    •  Site: All\n    •  Document No.: 8000000000 to 9999999999\n    •  Document Date:  2008 to audit date\n2.  Generate goods receipt of POs obtained in step no. 1. Use tcode SE16N. \n    •  Table:  Ekbe\n    •  Purchasing Doc: Obtained in step 1\n    •  Movement type:  101 to 102\n    •  Posting date:  {blank}\n    •  Quantity:  {blank}\n    •  Amount in LC: {Blank}\n    •  Entered on:  {Blank}\n    •  Created by:  {Blank}\n3.  Generate delivery date of POs obtained in step 1. use tcode Se16N. Filename: PODel.xls\n    •  Table:  EKET\n    •  Purchasing Doc:  Obtained in step 1\n    •  Delivery Date:  {Blank}\n4.  Create "New Collection" on AX core client.\n5.  From the output list "E_OutstandingPOAssetNoDlvry_WP". \n6. Determine age of POs from delivery date to extraction date.\n7.  Inquire with PFAS personnel for the status.', 'To ensure that all POs are served within the agreed delivery date', '1.00', 1, '2017-09-06', 1, '2017-09-06'),
(19, 'Deleted Purchase Order With Down Payment', '1.  Generate a list of Purchase orders in SAP. Use tcode ME2L (Purchasing documents per supplier type): LAYOUT: //FAS_AUDIT / FILENAME: PODtls.xls \n    •  supplier: All suppliers- NT\n    •  Document type: Obtained in step 1\n    •  Site : All\n    •  Document No.: 8000000000  to 9999999999\n    •  Document date: 2008 to audit date\n2.  Generate goods receipt of POs extracted obtained in step 1. Use tcode SE16N.\n    •  Table: Ekbe\n    •  Purchasing doc:  Obtained in step 1\n    •  Movement type: 101 to 102\n    •  Posting date: Blank\n    •  Quantity : {Blank}\n    •  Amount in LC: {Blank}\n    •  Entered on: {Blank}\n    •  Created by: {Blank}\n   Notes: FILENAME: GRDtls.xls , ensure to get hash total upon saving.\n3.  Generate delivery date of POs obtained in step 1. Use tcode SE16N. Filename: PODel.xls\n    •  Table:  EKET \n    •  Purchasing Doc.: Obtained in step 1\n    •  Delivery date: {Blank}\n4.  Create "new Collection" on AX core client. \n5.  From the output list " E_DeletedPOwBal_WP" and "E_POGRDamt_WP", Perform the following:\n    •  From the list of PO, select a judgemental sample of purchases. For each purchase check the  following documents such as  FASRS or checklist, budget, and canvass sheet.\n    •  View in tcode ME23N the PO details/history . Check if there are: \n      ? With down payment - ask assistance from Accounting if with addback made from the supplier\'s account.\n      ?  Finished project with unprocessed balance - If possible, check with PFAS personnel if with corresponding  Sales Invoice/ Service Invoice. Ask reason for the delay in processing.', 'Only POs aged 18 without unpaid balance/down payment are deleted/purged from SAP.', '2.00', 1, '2017-09-06', 1, '2017-09-06'),
(20, 'Purging/Archiving of Deleted Completed Purchase Orders', '1.  Generate a list of Purchase orders in SAP. Use tcode ME2L (Purchasing documents per supplier type): LAYOUT: //FAS_AUDIT / FILENAME: PODtls.xls \n    •  supplier: All suppliers-NT\n    •  Document type :  All\n    •  Site: All\n    •  Document No.: 8000000000 to 9999999999\n    •  Document date: 2008 to audit date\n2.  Generate goods receipt of POs obtained step 1. Use tcode SE16N. Filename: GRDtls.xls, further do not forget the hash total prior in saving.\n    •  Table:  Ekbe\n    •  Purchasing Doc.: obtained in step 1\n    •  Movement type: 101 to 102\n    •  Posting Date: {Blank}\n    •  Quantity:  {Blank}\n    •  Amount in LC:  {Blank}\n    •  Entered on:  {Blank}\n    •  Created by:  {Blank}\n3.  Generate delivery dateof POs obtained in step 1. Use tcode SE16N. Filename: PODel.xls\n    •  Table:  EKET\n    •  Purchasing Doc. :  Obtained in step 1\n    •  Delivery date: { Blank}\n4.  Create "new collection" on AX core client.\n5.  From the output list " E_outstandingDelPO_WP" . Inquire with ITY for the non processing of PO Archiving/ Purging.', 'All POS tagged for deletion are purged .', '0.50', 1, '2017-09-06', 1, '2017-09-06'),
(21, 'APO Test', '<p>APO Test</p>', 'Test APO', '1.00', 6, '2017-12-08', 6, '2017-12-08'),
(22, 'BAI Test', '<p>BAI Test</p>', 'Test BAI', '1.00', 6, '2017-12-08', 6, '2017-12-08'),
(23, 'EDM Test', '<p>EDM Test</p>', 'Test EDM', '1.00', 6, '2017-12-08', 6, '2017-12-08'),
(24, 'DSS Test', '<p>DSS Test</p>', 'Test DSS', '1.00', 6, '2017-12-08', 6, '2017-12-08'),
(25, 'MEA Test', '<p>MEA Test</p>', 'Test MEA', '1.00', 6, '2017-12-08', 6, '2017-12-08');

-- --------------------------------------------------------

--
-- Table structure for table `gp_transactions`
--

CREATE TABLE `gp_transactions` (
  `transaction_id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `branch_id` bigint(20) NOT NULL,
  `transaction_date` date NOT NULL,
  `redemption_date` date NOT NULL,
  `barcode` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `denomination` decimal(10,2) NOT NULL,
  `p_id` bigint(20) NOT NULL,
  `cashier_id` bigint(20) DEFAULT NULL COMMENT 'for DS',
  `approved_by` bigint(20) DEFAULT NULL,
  `approved_date` date DEFAULT NULL,
  `attribute_1` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attribute_2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'incomplete / approval / approved / rejected',
  `updated_by` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gp_users`
--

CREATE TABLE `gp_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(240) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_name` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `user_tag` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_tag_ref_id` int(11) DEFAULT NULL,
  `effective_start_date` date DEFAULT NULL,
  `effective_end_date` date DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login_date` date DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_date` date NOT NULL,
  `last_update_by` int(10) UNSIGNED NOT NULL,
  `last_update_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gp_users`
--

INSERT INTO `gp_users` (`user_id`, `user_name`, `password`, `description`, `display_name`, `employee_id`, `user_tag`, `user_tag_ref_id`, `effective_start_date`, `effective_end_date`, `remember_token`, `last_login_date`, `created_by`, `created_date`, `last_update_by`, `last_update_date`) VALUES
(17, 'admin', '$2y$10$SDo/Ku9l6kaUNWFCktiQxOrIgkZfF8iEZsHyYsor6Qxbuic72b/U2', 'Administrator', 'Administrator', 900, 'Administrator', NULL, '2017-12-20', '2018-04-21', '8snJyHpsSOjMkMT623SHujHIvVLt45PdG02qe6qW8jF7FFHcd3Vp3xqcP6Lb', '2018-03-21', 1, '2017-12-20', 17, '2018-03-21'),
(23, 'cashier', '$2y$10$QMDtBMAkW/ABFTreSWwSyehZXGjP0lp.wc0LuHXpK4pzXQdKRh.1S', 'Cashier 1', 'Cashier 1', NULL, NULL, NULL, '2018-03-01', '2029-11-27', 'GjEbiI9WhKor4GcWcs7M0mFpU1vssakJjctzOpZF3qkbENY5xHX1hExe17Fr', '2018-03-20', 17, '2018-03-20', 23, '2018-03-20'),
(24, 'approver', '$2y$10$1GS7a/.zEiQvN2SrM0AGU.kGsqszm.HYsTziPWMCEEBv92me6SmQi', 'Approver 1', 'Approver 1', NULL, NULL, NULL, '2018-03-01', '2029-12-01', 'kuzUUENL93I0LYJsW9rnVSPvW4FrMmZvkXWuiM5IrhoImKGVjSejkQiUslBT', '2018-03-20', 17, '2018-03-20', 24, '2018-03-20');

-- --------------------------------------------------------

--
-- Table structure for table `gp_user_has_permissions`
--

CREATE TABLE `gp_user_has_permissions` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gp_user_has_roles`
--

CREATE TABLE `gp_user_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gp_user_has_roles`
--

INSERT INTO `gp_user_has_roles` (`role_id`, `user_id`) VALUES
(1, 17),
(2, 17),
(3, 17),
(2, 23),
(3, 24);

-- --------------------------------------------------------

--
-- Table structure for table `gp_user_resp_assignments`
--

CREATE TABLE `gp_user_resp_assignments` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `responsibility_id` int(10) UNSIGNED DEFAULT NULL,
  `effective_start_date` date NOT NULL,
  `effective_end_date` date DEFAULT NULL,
  `created_by` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` date NOT NULL,
  `last_update_by` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `last_update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gp_value_sets`
--

CREATE TABLE `gp_value_sets` (
  `value_set_id` int(10) UNSIGNED NOT NULL,
  `value_set_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(240) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `meaning` varchar(240) COLLATE utf8_unicode_ci NOT NULL,
  `from_clause` varchar(240) COLLATE utf8_unicode_ci NOT NULL,
  `where_clause` varchar(240) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_date` date NOT NULL,
  `last_update_by` int(10) UNSIGNED NOT NULL,
  `last_update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gp_branches`
--
ALTER TABLE `gp_branches`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `gp_companies`
--
ALTER TABLE `gp_companies`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `gp_controls_master`
--
ALTER TABLE `gp_controls_master`
  ADD PRIMARY KEY (`control_id`),
  ADD KEY `controls_masters_component_code_principle_code_focus_code_index` (`component_code`,`principle_code`,`focus_code`),
  ADD KEY `controls_master_control_name_index` (`control_name`),
  ADD KEY `controls_master_control_code_index` (`control_code`);

--
-- Indexes for table `gp_controls_test_audit_types`
--
ALTER TABLE `gp_controls_test_audit_types`
  ADD KEY `controls_test_audit_types_control_test_id_foreign` (`control_test_id`);

--
-- Indexes for table `gp_controls_test_proc`
--
ALTER TABLE `gp_controls_test_proc`
  ADD PRIMARY KEY (`control_test_id`),
  ADD KEY `controls_test_proc_control_id_foreign` (`control_id`),
  ADD KEY `controls_test_proc_test_proc_id_foreign` (`test_proc_id`);

--
-- Indexes for table `gp_denominations`
--
ALTER TABLE `gp_denominations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gp_functions`
--
ALTER TABLE `gp_functions`
  ADD PRIMARY KEY (`function_id`),
  ADD KEY `executable_name_executable_type` (`executable_name`,`executable_type`),
  ADD KEY `effective_start_date_effective_end_date` (`effective_start_date`,`effective_end_date`),
  ADD KEY `function_name` (`function_name`);

--
-- Indexes for table `gp_logs`
--
ALTER TABLE `gp_logs`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `gp_masters`
--
ALTER TABLE `gp_masters`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `gp_menus`
--
ALTER TABLE `gp_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gp_options`
--
ALTER TABLE `gp_options`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `gp_password_resets`
--
ALTER TABLE `gp_password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `gp_pasv_monitor`
--
ALTER TABLE `gp_pasv_monitor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gp_permissions`
--
ALTER TABLE `gp_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `gp_permission_role`
--
ALTER TABLE `gp_permission_role`
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `gp_permission_user`
--
ALTER TABLE `gp_permission_user`
  ADD KEY `permission_user_user_id_index` (`user_id`),
  ADD KEY `permission_user_permission_id_index` (`permission_id`);

--
-- Indexes for table `gp_roles`
--
ALTER TABLE `gp_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `gp_role_has_permissions`
--
ALTER TABLE `gp_role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `gp_role_menus`
--
ALTER TABLE `gp_role_menus`
  ADD KEY `role_menus_role_id_foreign` (`role_id`),
  ADD KEY `role_menus_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `gp_role_user`
--
ALTER TABLE `gp_role_user`
  ADD KEY `role_user_user_id_index` (`user_id`),
  ADD KEY `role_user_role_id_index` (`role_id`);

--
-- Indexes for table `gp_segments`
--
ALTER TABLE `gp_segments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gp_sessions`
--
ALTER TABLE `gp_sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `gp_test_proc`
--
ALTER TABLE `gp_test_proc`
  ADD PRIMARY KEY (`test_proc_id`);

--
-- Indexes for table `gp_transactions`
--
ALTER TABLE `gp_transactions`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `gp_users`
--
ALTER TABLE `gp_users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `effective_start_date_effective_end_date` (`effective_start_date`,`effective_end_date`),
  ADD KEY `user_name` (`user_name`);

--
-- Indexes for table `gp_user_has_permissions`
--
ALTER TABLE `gp_user_has_permissions`
  ADD PRIMARY KEY (`user_id`,`permission_id`),
  ADD KEY `user_has_permissions_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `gp_user_has_roles`
--
ALTER TABLE `gp_user_has_roles`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `user_has_roles_user_id_foreign` (`user_id`);

--
-- Indexes for table `gp_user_resp_assignments`
--
ALTER TABLE `gp_user_resp_assignments`
  ADD KEY `effective_start_date_effective_end_date` (`effective_start_date`,`effective_end_date`),
  ADD KEY `users_FK` (`user_id`),
  ADD KEY `responsibility_FK` (`responsibility_id`);

--
-- Indexes for table `gp_value_sets`
--
ALTER TABLE `gp_value_sets`
  ADD PRIMARY KEY (`value_set_id`),
  ADD KEY `value_sets_value_set_name_index` (`value_set_name`),
  ADD KEY `value_sets_code_index` (`code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gp_branches`
--
ALTER TABLE `gp_branches`
  MODIFY `branch_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `gp_companies`
--
ALTER TABLE `gp_companies`
  MODIFY `company_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `gp_controls_master`
--
ALTER TABLE `gp_controls_master`
  MODIFY `control_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;
--
-- AUTO_INCREMENT for table `gp_controls_test_proc`
--
ALTER TABLE `gp_controls_test_proc`
  MODIFY `control_test_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;
--
-- AUTO_INCREMENT for table `gp_denominations`
--
ALTER TABLE `gp_denominations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `gp_functions`
--
ALTER TABLE `gp_functions`
  MODIFY `function_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gp_logs`
--
ALTER TABLE `gp_logs`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gp_masters`
--
ALTER TABLE `gp_masters`
  MODIFY `p_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `gp_menus`
--
ALTER TABLE `gp_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `gp_options`
--
ALTER TABLE `gp_options`
  MODIFY `option_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gp_pasv_monitor`
--
ALTER TABLE `gp_pasv_monitor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gp_permissions`
--
ALTER TABLE `gp_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gp_roles`
--
ALTER TABLE `gp_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `gp_segments`
--
ALTER TABLE `gp_segments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gp_test_proc`
--
ALTER TABLE `gp_test_proc`
  MODIFY `test_proc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `gp_transactions`
--
ALTER TABLE `gp_transactions`
  MODIFY `transaction_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `gp_users`
--
ALTER TABLE `gp_users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `gp_value_sets`
--
ALTER TABLE `gp_value_sets`
  MODIFY `value_set_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `gp_controls_test_audit_types`
--
ALTER TABLE `gp_controls_test_audit_types`
  ADD CONSTRAINT `controls_test_audit_types_control_test_id_foreign` FOREIGN KEY (`control_test_id`) REFERENCES `gp_controls_test_proc` (`control_test_id`) ON DELETE CASCADE;

--
-- Constraints for table `gp_controls_test_proc`
--
ALTER TABLE `gp_controls_test_proc`
  ADD CONSTRAINT `controls_test_proc_control_id_foreign` FOREIGN KEY (`control_id`) REFERENCES `gp_controls_master` (`control_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `controls_test_proc_test_proc_id_foreign` FOREIGN KEY (`test_proc_id`) REFERENCES `gp_test_proc` (`test_proc_id`) ON DELETE CASCADE;

--
-- Constraints for table `gp_permission_role`
--
ALTER TABLE `gp_permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `gp_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `gp_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `gp_permission_user`
--
ALTER TABLE `gp_permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `gp_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `gp_users` (`user_id`) ON DELETE CASCADE;

--
-- Constraints for table `gp_role_has_permissions`
--
ALTER TABLE `gp_role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `gp_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `gp_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `gp_role_menus`
--
ALTER TABLE `gp_role_menus`
  ADD CONSTRAINT `role_menus_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `gp_menus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_menus_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `gp_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `gp_role_user`
--
ALTER TABLE `gp_role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `gp_roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `gp_users` (`user_id`) ON DELETE CASCADE;

--
-- Constraints for table `gp_user_has_permissions`
--
ALTER TABLE `gp_user_has_permissions`
  ADD CONSTRAINT `user_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `gp_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_has_permissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `gp_users` (`user_id`) ON DELETE CASCADE;

--
-- Constraints for table `gp_user_has_roles`
--
ALTER TABLE `gp_user_has_roles`
  ADD CONSTRAINT `user_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `gp_roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_has_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `gp_users` (`user_id`) ON DELETE CASCADE;

--
-- Constraints for table `gp_user_resp_assignments`
--
ALTER TABLE `gp_user_resp_assignments`
  ADD CONSTRAINT `responsibility_FK` FOREIGN KEY (`responsibility_id`) REFERENCES `gp_responsibilities` (`responsibility_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `users_FK` FOREIGN KEY (`user_id`) REFERENCES `gp_users` (`user_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
